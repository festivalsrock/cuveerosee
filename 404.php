<?php


get_header(); ?>

    <div class="template-404">
        <div class="center">
            <div class="title">ERROR 404</div>    
            <div class="resume">PAGE NOT FOUND</div>
            <a class="button" href="/">BACK TO HOMEPAGE</a>
        </div>
    </div>

<?php get_footer(); ?>