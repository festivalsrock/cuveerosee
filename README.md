Cuvée Rosée

git repo : https://bitbucket.org/festivalsrock/cuveerosee


Command :
npm start
npm run dist

This command opens http://local.cuveerosee.fr/
If SSL ERROR, change the https to http
Start wamp for alias to work

In local development, db 'nvjt_cuveerosee' from 'nvjt.myd.infomaniak.com' is used (Owned by Rémi Boyer)

To push to preprod go to https://preprod.cuveerose.com/wp-admin > WP Pusher > Themes > Update theme
To update the production site https://cuveerose.com/ go to the admin and deploy