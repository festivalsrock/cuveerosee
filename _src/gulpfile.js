'use strict';

const gulp = require('gulp');
// const rev = require('gulp-rev');
// const collect = require('gulp-rev-collector');
// const revDel = require('rev-del');
const exit = require('gulp-exit');
const sass = require('gulp-sass');
const autoprefixer = require('gulp-autoprefixer');
const rename = require('gulp-rename');
const uglify = require('gulp-uglify');
const sourcemaps = require('gulp-sourcemaps');
const csso = require('gulp-csso');
const stripDebug = require('gulp-strip-debug');

const browserify = require('browserify');
const watchify = require('watchify');
const babelify = require('babelify');

const source = require('vinyl-source-stream');
const buffer = require('vinyl-buffer');

// LIVE RELOAD PHP, WORK THROUGH
// https://github.com/vohof/gulp-livereload/issues/122
const browserSync = require('browser-sync').create();
const localProxy = 'local.cuveerosee.fr';

const cssDestFolder = '../css/';
const cssMinDestFolder = '../css/';

const jsDestFolder = '../js/';


/** Version number to prevent cache */
/*
gulp.task('revision:rename', ['compile'], () =>
  gulp.src(['jsDestFolder+'*.js'])
  .pipe(rev())
  .pipe(revDel())
  .pipe(gulp.dest('.'))
  .pipe(rev.manifest({ path: 'manifest.json' }))
  .pipe(gulp.dest('.'))
);

gulp.task('revision:updateReferences', ['compile', 'revision:rename'], () =>
   gulp.src(['./manifest.json','./js/*.{json,js}'])
   .pipe(collect())
   .pipe(gulp.dest('.'))
);

gulp.task('compile:production', ['compile', 'revision:rename', 'revision:updateReferences']);
*/



gulp.task('minify', function () {
  // console.log('MINIFY');
  return gulp.src(cssDestFolder + 'style.css')
  // .pipe(sourcemaps.init())
  // .pipe(csso())
  // .pipe(sourcemaps.write())
  // .pipe(rename({suffix: '.min'}))
  .pipe(gulp.dest(cssMinDestFolder));
});
  

function compile(watch) {
  var bundler = watchify(browserify('./src/index.js', {debug: true}).transform(babelify, {
      presets: ['babel-preset-es2015'].map(require.resolve),
      sourceMaps: false
  }));  
  
  function rebundle() {
    return bundler
    .bundle()
    .on('error', function (err) {
      console.error(err);
      this.emit('end');
    })
    .pipe(source('temp.js'))
    .pipe(buffer())
    .pipe(rename('main.js'))
    .pipe(gulp.dest(jsDestFolder));
  }
  
  function rebundleDist() {
    return bundler
    .bundle()
    .on('error', function (err) {
      console.error(err);
      this.emit('end');
    })
    .pipe(source('temp.js'))
    .pipe(buffer())
    .pipe(rename('main.js'))
    .pipe(sourcemaps.init({loadMaps: true}))
    .pipe(stripDebug())
    .pipe(uglify())
    .pipe(sourcemaps.write(jsDestFolder))
    .pipe(gulp.dest(jsDestFolder));
  }
  
  if (watch) {
    bundler.on('update', function () {
      console.log('-> bundling...');
      // rebundle().pipe(browserSync.stream());
      rebundle().pipe(browserSync.reload({stream:true}));
    });
    rebundle();
  } else {
    rebundleDist().pipe(exit());
  }
}
  


gulp.task('sass', function () {
  return gulp.src('./scss/**/*.scss')
  .pipe(sass({ outputStyle: 'compact' }).on('error', sass.logError)) // compressed, expanded, compact
  .pipe(autoprefixer({
    // browsers: ['last 2 versions'],
    cascade: false
  }))
  .pipe(gulp.dest(cssDestFolder))
  .pipe(browserSync.stream());
});

gulp.task('watch', ['sass'], function () {
  browserSync.init({ 
    host: localProxy,
    proxy: 'http://' + localProxy, 
    // online: true,
    open: 'external',
    port: 3000
  });
  gulp.watch('./scss/**/*.scss', ['build']);
  compile(true);
  gulp.watch("../views/partials/*.php").on('change', browserSync.reload);
  gulp.watch("../views/*.php").on('change', browserSync.reload);
  gulp.watch("../*.php").on('change', browserSync.reload);
});


// Tâche "build"
gulp.task('build', ['sass']);


// Tâche "prod" = Build + minify
gulp.task('dist', ['build',  'minify'], function () {
  compile(false);
});

// Tâche par défaut
gulp.task('default', ['watch']);
