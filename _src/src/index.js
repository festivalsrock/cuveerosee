'use strict'

var platform = require('platform');
var objectFit = require('objectFitPolyfill');
var intro = require('./ui/intro');
var menu = require('./ui/menu');
var home = require('./ui/home');
var transitions = require('./ui/transitions');
var buttons = require('./ui/buttons');
var sliders = require('./ui/sliders');
var storeLocator = require('./ui/gmapStoreLocator');
var newsletter = require('./ui/newsletter');
var contest = require('./ui/contest');
var parallax = require('./ui/parallax');
var life = require('./ui/life');
var scrollUtils = require('./tools/scroll-utils');
var lazyLoading = require('./tools/lazy-loading');
var urlUtils = require('./tools/url-utils');

var isDesktop, isPortrait, isTablet, isChrome, isIOS = false;

var App = {

    checkOrientation: function() {
        // console.log('App checkOrientation()');
        var isPortrait = window.innerWidth < window.innerHeight;
        if(isPortrait) document.body.classList.add('portrait');
        else document.body.classList.add('landscape');
        
        window.addEventListener('resize', function() {
            var isPortrait = window.innerWidth < window.innerHeight;
            // console.log('ORIENTATION CHANGE', isPortrait);
            document.body.classList.remove('portrait');
            document.body.classList.remove('landscape');
            if(isPortrait) document.body.classList.add('portrait');
            else document.body.classList.add('landscape');
        });
    },

    init: function () {
        // console.log('App init()');

        this.checkOrientation();

        var os = platform.ua.toLowerCase().indexOf('mac') > -1 ? 'mac' : 'win';
        var elBody = document.body;
        var aPlatform = platform.name.split(' ');
        try {
            for(var i = 0, nb = aPlatform.length; i<nb; ++i) {
                elBody.classList.add(aPlatform[i].toLowerCase());
            }
            elBody.classList.add(os);
        } catch (e) {
            console.log('Handle error platform', e);
        }

        isDesktop = document.documentElement.classList.contains('desktop');
        isTablet = document.documentElement.classList.contains('tablet');
        isIOS = document.documentElement.classList.contains('ios');
        isChrome = document.body.classList.contains('chrome');
        isPortrait = window.innerWidth < window.innerHeight;
        if(isPortrait) {
            document.body.classList.add('portrait');
        } else {
            document.body.classList.add('landscape');
        }

        intro.init(isPortrait);
        home.init(isDesktop);
        transitions.init(isDesktop);
        buttons.init(isDesktop);
        sliders.init(isDesktop, isTablet, isPortrait, App.killSmoothScroll);
        menu.init();
        storeLocator.init();
        newsletter.init();
        contest.init();
        life.init();
        parallax.init(isDesktop);
        
        
        scrollUtils.init(isDesktop);
        scrollUtils.add(menu.checkScrollBehavior);
        // if(isDesktop) {
        //     new scrollUtils.SmoothScroll(document, 120, 12);
        // }



        /*var scrollElement = function (element, scrollPosition, duration) {
            var style = element.style;
          
            // setup CSS transition duration and easing function
            style.webkitTransition =
                  style.transition = duration + 's';
            style.webkitTransitionTimingFunction =
                  style.TransitionTimingFunction = 'ease-in-out';
          
            // use translate3d to force hardware acceleration
            style.webkitTransform =
                  style.Transform = 'translate3d(0, ' + -scrollPosition + 'px, 0)';
          }
          
          var scrollBody = scrollElement.bind(null, document.getElementsByTagName('body')[0]);*/
    },

    killSmoothScroll: function(b) {
        console.log('killSmoothScroll', b);        
        scrollUtils.setDisableSmoothScroll(b);
    },

    loadComplete: function() {
        // parallax.init(isDesktop);
        lazyLoading.init(isPortrait);
        scrollUtils.add(lazyLoading.lazyLoad);
        if(isDesktop && isChrome) scrollUtils.enableSmoothScroll();
    },

    cookieAccepted: function() { 

        var onSendSuccess = function(value) {
            // console.log('ON SEND SUCCESS', value.response);
            var doc = new DOMParser().parseFromString(value.response, 'text/html');
            var scripts = doc.querySelectorAll('script, noscript');
            var elScript, scrp;
            for(var i = 0, nb = scripts.length; i < nb; ++i) {      
                elScript = scripts[i];
                scrp = document.createElement(elScript.nodeName.toLocaleLowerCase());

                // Copy attributes
                if(elScript.hasAttributes()) {
                    var attr = elScript.attributes;
                    for(var j = 0, nbAttr = attr.length; j < nbAttr; ++j) {
                        scrp.setAttribute(attr[j].name, attr[j].value);
                    }
                }
                // Copy Content
                scrp.innerHTML = elScript.innerHTML;

                document.head.appendChild(scrp);
            }
        }
        var onSendError = function(value) {
            console.log('COOKIE CONSENT ERROR', value);
        }
        
        var params = `action=cookie_consent`;     
        urlUtils.ajaxPost(window.adminAjaxUrl, params, onSendSuccess, onSendError);  
    }
};

// Faster than onload, return the event when DOM is loaded (not the external files)
document.addEventListener('DOMContentLoaded', (evt) => {
    // console.log('document - DOMContentLoaded');
    window.App = App;
    App.init();
})

window.onload = function() {
    // console.log('window.onload - finished'); 
    App.loadComplete();
};
