
function getStyles(el) {
    let style = el.currentStyle || window.getComputedStyle(el);
    return style;
}
  


module.exports = {
    getStyles: getStyles
};