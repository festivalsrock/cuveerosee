import flatpickr from 'flatpickr';
import { French } from "flatpickr/dist/l10n/fr.js";

var BunnyValidation = require('../bunnyjs/Validation');
var moment = require('moment');


var init = function(elForm, validCallback) {

  // console.log('FormUtils init()');

  BunnyValidation.Validation.lang = {
    required: '!',
    email: '!',
    url: '!',
    tel: '!',
    maxLength: '!',
    minLength: '!',
    maxFileSize: '!',
    image: '!',
    minImageDimensions: '!',
    maxImageDimensions: '!',
    requiredFromList: '!',
    confirmation: '!',
    minOptions: '!'
  };

  // BunnyValidation.Validation.init(elForm, true);
  BunnyValidation.Validation.initInline(elForm);

  /*
  // Radio buttons
  function initRadioButtons(radio) {
    console.log('INIT RADIO BUTTONS', radio);
    for (let k = 0 ; k < radio.length; k++) {
      radio[k].checked = false;
      // radio[k].addEventListener('change', function() {
      //   console.log(`${this.name} = ${this.value}`);
      // });
    }
  }
  // initRadioButtons(elForm.elements.custom_newsletter);

  if(elForm.elements.custom_accept) {
    // initRadioButtons(elForm.elements.custom_accept);    
  }

  console.log('ELEMENTS', elForm.elements);
  console.log('ELEMENTS 2', elForm.querySelectorAll('input[type=checkbox]'));
  // var aInputOptin = elForm.elements
*/
  
  // const inputs = BunnyValidation.Validation.ui.getInputsInSection(node);
  // inputs.forEach(input => {
  //     input.addEventListener('change', () => {
  //         this.checkInput(input).catch(e => {});
  //     })
  // })


  // DATE : INPUT TYPE DATE // Not working in Safari...
  var elDate = elForm.querySelector('[type="date"]');
  if(elDate) {
    var isDateValid = false;
    elDate.addEventListener('blur', function(e) {
      var dateRegEx = /([0-9]{4}-[0-9]{2}-[0-9]{2})/g;
      if(elDate.checkValidity() == false) {
        elDate.parentNode.classList.add('has-danger');
      } else if (elDate.value.match(dateRegEx) == null) {      
        isDateValid = false;
        elDate.parentNode.classList.add('has-danger');
      } else {
        isDateValid = false;
        var sYears = moment(elDate.value).fromNow(true);
        if(sYears.indexOf(' years') > -1) {
          var nYears = sYears.split(' years')[0];
          if(nYears >= 18 && nYears <= 120) {
            elDate.parentNode.classList.remove('has-danger');  
            isDateValid = true;
          } else {          
            elDate.parentNode.classList.add('has-danger');
          }
        } else {
          elDate.parentNode.classList.add('has-danger');
        }      
      }
    });
  }

  
  // DATE : INPUT TEXT
  var date = moment();
  var youngDate = date.subtract(18, 'years');
  // console.log(date);
  // console.log(youngDate.format('Y-MM-DD'));


  var lang = document.documentElement.getAttribute('lang').split('-')[0];
  if(lang == 'fr') {
      flatpickr.localize(French);
  }

  var elDate = elForm.querySelector('.form-control-date');
  var pickr = flatpickr(elDate, {
      altInput: true,
      altFormat: lang == 'fr' ? 'd-m-Y' : 'Y-m-d',
      dateFormat: 'Y-m-d',
      minDate: '1900-01-01',
      maxDate: youngDate.format('Y-MM-DD'),
      allowInput: true,
      disableMobile: false });

  pickr._input.addEventListener('blur', (evt)=>{
      var s = evt.target.value;
      s = s.replace(/[^0-9]/g, '-');
      pickr.setDate(s, true, lang == 'fr' ? 'd-m-Y' : 'Y-m-d');
      // if (evt.currentTarget.value.length < 1) {
      //   elForm.querySelector('.birthday-wrapper').classList.remove('focus');
      // }
  });

  // pickr._input.addEventListener('focus', (evt)=>{    
  //   elForm.querySelector('.birthday-wrapper').classList.add('focus');
  // });


  pickr._input.addEventListener('keyup', (evt)=>{
      var s = evt.target.value;
      s = s.replace(/[^0-9]/g, '-');
      pickr._input.value = s;
  });




  const submitBtn = elForm.querySelector('[type="submit"]');
  submitBtn.disabled = false;
  
  elForm.addEventListener('submit', e => {
    e.preventDefault();
    
    // TweenMax.to(submitBtn, 0.2, {autoAlpha:0});

    // Check if form is valid then fill the database
    BunnyValidation.Validation.validateSection(elForm).then(result => {
      // [].forEach.call(submitBtns, submitBtn => {
      // });
      /*if(isDateValid == false) {
        
      } else */if (result === true) {
        // submitBtn.disabled = true;
        validCallback(elForm);
      } else {
        console.error('!!! FORM HAS ERROR !!!');
        // BunnyValidation.Validation.focusInput(result[0]);
      }
    }).catch(err => {
      console.error('!!! FORM HAS BUG !!!', err);
    });
  });
  
}

var showError = function(input) {
  input.parentNode.classList.add('has-danger');
}

var reset = function(elForm) {
  elForm.reset();
  var errors = elForm.querySelectorAll('.has-danger');
  for(var i = 0, nb = errors.length; i<nb; ++i){
    // console.log('Remove danger', errors[i]);
    errors[i].classList.remove('has-danger');
  }
  const submitBtn = elForm.querySelector('[type="submit"]');
  submitBtn.disabled = false;
}

function getIsRadioButtonSelected (radioButtons) {
  for(var i = 0; i < radioButtons.length; i++) {
    var button = radioButtons[i];
    if(button.checked) {
      return true;
    }
  }
  return false;
}


module.exports = {
  init,
  reset,
  showError
};