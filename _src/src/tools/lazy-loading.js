// Taken from
// https://www.sitepoint.com/five-techniques-lazy-load-images-website-performance/#robin-osborne-progressively-enhanced-lazy-loading

var detectIt = require('detect-it');
var imagesLoaded = require('imagesloaded');
var scrollUtils = require('./scroll-utils');


var lazy = [];
var debug;
var isPortrait = false;
var init = function(isPortrait) {
    
    // Everywhere but home, because swiper js handles lazy loading
    // if(document.getElementsByClassName('js-home-swiper').length == 0) {
        // console.log('Lazy Loading init()');

    
        isPortrait = isPortrait;

        debug = document.createElement('div');
        debug.setAttribute('id', 'debug');
        debug.style.position = 'fixed';
        debug.style.width = '70%';
        debug.style.height = '50vh';
        debug.style.top = '0';
        debug.style.overflow = 'scroll';
        debug.style.backgroundColor = 'rgba(255, 0, 0, 0.5)';
        debug.style.fontSize = '10px';
        debug.style.zIndex = '9999';
        debug.innerHTML = "";
        // document.body.appendChild(debug);
        debug.innerHTML += document.body.getAttribute('class')+'<br/>';

        lazy = [];
        
        var lazyLoadTimer = (evt) => {
            lazy = document.getElementsByClassName('lazy');
            if(lazy.length == 0) {
                window.removeEventListener('scroll', lazyLoadTimer);            
            } else {
                lazyLoad();        
            }
        }
}

var lazyLoad = function() {
    lazy = document.querySelectorAll('.lazy');
    if(lazy.length == 0) {
        console.log('SHOULD REMOVE LAZY LOADING ON SCROLL');         
        return false;        
    } else {
        
        // debug.innerHTML += document.body.getAttribute('class')+'<br/>';

        for(let i=0, nb = lazy.length; i<nb; i++){
            let myImgLazy = lazy[i];
            // console.log('is in viewport',scrollUtils.isInViewport(myImgLazy.getBoundingClientRect()));
            if(scrollUtils.isInViewport(myImgLazy.getBoundingClientRect())){
                // debug.innerHTML += '<u>in viewport</u> '+myImgLazy.src+'<br/>';
                if (myImgLazy.getAttribute('data-src')){
                    
                    var tmpImage = new Image();
                    tmpImage.onload = function() {
                        myImgLazy.src = this.src;
                        // console.log('Image loaded',myImgLazy.src);
                        myImgLazy.classList.remove('lazy');
                        myImgLazy.classList.add('lazy-loaded');

                        // debug.innerHTML += 'Loaded : '+myImgLazy.src+'<br/>';
                    }
                    tmpImage.src = myImgLazy.getAttribute('data-src');
                    // myImgLazy.removeAttribute('data-src');
                    // debug.innerHTML += '--> Load : '+tmpImage.src+'<br/>';
                    // console.log('Image start',myImgLazy.src);

                    // imagesLoaded(tmpImage, {ref: myImgLazy}, (instance) => {
                    //     console.log(instance.options.ref);
                    //     let el = instance.options.ref;
                    //     // let el = instance.elements[0];
                    //     el.src = instance.options.ref.src;
                    //     el.classList.remove('lazy');
                    //     el.classList.add('lazy-loaded');
                    // });
                } else if(myImgLazy.getAttribute('data-background')) {
                    
                    var tmpImage = new Image();
                    tmpImage.onload = function() {
                        myImgLazy.style.backgroundImage = `url(${this.src})`;
                        // console.log('Image loaded',myImgLazy.src);
                        myImgLazy.classList.remove('lazy');
                        myImgLazy.classList.add('lazy-loaded');
                    }
                    tmpImage.src = myImgLazy.getAttribute('data-background');
                    // myImgLazy.removeAttribute('data-background');

                    // imagesLoaded(myImgLazy, {background: true}, (instance) => {
                    //     let el = instance.elements[0];
                    //     el.classList.remove('lazy');
                    //     el.classList.add('lazy-loaded');
                    // });
                }
            }
        }       
    }
    // cleanLazy();
    return true;
}


module.exports = {
    init,
    lazyLoad
};