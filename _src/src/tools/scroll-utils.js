import {TweenMax, Power0, Power1} from 'gsap';
import {ScrollToPlugin} from '../gsap/plugins/ScrollToPlugin';
// const {ScrollToPlugin} = require('../gsap/plugins/ScrollToPlugin');

var detectIt = require('detect-it');

var nbStackFunction, aStackFunction;
var nPreviousScrollPositionY;
var nScrollDirection = 0;
var isDesktop;
var nWindowYOffset;

/**
 * This utility adds optimized scroll and resize events
 */
var init = function(isDesktop) {
    
    // console.log('Scroll Utils init()');

    ScrollToPlugin;

    nbStackFunction = 0;
    aStackFunction = [];
    isDesktop = isDesktop || false;

    // if(!isDesktop) {
        nPreviousScrollPositionY = window.pageYOffset;
    
        var scrollTimerId;
        var timerEnded = true;        
        
        function update(evt) {
            for(var i=0; i < nbStackFunction; ++i) {
                aStackFunction[i](evt);
            }
        }

        var scrollTimer = (evt) => {
            scrollDirection();
            update(evt);
        }

        var onOrientationChange = (evt) => {
            window.location.reload();
            // update(evt);
        }
        
        registerListenerDoc('scroll', scrollTimer);
        registerListener('resize', scrollTimer);
        registerListener('touchmove', scrollTimer);
        registerListener('touchstart', scrollTimer);
        registerListener('orientationchange', onOrientationChange);

        function registerListener(event, func) {
            window.addEventListener(event, func, detectIt.passiveEvents ? {passive:true} : false);
        }
    
        function registerListenerDoc(event, func) {
            document.addEventListener(event, func, detectIt.passiveEvents ? {passive:true} : false);
        }
    // }



    if (!window.requestAnimationFrame) {
        window.requestAnimationFrame = (function() {
            return window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || window.oRequestAnimationFrame || window.msRequestAnimationFrame || function(callback) {
                window.setTimeout(callback, 1000 / 60);
            };
        })();
    }


}

function scrollDirection() {
    nWindowYOffset = window.pageYOffset;
    if(nWindowYOffset <= 0) {
        nScrollDirection = 0;
    } else if(nPreviousScrollPositionY < nWindowYOffset){
        nScrollDirection = 1;
    } else if(nPreviousScrollPositionY > nWindowYOffset) {
        nScrollDirection = -1;
    }
    console.log(`scrollDirection ${nScrollDirection}`);    
}

function isScrollingUp() {
    return nScrollDirection == -1;
}
function isScrollingDown() {
    return nScrollDirection == 1;
}

// rect = el.getBoundingClientRect();    
function isInViewport(rect){
    // var rect = el.getBoundingClientRect();   
    // is 100% inside viewport 
    // return (
    //     rect.bottom >= 0 && 
    //     rect.right >= 0 && 
    //     rect.top <= (window.innerHeight || document.documentElement.clientHeight) && 
    //     rect.left <= (window.innerWidth || document.documentElement.clientWidth)
    // );
    // const vertInView = (rect.top <= (window.innerHeight || document.documentElement.clientHeight)) && ((rect.top + rect.height) >= -100);
    const vertInView = (rect.top <= (window.innerHeight || document.documentElement.clientHeight));
    return (
        vertInView
    );
}

/**
 * 
 * @param {*} fn must return true or false, to check if listener should be removed
 */
function addCallbackFunction(fn) {
    if(typeof fn == 'function') {
        fn();
        aStackFunction.push(fn);
        nbStackFunction = aStackFunction.length;
        // console.log(`Add function callback to scroll : ${fn.name}`);
    }
}

// Not tested
function removeCallbackFunction(fn) {
    var index = aStackFunction.indexOf(fn);
    if (index > -1) {
        aStackFunction.splice(index, 1);
    }
    nbStackFunction = aStackFunction.length;
}

function listCallbackFunction() {
    console.log('Functions called by scroll event :');
    for(var i=0, nb = aStackFunction.length; i < nb; ++i) {
        console.log(aStackFunction[i].name);
    }
}

function disableScroll() {
    // call your pop up and inside that function add below
    // $('body').on('scroll mousewheel touchmove', function(e) {
    //     e.preventDefault();
    //     e.stopPropagation();
    //     return false;
    // });
}

var bDisableSmoothScroll = false;
function enableSmoothScroll() {
	document.addEventListener('mousewheel', smoothScroll, false);
}

function disableSmoothScroll() {
    document.removeEventListener('mousewheel', smoothScroll);
}

function smoothScroll(evt) {
    evt.preventDefault();
    // console.log('bDisableSmoothScroll', bDisableSmoothScroll);
    if(!bDisableSmoothScroll) {
        TweenLite.to(window, 0.4, {scrollTo: window.scrollY + (evt.deltaY * 1.5), ease:Power1.easeOut});
    }
}

function setDisableSmoothScroll(b) {
    bDisableSmoothScroll = b;
}

/**
    Use it by calling new SmoothScroll(target,speed,smooth)
    ex: new SmoothScroll(document,120,12)
    Arguments:
        target: the element to be scrolled smoothly - can be a div or document
        speed: the amout of pixels to be scrolled per mousewheel step
        smooth: the smoothness factor, the higher the value, the more smooth.

        Fonctionne pas sur MAC
        Probleme quand utilise la scroll barre
 */

/*function SmoothScroll(target, speed, smooth) {
	if (target == document)
        target = (document.documentElement || document.body.parentNode || document.body) // cross browser support for document scrolling
        
	var moving = false;
	var pos = target.scrollTop;
	target.addEventListener('mousewheel', scrolled, false);
	target.addEventListener('DOMMouseScroll', scrolled, false);

	function scrolled(e) {
        e.preventDefault(); // disable default scrolling
        console.log(e);
		var delta = e.delta || e.wheelDelta;
		if (delta === undefined) {
			//we are on firefox
			delta = -e.detail;
        }
        delta = Math.max(-1, Math.min(1, delta)); // cap the delta to [-1,1] for cross browser consistency
        
		pos += -delta * speed;
		pos = Math.max(0, Math.min(pos, target.scrollHeight - target.clientHeight)); // limit scrolling

		if (!moving) update();
	}

	function update() {
		moving = true;
		var delta = (pos - target.scrollTop) / smooth;
        target.scrollTop += delta;
        
        // console.log(`update ${pos}, ${delta}`);

        for(var i=0; i < nbStackFunction; ++i) {
            aStackFunction[i]();
        }

		if (Math.abs(delta) > 0.5)
			requestFrame(update);
		else
			moving = false;
	}
}*/



var requestFrame = function() { // requestAnimationFrame cross browser
    return (
        window.requestAnimationFrame ||
        window.webkitRequestAnimationFrame ||
        window.mozRequestAnimationFrame ||
        window.oRequestAnimationFrame ||
        window.msRequestAnimationFrame ||
        function(func) {
            window.setTimeout(func, 1000 / 50);
        }
    );
}();

/* // Function originale
function SmoothScroll(target, speed, smooth) {
	if (target == document)
		target = (document.documentElement || document.body.parentNode || document.body) // cross browser support for document scrolling
	var moving = false
	var pos = target.scrollTop
	target.addEventListener('mousewheel', scrolled, false)
	target.addEventListener('DOMMouseScroll', scrolled, false)

	function scrolled(e) {
		e.preventDefault(); // disable default scrolling
		var delta = e.delta || e.wheelDelta;
		if (delta === undefined) {
			//we are on firefox
			delta = -e.detail;
        }
		delta = Math.max(-1, Math.min(1, delta)) // cap the delta to [-1,1] for cross browser consistency
        
        console.log('delta:', delta);
		pos += -delta * speed
		pos = Math.max(0, Math.min(pos, target.scrollHeight - target.clientHeight)) // limit scrolling

		if (!moving) update()
	}

	function update() {
		moving = true
		var delta = (pos - target.scrollTop) / smooth
		target.scrollTop += delta
		if (Math.abs(delta) > 0.5)
			requestFrame(update)
		else
			moving = false
	}

	var requestFrame = function() { // requestAnimationFrame cross browser
		return (
			window.requestAnimationFrame ||
			window.webkitRequestAnimationFrame ||
			window.mozRequestAnimationFrame ||
			window.oRequestAnimationFrame ||
			window.msRequestAnimationFrame ||
			function(func) {
				window.setTimeout(func, 1000 / 50);
			}
		);
	}()
}
*/




 
module.exports = {
    init: init,
    add: addCallbackFunction,
    remove: removeCallbackFunction,
    list: listCallbackFunction,
    isInViewport: isInViewport,
    isScrollingUp: isScrollingUp,
    isScrollingDown: isScrollingDown,
    enableSmoothScroll,
    disableSmoothScroll,
    setDisableSmoothScroll
};

