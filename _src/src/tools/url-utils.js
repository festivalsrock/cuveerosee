
// ?name=test
// getSearchParam('name')  // return 'test'
function getSearchParam (sVar) {
    return unescape(window.location.search.replace(new RegExp("^(?:.*[&\\?]" + escape(sVar).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1"));
}
  

/**
var xhr = urlUtils.getXMLHttpRequest();
    xhr.open('POST', window.adminAjaxUrl, true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xhr.onreadystatechange = function() {
        if(xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 0)) {
            console.log('COOKIE LEGAL RESPONSE', xhr);
        }
    };
	xhr.send('action=legal_cookie');
**/

function getXMLHttpRequest() {
	var xhr = null;
	if (window.XMLHttpRequest || window.ActiveXObject) {
		if (window.ActiveXObject) {
			try {
				xhr = new ActiveXObject("Msxml2.XMLHTTP");
			} catch(e) {
				xhr = new ActiveXObject("Microsoft.XMLHTTP");
			}
		} else {
			xhr = new XMLHttpRequest(); 
		}
	} else {
		alert("Votre navigateur ne supporte pas l'objet XMLHTTPRequest...");
		return null;
	}
	return xhr;
}

function ajaxPost(url, params, callback, callbackErr) {
	var xhr = this.getXMLHttpRequest();
    xhr.open('POST', url, true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xhr.onreadystatechange = function() {
        if(xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 0)) {
			console.log('AJAX RESPONSE', xhr);
			callback(xhr);
		} /*else {
			callbackErr(xhr);
		}*/
    };
	xhr.send(params);
}

module.exports = {
    getSearchParam: getSearchParam,
	getXMLHttpRequest: getXMLHttpRequest,
	ajaxPost: ajaxPost
};