const platform = require('platform');

const VIDEO_INLINE = '0';
const VIDEO_INLINE_IOS = '1';
const NO_VIDEO_INLINE = '2';

var getMethodByPlatform = () => {
    var os = platform.os.toString();
    var s = '0';
    if(os.indexOf('iOS 6') > -1 || os.indexOf('iOS 7') > -1 || os.indexOf('Windows Phone 8.0') > -1)
        s = NO_VIDEO_INLINE;
    else if(os.indexOf('iOS 8') > -1 || os.indexOf('iOS 9') > -1)
        s = VIDEO_INLINE_IOS;
    else
        s = VIDEO_INLINE;
    // s = NO_VIDEO_INLINE;
    return s;
}

module.exports = {
	getType:getMethodByPlatform,
    VIDEO_INLINE:VIDEO_INLINE,
    VIDEO_INLINE_IOS:VIDEO_INLINE_IOS,
    NO_VIDEO_INLINE:NO_VIDEO_INLINE
}
