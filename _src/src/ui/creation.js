var init = function(isDesktop) {    
    
    if(document.getElementsByClassName('template-creation').length > 0) {        
        console.log('Creation init()');

        // For dropdown menu see in artist-list.js
        
        if(document.getElementsByClassName('dropdown')[0].style.display != 'none') {
            var lastScrollPosition = 0;
            var ticking = false;

            const elDropdownLabel = document.querySelectorAll('.dropdown .dropdown-label')[0];
            const elToGos = document.querySelectorAll('.goto-gabarit');            
            var nLength = elToGos.length;
            var i;

            var label = elDropdownLabel.innerText;
            var tempLabel = '';

            var nOffsetTop = 0;
            function onScroll(scrollPosition) {
                for(i = 0; i < nLength; ++i) {
                    nOffsetTop = elToGos[i].offsetTop;
                    if(nOffsetTop - scrollPosition - 40 > 0) {
                        // console.log('CURRENT ', elToGos[i]);
                        elDropdownLabel.innerText = elToGos[i].innerText;
                        break;
                    }
                }
            }

            window.requestAnimationFrame = window.requestAnimationFrame || window.mozRequestAnimationFrame || window.webkitRequestAnimationFrame || window.msRequestAnimationFrame;

            // console.log('ADD SCROLL EVENT');

            window.addEventListener('scroll', function(e) {
                // console.log('scroll', window.pageYOffset);
                lastScrollPosition = window.pageYOffset;
                if (!ticking) {
                    window.requestAnimationFrame(function() {
                        onScroll(lastScrollPosition);
                        ticking = false;
                    });
                }
                ticking = true;
            });

        }
        
    }
}

module.exports = {
    init: init
};