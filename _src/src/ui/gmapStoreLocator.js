import $ from '../tools/jquery-global';
import 'gmap3';
import 'selectric';
import PerfectScrollbar from 'perfect-scrollbar';
// import { showUIExtra, disableSliderContainer, enableSliderContainer } from './utils';
// import { goToSection } from './menu';

$.gmap3(false);

const selectric = require('selectric')();

const loadGoogleMapsAPI = require('load-google-maps-api');

const themeGmap = [
  {
    featureType: "administrative",
    elementType: "all",
    stylers: [
      {
        saturation: "-100"
      }
    ]
  },
  {
    featureType: "administrative.province",
    elementType: "all",
    stylers: [
      {
        visibility: "off"
      }
    ]
  },
  {
    featureType: "landscape",
    elementType: "all",
    stylers: [
      {
        saturation: -100
      },
      {
        lightness: 65
      },
      {
        visibility: "on"
      }
    ]
  },
  {
    featureType: "poi",
    elementType: "all",
    stylers: [
      {
        saturation: -100
      },
      {
        lightness: "50"
      },
      {
        visibility: "off"
      }
    ]
  },
  {
    featureType: "road",
    elementType: "all",
    stylers: [
      {
        saturation: "-100"
      }
    ]
  },
  {
    featureType: "road.highway",
    elementType: "all",
    stylers: [
      {
        visibility: "simplified"
      }
    ]
  },
  {
    featureType: "road.arterial",
    elementType: "all",
    stylers: [
      {
        lightness: "30"
      }
    ]
  },
  {
    featureType: "road.local",
    elementType: "all",
    stylers: [
      {
        lightness: "40"
      }
    ]
  },
  {
    featureType: "transit",
    elementType: "all",
    stylers: [
      {
        saturation: -100
      },
      {
        visibility: "simplified"
      }
    ]
  },
  {
    featureType: "water",
    elementType: "geometry",
    stylers: [
      {
        hue: "#ffff00"
      },
      {
        lightness: -25
      },
      {
        saturation: -97
      }
    ]
  },
  {
    featureType: "water",
    elementType: "labels",
    stylers: [
      {
        lightness: -25
      },
      {
        saturation: -100
      }
    ]
  }
];

let _mapActive = false;
let _wsModalOpened = false;
let _sl;

class StoreLocator {
  constructor(el) {
    this.el  = el;
    this.minZoomLevel = 4;
    this.apiKey = this.el.querySelector('.gmap-container').getAttribute('data-key');
    this._map = null;
    this.background = this.el.querySelector('.background-wrapper');
    // this.video = document.getElementById('storeLocatorVideo');
    // this.video.addEventListener('play', this._playVideoHandler.bind(this), false);
    console.log('new StoreLocator');
    this.init();
  }

  init() {
    // this.video.load();
    this._initBlocWebstores();
    window.addEventListener('resize', this.resize.bind(this));
    // const containerSlider = document.querySelector('.slider-container');
    // containerSlider.addEventListener('scroll', (event) => {
    //   if (this.mainSearchInput) {
    //     this.mainSearchInput.blur();
    //   }
    // }, false);
    loadGoogleMapsAPI({
      key: this.apiKey,// 'AIzaSyA_bsIwGApmqolpPS8VI8Ss0g478nAGFuk',
      libraries: ['places'],
      language: window.lang || 'fr',
      v: 3
    })
      .then(googleMaps => {
        // console.log(googleMaps);
        this.markersShop = [];
        this._initMap();
      })
      .catch(err => {
        console.error(err);
      });
  }

  _initMap() {
    // console.log('###### INIT MAP ######', $('.gmap-container'));
    this.mainSearchInput = document.getElementById('main-search-input');
    this.mainSearchBox = new google.maps.places.Autocomplete(this.mainSearchInput);
    this.resultSearchInput = document.getElementById('result-search-input');
    this.resultSearchBox = new google.maps.places.Autocomplete(this.resultSearchInput);
    this.submitSearchButton = document.getElementById('submitSearchButton');
    this.geocoder = new google.maps.Geocoder();
    document.getElementById('submitSearchButton').addEventListener('click', (event) => {
      // console.log(this.mainSearchInput.value);
      
      this.geocodeAddress(this.mainSearchInput.value);
    });
    $('.gmap-container')
      .gmap3({
        address: 'France',
        zoom: 6,
        minZoom: this.minZoomLevel,
        mapTypeId: 'laup',
        mapTypeControl: false,
        disableDefaultUI: false,
        navigationControl: false,
        scaleControl: true,
        scrollwheel: true,
        streetViewControl: false,
        fullscreenControl: false,
        gestureHandling: 'cooperative'
      })
      .then((map) => {
        console.log('MAP', map);
        this._map = map;

        this._populateMarkersShop();
        this._renderMarkersShop();

        this.mainSearchBox.bindTo('bounds', this._map);
        this.mainSearchBox.addListener('place_changed', (event) => {
          const place = this.mainSearchBox.getPlace();
          this.resultSearchInput.value = this.mainSearchInput.value;
          this._gotoPlace(place);
        });
        this.resultSearchBox.bindTo('bounds', this._map);
        this.resultSearchBox.addListener('place_changed', (event) => {
          const place = this.resultSearchBox.getPlace();
          this._gotoPlace(place);
        });

        this.resultStore = this.el.querySelector('.result-search-bloc');
        this.resultStoreContent = this.resultStore.querySelector('.content');
        this.psContentResultStore = new PerfectScrollbar(this.resultStoreContent, {
          handlers: ['click-rail', 'drag-thumb', 'keyboard', 'wheel', 'touch'],
          suppressScrollX: true
        });
      })
      .on({
        drag: (map) => {
          this.checkLatitude(map);
        },
        dragend: (map) => {
          this.checkLatitude(map);
        },
        idle: (map) => {
          this.checkLatitude(map);
          this._getMarkersByBounds(map);
        },
        zoom_changed: (map) => {
          this.checkLatitude(map);
        },
      })
      .styledmaptype('laup', themeGmap, { name: 'laup' });

      this.el.querySelector('.close-map-button').addEventListener('click', (event) => {
        this.closeMap();
      });
  }

  closeMap() {
    this.el.querySelector('.container').classList.remove('hidden');
    this.el.querySelector('.map-container').classList.remove('show');
    // showUIExtra(true);
    // enableSliderContainer();
    _mapActive = false;
    // window.removeEventListener('resize', this.resize.bind(this));
    this.currentShopId = null; 
    document.querySelectorAll('.result-search-bloc .content ul li').forEach(item => {
      item.classList.remove('highlight');
    });
    
    this.cluster.markers().forEach(mc => {
      mc.setOpacity(1);
    });
  }

  geocodeAddress(address) {
    this.geocoder.geocode( { 'address': address }, (results, status) => {
      if (status == 'OK') {
        // console.log(
        //   results[0].geometry.location.lat(), results[0].geometry.location.lng()
        // );
        this.resultSearchInput.value = this.mainSearchInput.value;
        this._gotoPlace(results[0]);
      } else {
        alert('Geocode was not successful for the following reason: ' + status);
      }
    });
  }

  _gotoPlace(place) {
    if (place && place.geometry) {
      console.log(place);
      this.currentPlace = place;
      this.el.querySelector('.container').classList.add('hidden');
      this.el.querySelector('.map-container').classList.add('show');
      // goToSection(this.el, true, true);
      // showUIExtra(false);
      // disableSliderContainer();
      _mapActive = true;
      let radius = 100000;
      if (place.types.includes('country')) {
        radius = 500000;
      }
      if (place.types.includes('locality') || place.types.includes('postal_code')) {
        radius = 5000;
      }
      if (place.types.includes('street_address') || place.types.includes('route') || place.types.includes('neighborhood') || place.types.includes('point_of_interest')) {
        radius = 1000;
      }
      let nbShop = 0;
      this.markersShop.forEach((shop, idx) => {
        if (place.geometry.viewport.contains(shop.position)) {
          // console.log(shop);
          nbShop++;
        }
      });
      if (place.geometry.viewport && nbShop > 0) {
        this._map.fitBounds(place.geometry.viewport);
      } else {
        this._map.setCenter(place.geometry.location);
        // this._map.setZoom(17);
        const searchCircle = new google.maps.Circle({
          strokeColor: '#FF0000',
          strokeOpacity: 0.8,
          strokeWeight: 2,
          fillColor: '#FF0000',
          fillOpacity: 0.35,
          map: this._map,
          center: place.geometry.location,
          radius
        });
        this._map.fitBounds(searchCircle.getBounds());
        searchCircle.setMap(null);
      }
      
    }
  }

  _populateMarkersShop() {
    window.laup_map_stores.data.forEach((shop) => this._processShop(shop));
  }

  _processShop(shop) {
    this._addMarkerShop(shop);
  }

  _addMarkerShop(shop) {
    if (shop.lat !== '' && shop.lng !== '') {
      let url = '/data/themes/cuveerosee/images/pin.png';
      let size = new google.maps.Size(36, 52);

      if(window.devicePixelRatio > 1.5){
        url = '/data/themes/cuveerosee/images/pin@2x.png';
        size = new google.maps.Size(72, 104);
      }

      const icon = {
        url: url,
        anchor: new google.maps.Point(18, 52),
        size: new google.maps.Size(36, 52),
        scaledSize: new google.maps.Size(36, 52)
      };

      const marker = new google.maps.Marker({
        position: new google.maps.LatLng({lat : parseFloat(shop.lat), lng: parseFloat(shop.lng)}),
        id: shop.id + '-marker',
        tag: 'marker-shop',
        options: {
          icon: icon,
          clickable: true
        },
        data: shop,

      });
      this.markersShop.push(marker);
    }

  }

  _renderMarkersShop() {
    // console.log('RENDER MARKERS SHOPS ', this._map, this.markersShop);
    $('.gmap-container')
      .gmap3().cluster({
        size: 200,
        markers: this.markersShop,
        cb: (markers) => {
          if (markers.length > 1) { // 1 marker stay unchanged (because cb returns nothing)
            return {
              content: "<div class='cluster'>" + markers.length + "</div>",
              x: -20,
              y: -52
            };
          }
        }
      }).then( (cluster) => {
        // console.log(cluster.markers());
        this.cluster = cluster;
      }).on({
        click: (marker, clusterOverlay, cluster, event) => {
          // console.log('CLICK ', marker, clusterOverlay, cluster, cluster.markers());
          if (marker) {
            // console.log(marker);
            marker.setOpacity(1);
            this.currentShopId = marker.data.id; 
            document.querySelectorAll('.result-search-bloc .content ul li').forEach(item => {
              item.classList.remove('highlight');
            });
            if (document.getElementById(marker.data.id)) {
              document.getElementById(marker.data.id).classList.add('highlight');
              this.resultStoreContent.scrollTop = document.getElementById(marker.data.id).offsetTop;
            }
            cluster.markers().forEach(mc => {
              if (mc !== marker) {
                mc.setOpacity(0.5);
              }
            });
          } else if (clusterOverlay) {
            this.fitBoundsToMarkers(clusterOverlay.markers);
          }
        }
      });
  }

  fitBoundsToMarkers(markers) {
    const bounds = new google.maps.LatLngBounds();
    markers.forEach((marker, idx) => {
      bounds.extend( marker.getPosition() );
    });
    this._map.fitBounds(bounds);
  }

  _getMarkersByBounds(map, place) {
    // console.log('_getMarkersByBounds');
    const bounds = map.getBounds();
    let count = 0;
    const result = [];
    this.markersShop.forEach(marker => {
      if (bounds.contains(marker.position) === true) {
        count++;
        let dist = -1;
        if (this.currentPlace && this.currentPlace.geometry) {
          dist =  _getDistanceFromLatLonInKm(
            marker.getPosition().lat(), marker.getPosition().lng(),
            this.currentPlace.geometry.location.lat(), this.currentPlace.geometry.location.lng()
          );
          // console.log(marker.getPosition(), this.currentPlace.geometry, dist);
        }
        result.push({
          marker,
          dist
        });
      }
    });
    result.sort((a, b) => {
      return a.dist - b.dist;
    });
    const resultContainer = document.querySelector('.result-search-bloc .content ul');
    let resultHTML = '';
    result.forEach(({marker, dist}) => {
      if(marker.data.format == 'fr') {
        resultHTML += `
        <li id="${marker.data.id}" data-dist="${dist}">
          <address itemscope itemtype="http://schema.org/PostalAddress">
            <h3 itemprop="name">
                ${marker.data.name}
            </h3>
            <div itemprop="streetAddress">
                ${marker.data.address}<br />
                ${ (marker.data.address2) ? marker.data.address2 + '<br />' : '' }
            </div>
            <div>
              <span itemprop="postalCode">
              ${marker.data.zipcode}
              </span>
              <span itemprop="addressLocality">
              ${marker.data.city},
              </span>
              <span itemprop="addressCountry">
              ${marker.data.country_name}
              </span>
            </div>
            <div itemprop="telephone" content="${marker.data.phone_intl}">
              ${marker.data.phone}
            </div>
          </address>
        </li>
      `;
      }else { // Version US only
        resultHTML += `
        <li id="${marker.data.id}" data-dist="${dist}">
          <address itemscope itemtype="http://schema.org/PostalAddress">
            <h3 itemprop="name">
                ${marker.data.name}
            </h3>
            <div itemprop="streetAddress">
                ${marker.data.address}<br />
                ${ (marker.data.address2) ? marker.data.address2 + '<br />' : '' }
            </div>
            <div>
              <span itemprop="addressCountry">
              ${ (marker.data.country_name) ? marker.data.country_name : '' }
              </span>
            </div>
            <div itemprop="telephone" content="${marker.data.phone_intl}">
              ${ (marker.data.phone) ? marker.data.phone : '' }
            </div>
          </address>
        </li>
      `;
      }
    });
    resultContainer.innerHTML = resultHTML;
    if (this.currentShopId && document.getElementById(this.currentShopId)) {
      document.getElementById(this.currentShopId).classList.add('highlight');
    }
    resultContainer.querySelectorAll('li').forEach(item => {
      item.addEventListener('click', (event) => {
        event.preventDefault();
        // console.log(item.id);
        this.currentShopId = item.id;
        document.querySelectorAll('.result-search-bloc .content ul li').forEach(li => {
          li.classList.remove('highlight');
        });
        // console.log(this.markersShop);
        this.cluster.markers().forEach( ms => {
          // console.log('-------- ' + ms.data.id);
          if (ms.data.id === item.id) {   
            ms.setOpacity(1);
            item.classList.add('highlight');
            this._map.setCenter(ms.position);
            this._map.setZoom(15);
          } else {
            ms.setOpacity(0.5);
          }
        });
      });
    });
    if (this.psContentResultStore) {
      this.psContentResultStore.update();
    }
  }

  _initBlocWebstores() {
    const blocWS = this.el.querySelector('.webstore-bloc');
    console.log('INIT WEBSOTRES', blocWS);
    if (blocWS) {
      const content = blocWS.querySelector('.content');
      this.psBlocWS = new PerfectScrollbar(content, {
        handlers: ['click-rail', 'drag-thumb', 'keyboard', 'wheel', 'touch'],
        suppressScrollX: true
      });
      this.WSmodal = this.el.querySelector('.webstores-modal');
      $('#country-select-ws').selectric({
        maxHeight: 200,
        onInit: () => {
          const modalContent = this.WSmodal.querySelector('.content');
          this.psModalWS = new PerfectScrollbar(modalContent, {
            handlers: ['click-rail', 'drag-thumb', 'keyboard', 'wheel', 'touch'],
            suppressScrollX: true
          });
        }
        // ,onChange: (country) => {
        //   console.log('SELECTRIC', country);
        //   this.selectWSCountry($('#country-select-ws option:selected').val());
        // }
      });
      $('#country-select-ws').on('change', (country) =>{
        console.log('SELECTRIC', country);
        this.selectWSCountry($('#country-select-ws option:selected').val());
      });
      document.getElementById('see-all-webstore-button').addEventListener('click', this.openWSModal.bind(this));
      this.WSmodal.querySelector('.close-modal-button').addEventListener('click', this.closeWSModal.bind(this));
    }
  }

  openWSModal(event) {
    console.log('OPEN WS Modal');
    if (event) {
      event.preventDefault();
    }
    _wsModalOpened = true;
    this.WSmodal.classList.add('js-modal-open');

    document.body.classList.add('no-scroll');
    
    console.log(this.WSmodal);
    // showUIExtra(false);
    if ( window.lang ) {
      const countries = document.querySelectorAll('select[name="country"] option');
      // console.log(countries);
      
      let lg_index = 0;
      countries.forEach((country, index) => {
        if (country.value === window.lang) {
          lg_index = index;
        }
      });
      $('#country-select-ws').prop('selectedIndex', 0).selectric('refresh');
      console.log('INIT SELECT COUNTRY', $('#country-select-ws option:selected').val());
      this.selectWSCountry($('#country-select-ws option:selected').val());
    }
  }

  closeWSModal(event) {
    if (event) {
      event.preventDefault();
    }
    
    document.body.classList.remove('no-scroll');
    _wsModalOpened = false;
    this.WSmodal.classList.remove('js-modal-open');
    // showUIExtra(true);
  }

  selectWSCountry(country) {
    console.log('SELECT COUNTRY:', country);
    $('.stores-list').hide();
    $(`.stores-list-${country}`).show();
    if (this.psModalWS) {
      this.psModalWS.update();
    }
    $('#country-select-ws').selectric('refresh');
  }

  _playVideoHandler(event) {
    // console.log('PLAY VIDEO STORE LOCATOR');
    this.el.querySelector('.background-wrapper').classList.add('video-playing');
  }


  checkLatitude(map) {
    if (this.minZoomLevel) {
        if (map.getZoom() < this.minZoomLevel) {
            map.setZoom(parseInt(this.minZoomLevel));
        }
    }

    var bounds = map.getBounds();
    var sLat = map.getBounds().getSouthWest().lat();
    var nLat = map.getBounds().getNorthEast().lat();
    if (sLat < -85 || nLat > 85) {
      //the map has gone beyone the world's max or min latitude - gray areas are visible
      //return to a valid position
      if (this.lastValidCenter) {
        map.setCenter(this.lastValidCenter);
      }
    }
    else {
      this.lastValidCenter = map.getCenter();
    }
  }

  resize(event) {
    // console.log('RESIZE', event, this);
    if (_mapActive || _wsModalOpened) {
      // showUIExtra(false);
    }
    if (this.psModalWS) {
      this.psModalWS.update();
    }
  }
}





function _getDistanceFromLatLonInKm(lat1,lon1,lat2,lon2) {
  function deg2rad(deg) {
    return deg * (Math.PI/180);
  }

  const R = 6371; // Radius of the earth in km
  const dLat = deg2rad(lat2-lat1);  // deg2rad below
  const dLon = deg2rad(lon2-lon1);
  const a =
      Math.sin(dLat/2) * Math.sin(dLat/2) +
      Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *
      Math.sin(dLon/2) * Math.sin(dLon/2)
    ;
  const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
  const d = R * c; // Distance in km
  return d;
}

function getMapActive() {
  return _mapActive;
}

function getWSModalOpened() {
  return _wsModalOpened;
}

function closeMap() {
  if (_sl) {
    _sl.closeMap();
  }
}

function closeModalWS() {
  if (_sl) {
    _sl.closeWSModal();
  }
}

// window.addEventListener('DOMContentLoaded', (event) => {
//   const section = document.getElementById('section_gmap-store-locator');
//   if (section) {
//     _sl = new StoreLocator(section);
//   }
// }, false);

function init() {
  const section = document.getElementById('section_gmap-store-locator');
  if (section) {
    _sl = new StoreLocator(section);
  }
}

export {
  init,
  getMapActive,
  getWSModalOpened,
  closeMap,
  closeModalWS
};
