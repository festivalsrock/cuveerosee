import Parallax from 'parallax-js';
import { TweenMax, Power2 } from 'gsap';

var init = function(isDesktop) { 

    if(document.getElementById('page-homepage')) {
    
        // console.log('Homepage init()');
        
        var scene = document.getElementById('parallax-mouse');
        var parallaxInstance = new Parallax(scene, {
            relativeInput: true,
            pointerEvents: true,
            scalarX:3,
            scalarY:2,
            frictionX:0.2
        });
        parallaxInstance.disable();

        TweenMax.to('.parallax-bg', 0.34, {opacity:1, delay:0.2});
        TweenMax.fromTo('.parallax-bottle', 1, {opacity:0, y:'+=20'}, {ease:Power2.easeOut, opacity:1, y:'-=20', delay: 0.5, onComplete:()=>{
            parallaxInstance.enable();
        }});
        TweenMax.fromTo('.parallax-text', 1, {opacity:0, y:'+=20'}, {ease:Power2.easeOut, opacity:1, y:'-=20', delay: 0.5});

    }


}

module.exports = {
    init: init
};

