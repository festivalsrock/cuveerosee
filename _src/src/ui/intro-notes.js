
import { TweenMax, CSSPlugin, Power2 } from 'gsap';

var init = function() {
    console.log('Intro Notes init()');

    if(document.getElementById('intro-notes')) {        
        setTimeout(() => {
            document.getElementById('intro-notes').classList.add('show');

            setTimeout(()=> {
                TweenMax.to(document.getElementById('intro-notes'), 1, {autoAlpha:0});
            }, 4000);
        }, 200);
    }
}


module.exports = {
    init: init
};