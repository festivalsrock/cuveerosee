
var urlUtils = require('../tools/url-utils');
var introNotes = require('./intro-notes');

import { TweenMax, CSSPlugin, Power2 } from 'gsap';

var init = function() {
    // console.log('Intro init()');

    if(document.getElementById('intro')) {
        var btYes = document.getElementById('bt-answer-yes');
        btYes.addEventListener('click', onAnswerYes);
    
        var btNo = document.getElementById('bt-answer-no');
        btNo.addEventListener('click', onAnswerNo);
    
        // var logo = document.getElementById('logo-50-transparent');
        setTimeout(() => {
            // logo.classList.add('play');
            document.getElementById('intro').classList.add('show');
        }, 1000);
    } else {
        introNotes.init();
    }    
}

var onAnswerYes = function() {
    TweenMax.to(document.getElementById('intro'), 1, {autoAlpha:0});
    introNotes.init();

    var xhr = urlUtils.getXMLHttpRequest();
    xhr.open('POST', window.adminAjaxUrl, true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xhr.onreadystatechange = function() {
        if(xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 0)) {
            // console.log('COOKIE LEGAL RESPONSE', xhr);
        }
    };
    xhr.send('action=legal_cookie');
}
var onAnswerNo = function() {
    // TweenLite.set((".answer"), {transition: 'none'});
    TweenMax.to(document.getElementById('question'), 0.5, {autoAlpha:0, delay:0});
    TweenMax.to(document.getElementById('bt-answer-yes'), 0.5, {autoAlpha:0, delay:0, overwrite:true, clearProps:'transition'});
    TweenMax.to(document.getElementById('bt-answer-no'), 0.5, {autoAlpha:0, delay:0, overwrite:true, clearProps:'transition'});
    TweenMax.to(document.getElementById('answer-no'), 0.5, {opacity:1, delay:0.5});
}


module.exports = {
    init: init
};