
import { TweenMax, CSSPlugin, Power2 } from 'gsap';

var init = function(isDesktop) {    
    
    if(document.getElementById('page-life')) {
        console.log('Life init()');

        var aFilters = document.getElementsByClassName('life-articles-filters')[0].querySelectorAll('.link');
        console.log(aFilters);
        for(var i = 0, nb = aFilters.length; i < nb; ++i) {
            aFilters[i].addEventListener('click', (e)=>{
                console.log(e);
                showArticle(e.target.dataset.filter);
            });
        }
        
        var elContainer = document.getElementsByClassName('life-articles-container');
        var aContainers = elContainer[0].querySelectorAll('.news');
        var nbContainers = aContainers.length;
        
        function showArticle(filter) {
            console.log('filter', filter);
            console.log(aContainers);

            TweenMax.fromTo(elContainer, 0.3, {y:'+=20', opacity:0}, {y:'-=20', opacity:1});

            // Update filter view            
            var el;
            for(var i = 0, nb = aFilters.length; i < nb; ++i) {
                el = aFilters[i];
                if(el.dataset.filter == filter) {
                    el.classList.add('selected');
                } else {
                    el.classList.remove('selected');
                }
            }

            // Update news list
            if(filter == '') {
                for(var i = 0; i < nbContainers; ++i) {
                    aContainers[i].classList.remove('hide');
                    // TweenMax.to(aContainers[i], 1.5, {alpha:1});
                }
            }
            else {
                for(var i = 0; i < nbContainers; ++i) {
                    if(aContainers[i].classList.contains(filter) == false) {
                        // Hide
                        aContainers[i].classList.add('hide');
                        // TweenMax.to(aContainers[i], 0.6, {delay: i*0.5, alpha:0, onCompleteParams:[aContainers[i]], onComplete:(el)=> {
                        //     el.classLtist.add('hide');
                        //     // el.parentNode.removeChild(el);
                        // }});
                    } else {
                        aContainers[i].classList.remove('hide');
                        // TweenMax.to(aContainers[i], 0.6, {delay: i*0.1, alpha:1});
                    }
                }
            }

        }
        
    }
}

module.exports = {
    init: init
};