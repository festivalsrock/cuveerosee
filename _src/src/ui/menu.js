

import { TweenMax, CSSPlugin, Power2 } from 'gsap';

var header, menu, body, lang;
var init = function() {
    // console.log('Menu init()');

    header = document.getElementById('header');
    
    menu = document.getElementById('menu-mobile');
    
    body = document.body;

    lang = document.getElementById('lang-selector');

    // Menu mobile
    var menuBurger = document.getElementsByClassName('js-menu-burger')[0];
    menuBurger.addEventListener('click', _openMenuBurger);
    var menuBurgerClose = document.getElementsByClassName('js-menu-close')[0];
    menuBurgerClose.addEventListener('click', _closeMenuBurger);
}

var _openMenuBurger = function(e) {
    var menuContent = document.getElementsByClassName('js-menu-content')[0];
    menuContent.classList.add('opened');
    header.classList.add('menu-opened');
    document.documentElement.classList.add('no-scroll');
    TweenMax.to(menuContent, 0.3, {autoAlpha:1});
    TweenMax.to(lang, 0.5, {autoAlpha:0});

    var showMe = menuContent.querySelectorAll('.show-me, .menu-mobile-ul .menu-item, .menu-mobile-ul-2');
    for(var i = 0, nb = showMe.length; i < nb; ++i) {
        TweenMax.fromTo(showMe[i], 0.4, {y :'+=10', opacity:0}, {delay:i*0.1 + 0.1, transition:Power2.easeIn, opacity:1, y: '-=10'});
    }

    TweenMax.fromTo('.menu-mobile-ul-3', 0.6, {x:'+=20', opacity:0}, {delay: showMe.length * 0.15, x:'-=20', opacity:1, transition:Power2.easeIn});

    TweenMax.fromTo(menuContent.querySelector('.social'), 0.3, {opacity:0}, {opacity:1, delay:1.3});
}

var _closeMenuBurger = function(e) {
    var menuContent = document.getElementsByClassName('js-menu-content')[0];
    menuContent.classList.remove('opened');
    header.classList.remove('menu-opened');
    document.documentElement.classList.remove('no-scroll');
    TweenMax.to(menuContent, 0.3, {autoAlpha:0});
    TweenMax.to(lang, 0.5, {autoAlpha:1});
}

var previousScrollY = 0;
var menuState = '';
var menuZone = '';
var nZoneDefault = (window.innerHeight * 0.15);
var nZoneScroll = (window.innerHeight * 0.3);
// console.log('nZoneScroll', nZoneScroll);

var checkScrollBehavior = function(evt) {
    // console.log(`Menu scroll behavior new (${window.pageYOffset}), old (${previousScrollY})`);
    if(window.pageYOffset < nZoneDefault) {
        if(menuZone != '') {
            body.classList.remove('scroll-zone');
        }
        menuZone = '';
    }
    else if(window.pageYOffset < nZoneScroll) {
        if(menuZone != 'scroll-zone') {
            body.classList.add('scroll-zone');
        }
        menuZone = 'scroll-zone';
    } else {
        if(menuZone != '') {
            body.classList.remove('scroll-zone');
        }
        menuZone = '';
    }
    
    if(window.pageYOffset < nZoneDefault) {
        if(menuState != '') {
            body.classList.remove('scroll-down');
            body.classList.remove('scroll-up');
        }
        menuState = '';
    } else if(previousScrollY < window.pageYOffset){
        if(menuState != 'scroll-down') {
            body.classList.add('scroll-down');
            body.classList.remove('scroll-up');
        }
        menuState = 'scroll-down';
    } else if(previousScrollY > window.pageYOffset) {
        if(menuState != 'scroll-up') {
            body.classList.remove('scroll-down');
            body.classList.add('scroll-up');
        }
        menuState = 'scroll-up';
    }
    previousScrollY = window.pageYOffset;
    return true;
}



module.exports = {
    init: init,
    checkScrollBehavior: checkScrollBehavior
};