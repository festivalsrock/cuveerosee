import { TweenMax, CSSPlugin, Power2 } from 'gsap';

import FormUtils from '../tools/form-utils';
import urlUtils from '../tools/url-utils';

var moment = require('moment');
 


var menu;
var init = function() {
    // console.log('Newsletter init()');

    var form = document.getElementById('newsletter-form');
    var modal = document.getElementById('modal-newsletter');
    
    
    var onFormValid = function(form) {

        // console.log(form.lastname.value);
        // console.log(form.firstname.value);
        // console.log(form.birthday.value);
        // console.log(form.zipcode.value);
        // console.log(form.country.value);
        // console.log(form.email.value);
        // console.log(form.custom_newsletter.checked); // true
        // console.log(form.lang.value);

        var birthday = moment(form.birthday.value)

        var params = `action=mailchimp_subscribe`;
        params += `&lastname=${form.lastname.value}`;
        params += `&firstname=${form.firstname.value}`;
        // params += `&birthday=${form.birthday.value}`;
        params += `&birthday_day=${birthday.date()}`;
        params += `&birthday_month=${birthday.month() + 1}`;
        params += `&birthday_year=${birthday.year()}`;
        params += `&zipcode=${form.zipcode.value}`;
        params += `&country=${form.country.value}`;
        params += `&email=${form.email.value}`;
        params += `&custom_newsletter=${form.custom_newsletter.checked ? 1 : 0}`;
        params += `&lang=${form.lang.value}`;
        
        console.log(params);
        urlUtils.ajaxPost(window.adminAjaxUrl, params, onSendSuccess, onSendError);
    }
    
    FormUtils.init(form, onFormValid);
    var onSendSuccess = function(value) {
        console.log('ON SEND SUCCESS', value.response);
        switch(value.response) {
            case 'ok' :
                showSuccessMessage();
            break;
            case 'error_already_subscribed' :
                showErrorMessage('error_already_subscribed');
            break;
            case 'error_invalid_email' :
                FormUtils.showError(document.getElementById('email'));
                showErrorMessage('error_invalid_email');
            break;
            case 'error_age_legal' :
                showErrorMessage('error_age_legal');
            break;
            case 'error_global' :
            default : 
                showErrorMessage('error_global');
        }
    }
    var onSendError = function(value) {
        console.log('ON SEND ERROR', value);
    }

    var showSuccessMessage = function() {
        TweenMax.to(document.getElementById('newsletter-message-success'), 0.5, {autoAlpha:1});
        TweenMax.to(document.getElementById('newsletter-content'), 0.5, {autoAlpha:0});
    }

    var showErrorMessage = function(s) {
        var aResponse = form.querySelectorAll('.error');

        for(var i=0, nb = aResponse.length; i < nb; ++i) {
            aResponse[i].classList.remove('show');
        }

        form.querySelector('.error.'+s).classList.add('show');
    }

    // showErrorMessage('error_already_subscribed');
    // showErrorMessage('error_age_legal');
    // showErrorMessage('error_global');
    // showErrorMessage('error_invalid_email');
    // setTimeout(()=>{
    //     FormUtils.showError(document.getElementById('email'));
    // },2000);

    var hideErrorMessage = function() {
        
    }

    var showDefaultForm = function() {
        TweenMax.to(document.getElementById('newsletter-message-success'), 0.0, {autoAlpha:0});
        TweenMax.to(document.getElementById('newsletter-content'), 0.0, {autoAlpha:1});
    }

    var closeModal = function() {
        modal.classList.remove('open');
        document.body.classList.remove('no-scroll');
        TweenMax.to(modal, .5, {autoAlpha:0, onComplete:()=>{
            FormUtils.reset(form);
        }});
    }

    var openModal = function () {
        showDefaultForm();
        // document.getElementById('lastname').focus();
        modal.classList.add('open');
        document.body.classList.add('no-scroll');
        TweenMax.to(modal, .5, {autoAlpha:1});
    }

    document.getElementById('bt-close-newsletter').addEventListener('click', function(e) {
        closeModal();
    });

    var btModalBg = document.getElementById('modal-newsletter');
    btModalBg.addEventListener('click', function(e) {
        if(e.target == btModalBg) {
            closeModal();
        }
    });

    // openModal();
    document.getElementById('show-modal-newsletter').addEventListener('click', function(ev) {
        ev.preventDefault();
        openModal();
        return false;
    }, false);
}


module.exports = {
    init: init
};