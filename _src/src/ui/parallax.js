var ScrollMagic = require('scrollmagic');
var URLUtils = require('../tools/url-utils');
import 'scrollmagic/scrollmagic/uncompressed/plugins/debug.addIndicators';
import 'scrollmagic/scrollmagic/uncompressed/plugins/animation.gsap';
import {TweenLite, Power0, Power1} from 'gsap';
// import {ScrollToPlugin} from 'gsap/ScrollToPlugin';

var init = function(isDesktop) { 

    // SCROLL APPARITION, ONLY DESKTOP
    if(isDesktop) {    
        // console.log('Parallax init()');
        var controller = new ScrollMagic.Controller({addIndicators: false});        
        var scene;
        [].forEach.call(document.getElementsByClassName('parallax'), (item, id)=>{
            // console.log('Parallax on el ', id);
            var bIsSlideIn = item.classList.contains('slide-in');
            var bIsFadeIn = item.classList.contains('fade-in');
            
            scene = new ScrollMagic.Scene({
                triggerElement: item,
                duration: '100%',	// the scene should last for a scroll distance of 100px 
                offset: '-0%',		// start this scene after scrolling for 50px 
                triggerHook: 0.73
            })
            .on('enter', (evt) => {
                let nDelay = 0;
                if(item.dataset.parallaxDelay) {
                    nDelay = item.dataset.parallaxDelay;
                }

                if(bIsSlideIn) {
                    // console.log('SLIDE IN', evt);
                    TweenLite.to(item, 0.6, {autoAlpha: 1, y: 0, delay: nDelay, ease:Power1.easeOut});
                }
                else if(bIsFadeIn) {
                    // console.log('FADE IN', evt);
                    TweenLite.to(item, 0.7, {autoAlpha: 1, delay: nDelay, ease:Power1.easeOut});
                }
            })
            .on('update', (evt) => {
                // console.log(`scroll update, progress ${evt.progress}`, item);
                // TweenLite.to(item, 0.6, {autoAlpha: 1, y: 0, delay: nDelay, ease:Power1.easeOut});
            })
            .on('progress', (evt) => {
                // var decalY = (0.5 - evt.progress) * 100; // Decal par rapport au milieu de l'écran, sur un déplacement de 50px
                // console.log(`scroll progress ${evt.progress}, decalY ${decalY}`, item);
                TweenLite.to(item, 0.5, {y: (0.5 - evt.progress) * 100, ease:Power1.easeOut});
            })
            // .setTween(TweenLite.to(item, 1, {autoAlpha: 1, ease:Power0.easeNone}));
            .addTo(controller);
        });
        
        // controller.scrollto((newpos) => {
        //     console.log('scrollto', newpos);
        //     TweenLite.to(window, 1, {scrollTo: {y: newpos, autokill: false}, ease:Power1.easeInOut})
        // });
    }

    // Parallax for smooth scrolling
    /*if(isDesktop) {
        function setTranslate(xPos, yPos, el) {
            el.style.transform = "translate3d(" + xPos + ", " + yPos + "px, 0)";
        }     
     
        var xScrollPosition;
        var yScrollPosition;

        var requestAnimationFrame = window.requestAnimationFrame;

        var wineCrafted = document.querySelector('.wine-crafted');
        var wineInfosPhotos = document.querySelector('.wine-infos .photo2-container');
        var wineInfosPhotos = document.querySelector('.wine-infos .photo2-container');
     
        function scrollLoop() {
            xScrollPosition = window.scrollX;
            yScrollPosition = window.scrollY;
     
            setTranslate(0, yScrollPosition * -0.2, wineCrafted);
            setTranslate(0, yScrollPosition * -1.5, wineInfosPhotos);
            setTranslate(0, yScrollPosition * .5, greenPentagon);
     
            requestAnimationFrame(scrollLoop);
        }

        scrollLoop();
    }*/


    // SCROLL TO SHARED PROJECT
    /*var gid = URLUtils.getSearchParam('gid');
    if(gid != '') {
        console.log('scroll to gid', gid);
        var els =  document.querySelectorAll(`[data-id='${URLUtils.getSearchParam('gid')}']`);
        // Menu is 90px height desktop, 60px mobile
        var space = (window.innerHeight - 90 - els[0].offsetHeight) * 0.3; 
        TweenLite.to(window, 1, {scrollTo: {y: isDesktop ? els[0].offsetTop - space - 90 : els[0].offsetTop - 60, autokill: false}, ease:Power1.easeInOut});
    }*/


}

module.exports = {
    init: init
};

