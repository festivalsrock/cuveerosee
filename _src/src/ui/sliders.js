var Swiper = require('swiper');
var scrollUtils = require('../tools/scroll-utils');
import { TweenMax, TimelineMax, Power2 } from 'gsap';
import ScrollToPlugin from '../gsap/plugins/ScrollToPlugin';
import MorphSVGPlugin from '../gsap/plugins/MorphSVGPlugin';
import DrawSVGPlugin from '../gsap/plugins/DrawSVGPlugin';


var init = function(isDesktop, isTablet, isPortrait, cbkKillSmoothScroll) {    

  DrawSVGPlugin;
    
    if(document.getElementsByClassName('js-slider-years').length) {
        console.log('Slider Notes init()');

        // var galleryYear = new Swiper('.js-slider-years', {
        //   direction: isDesktop ? 'vertical' : 'horizontal',
        //   spaceBetween: 1,
        //   centeredSlides: true,
        //   slidesPerView: 'auto',
        //   touchRatio: 0.2,
        //   slideToClickedSlide: true,
        //   mousewheel: true,
        // });

        const plugins = [ScrollToPlugin];

        var elSliderNotes = document.getElementById('wine-notes');

        var direction = isDesktop || !isPortrait ? 'vertical' : 'horizontal';
        // console.log(`Init slider Notes ${direction}`);

        var galleryNotes = new Swiper('.js-slider-notes', {
          init: false,
          direction: direction,
          spaceBetween: 0,
          freeMode: true,
          // mousewheel: true,
          mousewheel: {
            releaseOnEdges: true,
            eventsTarged: elSliderNotes
          },
          slidesPerView: 'auto',
          // slideToClickedSlide: true,
          // loopedSlides : 20,
          // CSSWidthAndHeight: true,
        });

        // galleryNotes.controller.control = galleryYear;
        // galleryYear.controller.control = galleryNotes;
        if(direction == 'vertical') {
          // Align notes left or right
          var nlNotes = document.querySelectorAll('.swiper-slide-note');
          for (var i = 0, nb = nlNotes.length; i < nb; ++i) {       
            if(i % 2 == 1)
              nlNotes[i].classList.add('align-right');
          }
        }

        // Add click to years item
        var nlYears = document.querySelectorAll('.js-slider-years ul li');
        for (var i = 0, nb = nlYears.length; i < nb; ++i) {          
          (function (index){
            nlYears[i].addEventListener('click', (evt) => {
              galleryNotes.slideTo(index);
            });
          })(i);
        }

        var prevIndex = 0;

        galleryNotes.on('slideChange', function () {
          nlYears[this.realIndex].classList.add('active');
          nlYears[prevIndex].classList.remove('active');
          prevIndex = this.realIndex;
        });


        if(direction == 'vertical') {
          var elWineNotes = document.getElementsByClassName('js-wine-notes')[0];
  
          galleryNotes.on('reachBeginning', function() {
            console.log('reachBeginning');            
            cbkKillSmoothScroll(false);
          });
  
          galleryNotes.on('reachEnd', function() {
            console.log('reachEnd');
            cbkKillSmoothScroll(false);
          });

          var onOverSlider = function(e) {
            cbkKillSmoothScroll(true);
            TweenMax.to(window, 0.3, {scrollTo: {y:elWineNotes.offsetTop}, delay: 0.2});
            elWineNotes.removeEventListener('mouseover', onOverSlider);
            elWineNotes.addEventListener('mouseout', onOutSlider);
          }

          var onOutSlider = function(e) {
            var rect = elWineNotes.getBoundingClientRect();
            if(e.clientY < rect.y || e.clientY > rect.bottom) {
              cbkKillSmoothScroll(false);
              elWineNotes.removeEventListener('mouseout', onOutSlider);
              elWineNotes.addEventListener('mouseover', onOverSlider);
            }
          }
          elWineNotes.addEventListener('mouseover', onOverSlider);
          // document.getElementsByClassName('left')[0].addEventListener('mouseover', onOverSlider);
          // document.getElementsByClassName('swiper-wrapper')[0].addEventListener('mouseover', onOverSlider);
        }

        galleryNotes.init();
    }

    /**
     * WINE PORTRAITS
     */
    if(document.getElementsByClassName('js-slider-portraits').length) {
      console.log('Slider Portraits init()');        

      var updatePage = (currentPage) => {
        document.getElementById('page-num').innerText = currentPage;
      }

      var updatePageTotal = (totalPage) => {
        document.getElementById('page-total').innerText = totalPage;
      }      

      var galleryPortraits = new Swiper('.js-slider-portraits', {
        init: false,
        direction: 'horizontal',
        spaceBetween: 0,
        slidesPerView: 1,
        mousewheel: false,
        loop:true,
        effect: 'fade',
        fadeEffect: {
          crossFade: true
        },
        
        preloadImages: false,
        lazy: {
            loadPrevNext: true,
            elementClass: 'swiper-lazy',
            loadedClass: 'swiper-lazy-loaded',
        },

        // setWrapperSize: true,
        shortSwipes : !isDesktop,

        navigation: {
          nextEl: '.js-slider-portraits .swiper-button-next',
          prevEl: '.js-slider-portraits .swiper-button-prev',
        },
      });

      var nbSlides = document.querySelectorAll('.js-slider-portraits .swiper-slide').length;

      updatePageTotal(nbSlides);
      
      galleryPortraits.on('init', function() {
        setTimeout(function() {
          var bg = document.querySelector('.wine-portraits-slider-bg');
          var photo = document.querySelectorAll('.js-slider-portraits .swiper-slide-active .photo')[0];
          var bgDecal = photo.getBoundingClientRect().top - bg.getBoundingClientRect().top;
          console.log('PHOTO', photo);
          console.log('PHOTO', photo.getBoundingClientRect());
          console.log('BG DECAL', bgDecal);
          var btnSwiper = document.querySelector('.js-slider-portraits .swiper-button');
          btnSwiper.style.top = `${photo.getBoundingClientRect().height - btnSwiper.getBoundingClientRect().height + 1}px`;
        }, 500);
      });

      galleryPortraits.on('slideChange', function () {
        updatePage((this.realIndex + 1));
      });

      galleryPortraits.init();
    }
  
    
  
  /**
   * ARTICLES
   */
  if(document.getElementsByClassName('js-slider-life-article').length) {
      console.log('Slider Life Article init()');     
      
      var galleryArticle = new Swiper('.js-slider-life-article', {
        init: false,
        direction: 'horizontal',
        spaceBetween: 0,
        slidesPerView: 1,
        mousewheel: false,
        loop:true,
        effect: 'fade',
        fadeEffect: {
          crossFade: true
        },
        
        preloadImages: false,
        lazy: {
            loadPrevNext: true,
            elementClass: 'swiper-lazy',
            loadedClass: 'swiper-lazy-loaded',
        },

        // setWrapperSize: true,
        shortSwipes : !isDesktop,

        navigation: {
          nextEl: '.js-slider-life-article .swiper-button-next',
          prevEl: '.js-slider-life-article .swiper-button-prev',
        },
      });

      galleryArticle.init();

      // Timer
      MorphSVGPlugin.convertToPath('#btn-swiper-next-circle');
      TweenMax.set('#btn-swiper-next-circle', {rotation:-90, drawSVG:"0%", transformOrigin:'center center'});
      
      var tl = new TimelineMax({repeat:0, yoyo:false});
      function startTimer() {
        console.log('----> startTimer');
        tl = new TimelineMax({repeat:0, yoyo:false});
        tl.fromTo('#btn-swiper-next-circle', 8, {drawSVG:"0%"}, {drawSVG:"100%", onComplete:()=>{
          console.log('startTimer complete');
          galleryArticle.slideNext();
          startTimer();
        }}, 0.1);
      }
      setTimeout(()=>{
        startTimer();
      }, 1000);
      
      var btnSvgNext = document.getElementById('btn-swiper-next-circle');
      btnSvgNext.addEventListener('click', ()=>{
        tl.restart();
      });
  }
}

module.exports = {
    init: init
};