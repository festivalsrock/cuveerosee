<?php

// Redirection de.com vers la langue du navigateur
if(!empty($_SERVER['HTTP_ACCEPT_LANGUAGE']) && empty(str_replace('/', '', $_SERVER['REQUEST_URI']))) {

    $redirectLanguages = ['fr' => '/fr/', 'en-gb' => '/en/'];
    $aBrowserLanguages = explode(',',$_SERVER['HTTP_ACCEPT_LANGUAGE']);
    foreach($aBrowserLanguages as $sBrowserLanguage) {
        $sLang = strtolower($sBrowserLanguage);
//            echo '<pre>';
//            var_dump($sLang, $aBrowserLanguages);
//            echo '</pre>';
//            exit;
        if($sLang == 'fr' || substr($sLang, 0, 2) == 'fr') {
            header('Location: https://'.$_SERVER['HTTP_HOST'].'/fr/');
            exit;
        }
        // A activer quand l'anglais sera en ligne
            if($sLang == 'en-gb' || $sLang == 'en-uk' || $sLang == 'uk') {
                header('Location: https://'.$_SERVER['HTTP_HOST'].'/uk/');
                exit;
            }
        if($sLang == 'en-us' || substr($sLang, 0, 2) == 'en') {
            header('Location: https://'.$_SERVER['HTTP_HOST'].'/us/');
            exit;
        }
    }
}

// Disable HSTS
add_action( 'send_headers', 'add_header_xua' );
function add_header_xua() {
    header( 'Strict-Transport-Security: max-age=0;' );
}

$cuveerose_includes = [
    'inc/conf.php', // Variables globales de configuration
    'inc/countries.php', // Variables globales de configuration liste pays (multilangue)
    'inc/admin.php', // Config backoffice
    'inc/nav.php', // Config nav
    'inc/clean.php', // Clean metas
    'inc/media.php', // Medias
    'inc/helpers.php', // Helpers
    'inc/cookie.php', // Legal cookie
    'inc/mailchimp.php', // Mailchimp
    'inc/stores.php', // Stores
    'inc/concours.php', // Concours
//
//    // Custom Post Type
    'inc/type-timeline.php',

    // EMPTY WP CACHE & TRANSIENTS
//    //'inc/empty-cache.php'
];

foreach ($cuveerose_includes as $file) {
    if (!$filepath = locate_template($file)) {
        trigger_error(sprintf(__('Error locating %s for inclusion', 'cuveerose'), $file), E_USER_ERROR);
    }
    require_once $filepath;
}
unset($file, $filepath);

function cuveerose_scripts() {
    wp_enqueue_style( 'cuveerose', get_template_directory_uri() . '/css/style.css' );

    wp_enqueue_script( 'cuveerose', get_template_directory_uri() . '/js/main.js', array(), '1.0.2', true );

    // STORE LOCATOR
    // wp_enqueue_style( 'cuveerose_storelocator', get_template_directory_uri() . '/css/store-locator.css' );
    // wp_enqueue_script( 'cuveerose_storelocator', get_template_directory_uri() . '/js/storeLocator.js', array(), '1.0.2', true );

    wp_localize_script('cuveerose', 'adminAjaxUrl', admin_url( 'admin-ajax.php' ) );
//    $playlistData = array();
//    $currentPlaylist = unserialize(base64_decode($_COOKIE['current_playlist']));
//    if(isset($currentPlaylist['data']) && is_array($currentPlaylist['data'])) $playlistData = $currentPlaylist['data'];
//
//    wp_localize_script('cuveerose', 'playlistData', array_merge($playlistData) );

}
add_action( 'wp_enqueue_scripts', 'cuveerose_scripts', 1 );

function secure_api( $result ) {
    if ( ! empty( $result ) ) {
        return $result;
    }
    if ( ! is_user_logged_in() ) {
        return new WP_Error( 'rest_not_logged_in', 'You are not currently logged in.', array( 'status' => 401 ) );
    }
    return $result;
}
add_filter('rest_authentication_errors', 'secure_api');

add_filter( 'xmlrpc_enabled', '__return_false' );
remove_action( 'wp_head', 'rsd_link' );