<?php

add_action('admin_menu','cuveerose_remove_links_admin_menu');

function cuveerose_remove_links_admin_menu() {
    remove_menu_page( 'edit-comments.php' );          //Comments
}

if (!defined('WP_POST_REVISIONS')) define('WP_POST_REVISIONS', 10);

/*
 * ACF OPTION PAGE
 */
if( function_exists('acf_add_options_page') ) {

    acf_add_options_page(array('page_title' => 'Config', 'capability' => 'delete_others_pages', 'post_id' => CUVEEROSE_ACF_OPTIONS));

}

/*
 * CUSTOM ADMIN CSS
 */
function cuveerose_admin_style_scripts() {
    wp_enqueue_style('admin-styles', get_template_directory_uri().'/css/admin.css');
    wp_enqueue_style('admin-lightbox-styles', get_template_directory_uri().'/css/admin-lightbox.min.css');
    //wp_enqueue_script( 'admin-scripts', get_template_directory_uri() . '/js/admin.js' );
    wp_localize_script('admin-scripts', 'ajaxurl', admin_url( 'admin-ajax.php' ) );
    wp_enqueue_script( 'admin-lightbox-scripts', get_template_directory_uri() . '/js/admin-lightbox.min.js' );
}
add_action('admin_enqueue_scripts', 'cuveerose_admin_style_scripts');


/*
 * REMOVE P FROM ACF WYSIWIG AND CUSTOM TOOLBAR
 */

add_filter( 'acf/fields/wysiwyg/toolbars' , 'my_toolbars'  );
function my_toolbars( $toolbars )
{
    // Uncomment to view format of $toolbars

//    echo '< pre >';
//        print_r($toolbars);
//    echo '< /pre >';
//    die;


    // Add a new toolbar called "Very Simple"
    // - this toolbar has only 1 row of buttons
    $toolbars['CuveeRose' ] = array();
    $toolbars['CuveeRose' ][1] = array('bold' , 'forecolor ' , 'italic' , 'underline', 'link' );

    // Edit the "Full" toolbar and remove 'code'
    // - delet from array code from http://stackoverflow.com/questions/7225070/php-array-delete-by-value-not-key
//    if( ($key = array_search('code' , $toolbars['Full' ][2])) !== false )
//    {
//        unset( $toolbars['Full' ][2][$key] );
//    }

    // remove the 'Basic' toolbar completely
    unset( $toolbars['Basic' ] );

    // return $toolbars - IMPORTANT!
    return $toolbars;
}
add_action('acf/input/admin_head', 'my_acf_modify_wysiwyg_height');

function my_acf_modify_wysiwyg_height() {

    ?>
    <style type="text/css">
        .acf-field-wysiwyg iframe{
            min-height: 150px !important;
            height: 200px !important;
        }
    </style>
    <?php

}
//
//function my_previous_post_where() {
//
//    global $post, $wpdb;
//
//    return $wpdb->prepare( "WHERE p.menu_order < %s AND p.post_type = %s AND p.post_status = 'publish'", $post->menu_order, $post->post_type);
//}
//add_filter( 'get_previous_post_where', 'my_previous_post_where' );
//
//function my_next_post_where() {
//
//    global $post, $wpdb;
//
//    return $wpdb->prepare( "WHERE p.menu_order > %s AND p.post_type = %s AND p.post_status = 'publish'", $post->menu_order, $post->post_type);
//}
//add_filter( 'get_next_post_where', 'my_next_post_where' );
//
//function my_previous_post_sort() {
//
//    return "ORDER BY p.menu_order desc LIMIT 1";
//}
//add_filter( 'get_previous_post_sort', 'my_previous_post_sort' );
//
//function my_next_post_sort() {
//
//    return "ORDER BY p.menu_order asc LIMIT 1";
//}
//add_filter( 'get_next_post_sort', 'my_next_post_sort' );