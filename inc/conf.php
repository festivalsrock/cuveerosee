<?php

add_theme_support( 'title-tag' );

$current_lang = apply_filters( 'wpml_current_language', NULL );
define('CUVEEROSE_ACF_OPTIONS', 'cuveerose_options_'.$current_lang);