<?php

// Action : legal_cookie
add_action( 'wp_ajax_legal_cookie', 'cuveerose_legal_cookie' );
add_action( 'wp_ajax_nopriv_legal_cookie', 'cuveerose_legal_cookie' );

function cuveerose_legal_cookie() {

    setcookie('legal_cookie', true, time() + (365 * 24 * 60 * 60), '/');

    echo '1';
    wp_die();
}


// Action : cookie_consent
add_action( 'wp_ajax_cookie_consent', 'cuveerose_cookie_consent' );
add_action( 'wp_ajax_nopriv_cookie_consent', 'cuveerose_cookie_consent' );

function cuveerose_cookie_consent() {
    
    // $code_head = get_field('code_head', CUVEEROSE_ACF_OPTIONS);
    $code_head = "<!-- Facebook Pixel Code -->
    <script>
    !function(f,b,e,v,n,t,s)
    {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};
    if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
    n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];
    s.parentNode.insertBefore(t,s)}(window,document,'script',
    'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '210895856462105'); 
    fbq('track', 'PageView');
    </script>
    <noscript>
    <img height=\"1\" width=\"1\" 
    src=\"https://www.facebook.com/tr?id=210895856462105&ev=PageView&noscript=1\"/>
    </noscript>
    <!-- End Facebook Pixel Code -->";

    echo $code_head;
    // wp_die();
}