<?php

/*
 * SUPPORT DETECTION
 */

require_once locate_template('inc/plugins/mobiledetect/Mobile_Detect.php');

$detect = new Mobile_Detect();
$classes = [];

if( $detect->isMobile() && !$detect->isTablet() ){
    define('CUVEEROSE_IS_MOBILE', true);
}else {
    define('CUVEEROSE_IS_MOBILE', false);
}

function cuveerose_get_support_classes() {
    $detect = new Mobile_Detect();
    $classes = [];
    if( $detect->isTablet() ){
        $classes[] = 'tablet';
    }
    if( $detect->isMobile() && !$detect->isTablet() ){
        $classes[] = 'mobile';
    }
    if( !$detect->isMobile() && !$detect->isTablet() ){
        $classes[] = 'desktop';
    }
    if( $detect->isiOS() ){
        $classes[] = 'ios';
    }
    if( $detect->isAndroidOS() ){
        $classes[] = 'android';
    }
    return $classes;
}


function cuveerose_curl_get_data($url){

    $ch = curl_init();

    curl_setopt($ch, CURLOPT_AUTOREFERER, TRUE);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, FALSE);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 3);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    $data = curl_exec($ch);
    curl_close($ch);

    return $data;
}





// Called by gabarit using video or image
function getImageOrVideo($type, $image, $image_alt, $video_hd, $video_sd, $video_poster, $image_lazy = '') {
    $html = '';
    if($type == 'image') {
        $html .= '<img class="lazy" src="'.$image_lazy.'" data-src="'.$image.'" alt="'.$image_alt.'">';
    } else {
        $html .= '<div>';
        $html .=    '<video class="simple-video" muted="true" webkit-playsinline playsinline="" autoplay loop src="'.$video_hd.'" poster="'.$video_poster.'">';
        $html .=        '<source src="'.$video_hd.'" type="video/mp4" media="(min-width: 769px)">';
        $html .=        '<source src="'.$video_sd.'" type="video/mp4">';
        $html .=    '</video>';
        $html .=    '<div class="simple-video-control">';
        $html .=        '<div class="play"><img src="'. esc_url( get_template_directory_uri() ).'/images/bt-play-mini.svg" alt="Play"></div>';
        $html .=        '<div class="pause"><img src="'. esc_url( get_template_directory_uri() ).'/images/bt-pause-mini.svg" alt="Pause"></div>';
        $html .=    '</div>';
        $html .= '</div>';
    }
    return $html;
}


// Called by gabarit to add project title with position and alignement 
function getProjectTitleHTML($titre_gabarit) {

    $title = $titre_gabarit['titre'];
    $color = $titre_gabarit['titre_couleur'];
    $pTop = intval($titre_gabarit['titre_top']).'%';
    $pLeft = intval($titre_gabarit['titre_left']).'%';
    $textAlign = (in_array($titre_gabarit['titre_text_align'], ['left', 'center', 'right'])) ? $titre_gabarit['titre_text_align'] : 'left'; // left, center, right

    return '<div class="title-project" style="color:'.$color.'; top:'.$pTop.'; left:'.$pLeft.'; text-align:'.$textAlign.'">'. $title .'</div>';
}


function strip_tags_only($text, $allowedTags = [])
{
    if (!is_array($allowedTags)) {
        $allowedTags = [
            $allowedTags
        ];
    }

    array_map(
        function($allowedTag) use (&$text) {
            $regEx = '#<' . $allowedTag . '.*?>(.*?)</' . $allowedTag . '>#is';
            $text = preg_replace($regEx, '$1', $text);
        },
        $allowedTags
    );

    return $text;
}