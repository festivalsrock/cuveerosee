<?php

// Action : mailchimp_subscribe
add_action( 'wp_ajax_mailchimp_subscribe', 'cuveerose_mailchimp_subscribe' );
add_action( 'wp_ajax_nopriv_mailchimp_subscribe', 'cuveerose_mailchimp_subscribe' );

function cuveerose_mailchimp_subscribe() {

    $fname = $_POST['firstname'];
    $lname = $_POST['lastname'];
    $zipcode = $_POST['zipcode'];
    $zone = $_POST['country'];
    $custom_newsletter = (intval($_POST['custom_newsletter']) == 1) ? 'yes' : 'no';
    $birthday_day = $_POST['birthday_day'];
    $birthday_month = $_POST['birthday_month'];
    $birthday_year = $_POST['birthday_year'];
    $birthday_date = $birthday_year.'-'.$birthday_month.'-'.$birthday_day;
//    $birthday_date = $_POST['birthday'];


    if(in_array($_POST['lang'], ['uk', 'us', 'fr'])) $current_lang = $_POST['lang'];
    else $current_lang = apply_filters( 'wpml_current_language', NULL );

    $email = $_POST['email'];

    // Limite âge
    $birthday_limit = ($zone == 'US') ? 21 : 18;
//    var_dump($zone, $birthday_limit , strtotime($birthday_date), strtotime('-'.$birthday_limit.' years'));
    if(strtotime('-'.$birthday_limit.' years') < strtotime($birthday_date)) {
        echo 'error_age_legal';
        wp_die();
    }

    $output = '';

    if(!empty($email) && !filter_var($email, FILTER_VALIDATE_EMAIL) === false){
        // MailChimp API credentials
        $apiKey = get_field('mailchimp_api_key', CUVEEROSE_ACF_OPTIONS);
        $listID = get_field('mailchimp_list_id', CUVEEROSE_ACF_OPTIONS);

        // MailChimp API URL
        $memberID = md5(strtolower($email));
        $dataCenter = substr($apiKey,strpos($apiKey,'-')+1);
        $url = 'https://' . $dataCenter . '.api.mailchimp.com/3.0/lists/' . $listID . '/members/' . $memberID;
//        $url = 'https://' . $dataCenter . '.api.mailchimp.com/3.0/lists/' . $listID . '/members';

        // member information
        $json = json_encode([
            'email_address' => $email,
            'status'        => 'subscribed',
            'merge_fields'  => [
                'FNAME'     => $fname,
                'LNAME'     => $lname,
                'ZIPCODE'   => $zipcode,
                'BIRTHDATE' => $birthday_date,
                'ZONE'      => $zone,
                'CUSTOM'    => $custom_newsletter,
                'LANG'      => $current_lang
            ]
        ]);

        // send a HTTP POST request with curl
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_USERPWD, 'user:' . $apiKey);
        curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
        $result = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        if ($httpCode == 200) {
            $output = 'ok';
        } else {
            switch ($httpCode) {
                case 214:
                    $output = 'error_already_subscribed';
                    break;
                default:
                    $output = 'error_global';
                    break;
            }
        }
    }else{
        $output = 'error_invalid_email';
    }
    echo $output;
    wp_die();
}