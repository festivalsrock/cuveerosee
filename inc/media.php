<?php
// Remove default image sizes here.
function cuveerose_remove_default_images( $sizes ) {
    unset( $sizes['small']); // 150px
//    unset( $sizes['medium']); // 300px
    unset( $sizes['large']); // 1024px
    unset( $sizes['medium_large']); // 768px
    return $sizes;
}

add_filter( 'intermediate_image_sizes_advanced', 'cuveerose_remove_default_images', 10, 1 );

// Disable WordPRess responsive srcset images
add_filter('max_srcset_image_width', function() {
    return 1;
});

// Remove accents for filenames
add_filter('sanitize_file_name', 'remove_accents' );

// Custom sizes
function cuveerose_custom_sizes( $sizes ) {
    return array_merge( $sizes, array(
        'mobile-small' => __( 'Mobile (small)' ),
        'mobile' => __( 'Mobile' )
    ) );
}
add_filter( 'image_size_names_choose', 'cuveerose_custom_sizes' );

add_image_size( 'lazy', 50, 50, false );
add_image_size( 'mobile', 1125, null, false );
add_image_size( 'mobile-small', 640, null, false );


/* Autoriser les fichiers SVG */
function wpc_mime_types($mimes) {
    $mimes['svg'] = 'image/svg+xml';
    return $mimes;
}
add_filter('upload_mimes', 'wpc_mime_types');


function getAcfImageUrl($acf_image, $format = '') {
    $sizes = get_intermediate_image_sizes();
    if($format == '') {
        if(isset($acf_image['url'])) return $acf_image['url'];
    }
    else {
        if(in_array($format, $sizes)) {
            if(isset($acf_image['sizes'][$format])) {
                return $acf_image['sizes'][$format];
            }
        }
    }
    return '';
}
function getAcfImageWidth($acf_image, $format = '') {
    $sizes = get_intermediate_image_sizes();
    if($format == '') {
        if(isset($acf_image['width'])) return $acf_image['width'];
    }
    else {
        if(in_array($format, $sizes)) {
            if(isset($acf_image['sizes'][$format.'-width'])) $acf_image['sizes'][$format.'-width'];
        }
    }
    return '';
}
function getAcfImageHeight($acf_image, $format = '') {
    $sizes = get_intermediate_image_sizes();
    if($format == '') {
        if(isset($acf_image['height'])) return $acf_image['height'];
    }
    else {
        if(in_array($format, $sizes)) {
            if(isset($acf_image['sizes'][$format.'-height'])) $acf_image['sizes'][$format.'-height'];
        }
    }
    if(isset($acf_image['height'])) return $acf_image['height'];
    return '';
}

function getAcfImageAlt($acf_image) {
    return (isset($acf_image['alt'])) ? $acf_image['alt'] : '';
}


function getFileSizeName($octet)
{
    // Array contenant les differents unités
    $unite = array('octet',' Ko',' Mo',' Go');

    if ($octet < 1000) // octet
    {
        return $octet.$unite[0];
    }
    else
    {
        if ($octet < 1000000) // ko
        {
            $ko = round($octet/1024,0);
            return $ko.$unite[1];
        }
        else // Mo ou Go
        {
            if ($octet < 1000000000) // Mo
            {
                $mo = round($octet/(1024*1024),0);
                return $mo.$unite[2];
            }
            else // Go
            {
                $go = round($octet/(1024*1024*1024),0);
                return $go.$unite[3];
            }
        }
    }
}


// Blur lazy
//
//add_filter('wp_generate_attachment_metadata','bw_images_filter');
//function bw_images_filter($meta) {
//    $file = wp_upload_dir();
//    $file = trailingslashit($file['path']).$meta['sizes']['lazy']['file'];
//    list($orig_w, $orig_h, $orig_type) = @getimagesize($file);
//    $image = wp_get_image_editor($file);
////    imagefilter($image, IMG_FILTER_GRAYSCALE);
//    @imagefilter($image, IMG_FILTER_GAUSSIAN_BLUR,9999);
//    @imagefilter($image, IMG_FILTER_SMOOTH,99);
//    //imagefilter($image, IMG_FILTER_GAUSSIAN_BLUR);
//    switch ($orig_type) {
//        case IMAGETYPE_GIF:
////            $file = str_replace(".gif", "-blur.gif", $file);
//            imagegif( $image, $file );
//            break;
//        case IMAGETYPE_PNG:
////            $file = str_replace(".png", "-blur.png", $file);
//            imagepng( $image, $file );
//            break;
//        case IMAGETYPE_JPEG:
////            $file = str_replace(".jpg", "-blur.jpg", $file);
//            imagejpeg( $image, $file );
//            break;
//    }
//    return $meta;
//}
