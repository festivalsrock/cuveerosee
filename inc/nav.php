<?php
function cuveerose_main_menu() {
    $wpml_languages = apply_filters( 'wpml_active_languages', NULL );
    foreach($wpml_languages as $wpml_language) {
        register_nav_menu('main-menu-'.$wpml_language['code'],__( 'Menu principal '.$wpml_language['code'] ));
        register_nav_menu('main-menu2-'.$wpml_language['code'],__( 'Menu principal 2 '.$wpml_language['code'] ));
        register_nav_menu('main-menu3-'.$wpml_language['code'],__( 'Menu principal 3 '.$wpml_language['code'] ));
        register_nav_menu('main-footer-'.$wpml_language['code'],__( 'Menu footer '.$wpml_language['code'] ));
        register_nav_menu('main-footer2-'.$wpml_language['code'],__( 'Menu footer 2 '.$wpml_language['code'] ));
    }
}
add_action( 'init', 'cuveerose_main_menu' );
