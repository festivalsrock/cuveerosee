<?php

function cuveerose_create_timeline_type() {

        // create a new taxonomy
    $labels = array(
        'name' => 'Notes',
        'singular_name' => 'Note',
        'all_items' => __('Toutes les notes'),
        'edit_item' => __('Éditer la note'),
        'view_item' => __('Voir la note'),
        'update_item' => __('Mettre à jour la note'),
        'add_new_item' => __('Ajouter une note'),
        'new_item_name' => __('Nouvelle note'),
        'search_items' => __('Rechercher parmi les notes'),
        'popular_items' => __('Notes les plus utilisées')
    );

    $args = array(
        'label' => __( 'Timeline' ),
        'labels' => $labels,
        'hierarchical'        => false,
        'public'              => true,
        'rewrite'             => array('slug' => 'timeline'),
        'has_archive'         => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 5,
        'menu_icon'           => 'dashicons-awards',
        'can_export'          => true,
        'exclude_from_search' => false,
        'publicly_queryable'  => false,
        'capability_type'     => 'post',
    );

    // Registering your Custom Post Type
    register_post_type( 'cuveerose_timeline', $args );

//    $labels = array(
//        'name' => __('Années'),
//        'singular_name' => __('Année'),
//        'all_items' => __('Toutes les années'),
//        'edit_item' => __('Éditer l\'année'),
//        'view_item' => __('Voir l\'année'),
//        'update_item' => __('Mettre à jour l\'année'),
//        'add_new_item' => __('Ajouter une année'),
//        'new_item_name' => __('Nouvelle année'),
//        'search_items' => __('Rechercher parmi les années'),
//        'popular_items' => __('Années les plus utilisés')
//    );
//    register_taxonomy(
//        'cuveerose_timeline_year',
//        'cuveerose_timeline',
//        array(
//            'label' => __( 'Années' ),
//            'labels' => $labels,
//            'rewrite' => array( 'slug' => 'timeline_year' ),
//            'capabilities' => array(
////                    'assign_terms' => 'edit_guides',
////                    'edit_terms' => 'publish_guides'
//            ),
//            'show_in_quick_edit' => false,
//            'meta_box_cb' => false,
//            'hierarchical' => false,
//        )
//    );
//
//    register_taxonomy_for_object_type( 'cuveerose_timeline_year', 'cuveerose_timeline' );

}
add_action( 'init', 'cuveerose_create_timeline_type' );

// Add the custom columns to the book post type:
add_filter( 'manage_cuveerose_timeline_posts_columns', 'set_custom_edit_cuveerose_timeline_columns' );
function set_custom_edit_cuveerose_timeline_columns($columns) {
    unset( $columns['author'] );
    unset( $columns['date'] );
    $columns['year'] = __( 'Année', 'cuveerose' );
    $columns['image'] = __( 'Image', 'cuveerose' );
    $columns['date'] = __( 'Date', 'cuveerose' );

    return $columns;
}

// Add the data to the custom columns for the book post type:
add_action( 'manage_cuveerose_timeline_posts_custom_column' , 'custom_cuveerose_timeline_column', 10, 2 );
function custom_cuveerose_timeline_column( $column, $post_id ) {
    switch ( $column ) {

        case 'year' :
            if(intval(get_field('year', $post_id)) > 1900) echo get_field('year', $post_id);
            break;

        case 'image' :
            echo '<div style="background:#3e0c3f;padding: 3px;display:inline-block;"><img height="60" style="max-width:100%;" src="'.getAcfImageUrl(get_field('image', $post_id)).'"></div>';
            break;

    }
}

add_filter( 'manage_edit-cuveerose_timeline_sortable_columns', 'set_custom_cuveerose_timeline_sortable_columns' );

function set_custom_cuveerose_timeline_sortable_columns( $columns ) {
    $columns['year'] = 'year';

    return $columns;
}

add_action( 'pre_get_posts', 'manage_wp_posts_be_qe_pre_get_posts', 1 );
function manage_wp_posts_be_qe_pre_get_posts( $query ) {

    if ( $query->is_main_query() && ( $orderby = $query->get( 'orderby' ) ) ) {
        switch( $orderby ) {
            case 'year':
                $query->set( 'meta_key', 'year' );
                $query->set( 'orderby', 'meta_value' );
                break;
        }
    }
}