
window.onload = function() {

    $('.select-gabarits .filter.-taxonomy select option:eq(0)').html('Filter par artiste/projet');

    $(document).on('mouseup','.remove-projet',function (event) {
        event = event || window.event;
        event.preventDefault();
        event.stopPropagation();
        if(confirm('Souhaitez-vous réellement supprimer ce projet ?')) {
            jQuery.post(
                ajaxurl,
                {
                    'action': 'onirim_remove_project',
                    'id_projet': $(this).data('id')
                },
                function(response){
                    // console.log('retour ajax');
                    // console.log(response);
                }
            );
        }
    });
};