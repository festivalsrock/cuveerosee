<?php
/**
 * Template Name: Contest
 */

$cover_uptitle = get_field('cover_uptitle');
$cover_title = nl2br(get_field('cover_title', null, false));
$cover_image = get_field('cover_image');
$cover_image_portrait = get_field('cover_image_portrait');
$cover_description = nl2br(get_field('cover_description', null, false));
$contest_checkbox_accept_text = nl2br(get_field('contest_checkbox_accept_text', null, false));

// $newsletter_uptitle = get_field('newsletter_uptitle', CUVEEROSE_ACF_OPTIONS);
//$newsletter_title = "";
$newsletter_label_name = get_field('newsletter_label_name', CUVEEROSE_ACF_OPTIONS);
$newsletter_label_firstname = get_field('newsletter_label_firstname', CUVEEROSE_ACF_OPTIONS);
$newsletter_label_birthday = get_field('newsletter_label_birthday', CUVEEROSE_ACF_OPTIONS);
$newsletter_label_birthday_jj = get_field('newsletter_label_birthday_jj', CUVEEROSE_ACF_OPTIONS);
$newsletter_label_birthday_mm = get_field('newsletter_label_birthday_mm', CUVEEROSE_ACF_OPTIONS);
$newsletter_label_birthday_yyyy = get_field('newsletter_label_birthday_yyyy', CUVEEROSE_ACF_OPTIONS);
$newsletter_label_zipcode = get_field('newsletter_label_zipcode', CUVEEROSE_ACF_OPTIONS);
$newsletter_label_zone = get_field('newsletter_label_zone', CUVEEROSE_ACF_OPTIONS);
$newsletter_label_email = get_field('newsletter_label_email', CUVEEROSE_ACF_OPTIONS);

$newsletter_error_age_legal = get_field('newsletter_error_age_legal', CUVEEROSE_ACF_OPTIONS);
$newsletter_error_already_subscribed = get_field('newsletter_error_already_subscribed', CUVEEROSE_ACF_OPTIONS);
$newsletter_error_email_already_subscribed = "Cet email est déjà utilisé";
$newsletter_error_invalid_email = get_field('newsletter_error_invalid_email', CUVEEROSE_ACF_OPTIONS);
$newsletter_error_global = get_field('newsletter_error_global', CUVEEROSE_ACF_OPTIONS);

$newsletter_must_filled = get_field('newsletter_must_filled', CUVEEROSE_ACF_OPTIONS);
$newsletter_legend = nl2br(get_field('newsletter_legend', CUVEEROSE_ACF_OPTIONS, false));
$newsletter_checkbox_text = nl2br(get_field('newsletter_checkbox_text', CUVEEROSE_ACF_OPTIONS, false));
//$newsletter_checkbox_accept_text = "J’ai lu et j’accepte <a href='reglement' target='_blank'>le reglèment de jeu</a>.";
$newsletter_submit_text = get_field('contest_submit_text');
//$newsletter_submit_text = get_field('newsletter_submit_text', CUVEEROSE_ACF_OPTIONS);
$newsletter_footer = nl2br(get_field('newsletter_footer', CUVEEROSE_ACF_OPTIONS, false));
//$newsletter_success_uptitle = get_field('newsletter_success_uptitle', CUVEEROSE_ACF_OPTIONS);
$newsletter_success_uptitle = get_field('contest_success_uptitle');
//$newsletter_success_title = get_field('newsletter_success_title', CUVEEROSE_ACF_OPTIONS);
$newsletter_success_title = get_field('contest_success_title');

$current_lang = apply_filters( 'wpml_current_language', NULL );
$countries = COUNTRIES_EN;
if(defined('COUNTRIES_'.strtoupper($current_lang))) {
    $countries = constant('COUNTRIES_'.strtoupper($current_lang));
}

get_header(); ?>


<div class="contest" id="page-contest">
    
    <section class="contest-header"> 

        <?php if(CUVEEROSE_IS_MOBILE) { ?>

        <div class="background lazy" data-background="<?php echo getAcfImageUrl($cover_image_portrait); ?>" style="background-image: url(<?php echo esc_url( get_template_directory_uri() ); ?>/images/concours-header.jpg"></div>

        <?php } else { ?>

        <div class="background portrait-only lazy" data-background="<?php echo getAcfImageUrl($cover_image_portrait); ?>" style="background-image: url(<?php echo getAcfImageUrl($cover_image_portrait, 'lazy'); ?>)"></div>

        <div class="background landscape-only lazy" data-background="<?php echo getAcfImageUrl($cover_image); ?>" style="background-image: url(<?php echo getAcfImageUrl($cover_image, 'lazy'); ?>)"></div>

        <?php } ?>

        <div class="container">
            <div class="container-title title"><?php echo $cover_uptitle; ?></div>
            <h2 class="container-resume"><?php echo $cover_title; ?></h2>
            <div class="separator"></div>
        </div>
    </section>

    <section class="contest-form">
        <div id="contest-message-success" class="contest-message-success">
            <h2 class="title"><?php echo $newsletter_success_uptitle; ?></h2>
            <h3 class="subtitle"><?php echo $newsletter_success_title; ?></h3>
        </div>

        <div id="contest-content" class="modal-newsletter-content">
            <form id="contest-form" class="form-horizontal" method="POST" novalidate="">
                <div class="modal-newsletter-header">
                    <h3 class="subtitle"><?php echo $cover_description; ?></h3>
                </div>

                <div class="modal-newsletter-body">

                    <div class="form-group">
                        <label for="lastname"><?php echo $newsletter_label_name; ?></label>
                        <input type="text" name="lastname" id="lastname" class="form-control" placeholder="<?php echo $newsletter_label_name; ?>" required="required" minlength="2" maxlength="50">
                    </div>

                    <div class="form-group">
                        <label for="firstname"><?php echo $newsletter_label_firstname; ?></label>
                        <input type="text" name="firstname" id="firstname" class="form-control" placeholder="<?php echo $newsletter_label_firstname; ?>" required="required" minlength="2" maxlength="30">
                    </div>
                    
                    <div class="form-group">
                        <label for="birthday-contest"><?php echo $newsletter_label_birthday; ?></label>
                        <input type="text" id="birthday-contest" name="birthday-contest" class="form-control form-control-date" placeHolder="<?php echo $newsletter_label_birthday; ?> (dd-mm-yyy)" required="required" title="Birthday"/>
                    </div>
                                        
                    <div class="form-group">
                        <label for="zipcode"><?php echo $newsletter_label_zipcode; ?></label>
                        <input type="text" name="zipcode" id="zipcode" class="form-control" placeholder="<?php echo $newsletter_label_zipcode; ?>" required="required" minlength="2" maxlength="6" pattern="[0-9]{5}[A-Za-z]?">
                    </div>

                    <div class="form-group select">
                        <label for="country"><?php echo $newsletter_label_zone; ?></label>
                        <select name="country" id="country" class="c-select form-control" required="required">
                            <option value="" selected=""><?php echo $newsletter_label_zone; ?></option>
                            <?php foreach($countries as $iso=>$country) : ?>
                                <option value="<?php echo $iso; ?>"><?php echo $country; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="email"><?php echo $newsletter_label_email; ?></label>
                        <input type="email" name="email" id="email" class="form-control" placeholder="<?php echo $newsletter_label_email; ?>" required="required" maxlength="50">
                    </div>
                    
                    <div class="mendatory"><?php echo $newsletter_must_filled; ?></div>
                    
                    <div class="input-error">
                        <div class="error error_invalid_email"><?php echo $newsletter_error_invalid_email; ?></div>
                    </div>
                    
                    <p class="info"><?php echo $newsletter_legend; ?></p>

                    <div class="form-group form-group-optin">
                        <div class="wrapper_input ">
                            <input id="custom_contest_newsletter" name="custom_contest_newsletter" type="checkbox" value="custom_contest_newsletter">
                            <label for="custom_contest_newsletter" class="c-input c-radio c-radio-yes">
                            <span class="c-indicator"></span><div class="agree-txt"><?php echo $newsletter_checkbox_text; ?></div>
                            </label>
                        </div>
                    </div>

                    <div class="form-group form-group-optin">
                        <div class="wrapper_input ">
                            <input id="custom_contest_accept" name="custom_contest_accept" type="checkbox" value="custom_contest_accept" required="required">
                            <label for="custom_contest_accept" class="c-input c-radio c-radio-yes">
                            <span class="c-indicator"></span><div class="agree-txt"><?php echo $contest_checkbox_accept_text; ?></div>
                            </label>
                        </div>
                    </div>

                    <div class="response">
                        <div class="error error_global"><?php echo $newsletter_error_global; ?></div>
                        <div class="error error_age_legal"><?php echo $newsletter_error_age_legal; ?></div>
                        <div class="error error_already_subscribed"><?php echo $newsletter_error_already_subscribed; ?></div>
                        <div class="error error_email_already_subscribe"><?php echo $newsletter_error_email_already_subscribed; ?></div>
                    </div>

                    <input type="hidden" id="lang" name="lang" value="<?php echo $current_lang; ?>">
                    <input type="submit" class="btn btn-link2" value="<?php echo $newsletter_submit_text; ?>">
                    

                </div>
            </form>
        </div>
        <div class="legal"><?php echo $newsletter_footer; ?></div>
    </section>
</div>

<?php get_footer(); ?>