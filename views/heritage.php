<?php
/**
 * Template Name: Heritage
 */
$cover_uptitle = get_field('cover_uptitle');
$cover_title = nl2br(get_field('cover_title', null, false));
$cover_image = get_field('cover_image');
$cover_image_portrait = get_field('cover_image_portrait');
$cover_scroll_text = get_field('cover_scroll_text');

$intro_title = nl2br(get_field('intro_title', null, false));
$intro_text = nl2br(get_field('intro_text', null, false));
$intro_legend = get_field('intro_legend');

$createur_uptitle = get_field('createur_uptitle');
$createur_title = nl2br(get_field('createur_title', null, false));
$createur_image_topright = get_field('createur_image_topright');
$createur_image_bottomright = get_field('createur_image_bottomright');
$createur_image_left = get_field('createur_image_left');
$createur_image_left_legend = get_field('createur_image_left_legend');

$style_title = nl2br(get_field('style_title', null, false));
$style_title2 = nl2br(get_field('style_title2', null, false));
$style_text = nl2br(get_field('style_text', null, false));
$style_image_left = get_field('style_image_left');
$style_image_left_legend = nl2br(get_field('style_image_left_legend', null, false));
$style_image_bottomleft = get_field('style_image_bottomleft');

$flagrance_title = nl2br(get_field('flagrance_title', null, false));
$flagrance_text = nl2br(get_field('flagrance_text', null, false));
$flagrance_image_right = get_field('flagrance_image_right');
$flagrance_image_right_tampon = get_field('flagrance_image_right_tampon');
$flagrance_image_right_legend = get_field('flagrance_image_right_legend');
$flagrance_image_bottomleft = get_field('flagrance_image_bottomleft');

$avenir_title = nl2br(get_field('avenir_title', null, false));
$avenir_text = nl2br(get_field('avenir_text', null, false));
$avenir_image1 = get_field('avenir_image1');
$avenir_image1_portrait = get_field('avenir_image1_portrait');
$avenir_image2 = get_field('avenir_image2');

get_header(); ?>

    <div class="heritage">

        <div class="heritage-header">            

        <?php if(CUVEEROSE_IS_MOBILE) { ?>

            <div class="background lazy" data-background="<?php echo getAcfImageUrl($cover_image_portrait); ?>" style="background-image: url(<?php echo getAcfImageUrl($cover_image_portrait, 'lazy'); ?>)"></div>

        <?php } else { ?>
            
            <div class="background portrait-only lazy" data-background="<?php echo getAcfImageUrl($cover_image_portrait); ?>" style="background-image: url(<?php echo getAcfImageUrl($cover_image_portrait, 'lazy'); ?>)"></div>

            <div class="background landscape-only lazy" data-background="<?php echo getAcfImageUrl($cover_image); ?>" style="background-image: url(<?php echo getAcfImageUrl($cover_image, 'lazy'); ?>)"></div>

        <?php } ?>
        

            <div class="container">
                <div class="parallax slide-in">
                    <h2 class="container-title txt-title-small"><?php echo $cover_uptitle; ?></h2>
                    <h3 class="container-resume"><?php echo $cover_title; ?></h3>
                </div>
            </div>  
            

            <?php if(!CUVEEROSE_IS_MOBILE) { ?>
            <div class="homepage-header-scroll desktop-only">
                <!-- <div class="center"> -->
                    <div class="txt"><?php echo $cover_scroll_text; ?></div>
                    <div class="separator"></div>
                <!-- </div> -->
            </div>   
            <?php } ?>

        </div>

        <section class="heritage-presentation max-width">
            <h2 class="title parallax slide-in"><?php echo $intro_title; ?></h2>
            <div class="parallax slide-in">
                <div class="separator"></div>
                <p class="desc"><?php echo $intro_text; ?></p>
                <p class="name"><?php echo $intro_legend; ?></p>
            </div>
        </section>

        <section class="heritage-creator">
            <div class="max-width clearfix">
                <img class="photo1 parallax slide-in lazy" data-src="<?php echo getAcfImageUrl($createur_image_topright); ?>" src="<?php echo getAcfImageUrl($createur_image_topright, 'lazy'); ?>" alt="<?php echo getAcfImageAlt($createur_image_topright); ?>">
                <div class="heritage-creator-txts parallax slide-in">
                    <h3 class="name txt-title-small"><?php echo $createur_uptitle; ?></h3>
                    <h2 class="title"><?php echo $createur_title; ?></h2>
                </div>
                
                <div class="heritage-creator-photo parallax slide-in">
                    <div class="heritage-creator-photo-bg"></div>
                    <div class="heritage-creator-photo-image">
                        <img class="lazy" data-src="<?php echo getAcfImageUrl($createur_image_left); ?>" src="<?php echo getAcfImageUrl($createur_image_left, 'lazy'); ?>" alt="<?php echo $createur_image_left_legend; ?>" />
                        <div class="heritage-creator-photo-image-legend"><?php echo $createur_image_left_legend; ?></div>
                    </div>
                </div>

                <img class="heritage-creator-photo2 parallax slide-in lazy" data-src="<?php echo getAcfImageUrl($createur_image_bottomright); ?>" src="<?php echo getAcfImageUrl($createur_image_bottomright, 'lazy'); ?>" alt="<?php echo getAcfImageAlt($createur_image_bottomright); ?>">
            </div>
        </section>

        <section class="heritage-style">
            <div class="max-width clearfix">
                <h2 class="title parallax slide-in"><?php echo $style_title; ?></h2>
                
                <div class="separator parallax slide-in"></div>

                <div class="heritage-style-photo parallax slide-in">
                    <div class="heritage-style-photo-bg"></div>
                    <div class="heritage-style-photo-image">
                        <img class="lazy" data-src="<?php echo getAcfImageUrl($style_image_left); ?>" src="<?php echo getAcfImageUrl($style_image_left, 'lazy'); ?>" alt="<?php echo $style_image_left_legend; ?>" />
                        <div class="heritage-style-photo-image-legend"><?php echo $style_image_left_legend; ?></div>
                    </div>
                </div>

                <div class="bg parallax fade-in">
                    <h2 class="title title-2"><?php echo $style_title2; ?></h2>
                    <p class="desc"><?php echo $style_text; ?></p>
                </div>
            </div>
        </section>

        <section class="heritage-crafted">
            <div class="max-width clearfix">
                <img class="heritage-crafted-photo2 parallax slide-in lazy" data-src="<?php echo getAcfImageUrl($style_image_bottomleft); ?>" src="<?php echo getAcfImageUrl($style_image_bottomleft, 'lazy'); ?>" alt="<?php echo getAcfImageAlt($style_image_bottomleft); ?>">

                <div class="bg parallax fade-in">
                    <h2 class="title"><?php echo $flagrance_title; ?></h2>
                    <p class="desc"><?php echo $flagrance_text; ?></p>
                </div>

                <div class="heritage-crafted-photo parallax slide-in">
                    <div class="heritage-crafted-photo-bg"></div>
                    <div class="heritage-crafted-photo-image">
                        <img class="lazy" data-src="<?php echo getAcfImageUrl($flagrance_image_right); ?>" src="<?php echo getAcfImageUrl($flagrance_image_right, 'lazy'); ?>" alt="<?php echo $flagrance_image_right_legend; ?>" />             
                        <div class="heritage-crafted-photo-image-legend"><?php echo $flagrance_image_right_legend; ?></div>
                    </div>
                    <!-- <img class="logo-50" src="<?php echo getAcfImageUrl($flagrance_image_right_tampon); ?>" alt="<?php echo getAcfImageAlt($flagrance_image_right_tampon); ?>" /> -->
                </div>       


                <?php if(!CUVEEROSE_IS_MOBILE) { ?>
                    <div class="parallax slide-in">
                        <img class="heritage-crafted-photo3 landscape-only lazy" data-src="<?php echo getAcfImageUrl($flagrance_image_bottomleft); ?>" src="<?php echo getAcfImageUrl($flagrance_image_bottomleft, 'lazy'); ?>" alt="<?php echo getAcfImageAlt($flagrance_image_bottomleft); ?>">
                    </div>
                <?php } ?> 

            </div>
        </section>


        <section class="heritage-futur">
            <div class="max-width clearfix">

                
            <?php if(CUVEEROSE_IS_MOBILE) { ?>
                <img class="heritage-futur-photo lazy" data-src="<?php echo getAcfImageUrl($avenir_image1_portrait); ?>" src="<?php echo getAcfImageUrl($avenir_image1_portrait, 'lazy'); ?>" alt="<?php echo getAcfImageAlt($avenir_image1_portrait); ?>">
            <?php } else { ?>
                <img class="heritage-futur-photo portrait-only lazy" data-src="<?php echo getAcfImageUrl($avenir_image1_portrait); ?>" src="<?php echo getAcfImageUrl($avenir_image1_portrait, 'lazy'); ?>" alt="<?php echo getAcfImageAlt($avenir_image1_portrait); ?>">
                <img class="heritage-futur-photo landscape-only parallax slide-in lazy" data-src="<?php echo getAcfImageUrl($avenir_image1); ?>" src="<?php echo getAcfImageUrl($avenir_image1, 'lazy'); ?>" alt="<?php echo getAcfImageAlt($avenir_image1); ?>">
            <?php } ?>
                        
                <div class="bg parallax fade-in">
                    <h2 class="title"><?php echo $avenir_title; ?></h2>
                    <p class="desc"><?php echo $avenir_text; ?></p>
                </div>
                
                <img class="heritage-futur-photo2 parallax fade-in lazy" data-src="<?php echo getAcfImageUrl($avenir_image2); ?>" src="<?php echo getAcfImageUrl($avenir_image2, 'lazy'); ?>" alt="<?php echo getAcfImageAlt($avenir_image2); ?>">
            </div>
        </section>

    </div>

<?php get_footer(); ?>