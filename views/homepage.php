<?php
/**
 * Template Name: Homepage
 */

$home_cover_title = get_field('home_cover_bigtitle_1'); //chosen
$home_cover_title2 = get_field('home_cover_bigtitle_2'); // by the best
$home_cover_subtitle = get_field('home_cover_title'); // Craft for a flagrance...
$home_cover_subtitle2 = get_field('home_cover_subtitle'); //"- Venues, Chefs, Sommeliers, Journalists, Wine Tasters -";
$home_cover_link_text = get_field('home_cover_link_text');
$home_cover_link = get_field('home_cover_link');
$home_cover_scroll_text = get_field('home_cover_scroll_text');
$home_cover_background = get_field('home_cover_background');
$home_cover_background_portrait = get_field('home_cover_background_portrait');

$home_news_link_text = get_field('home_news_link_text');
$home_news_link = get_field('home_news_link');
$home_news_txt_discover = get_field('home_news_txt_discover');

$home_heritage_uptitle = get_field('home_heritage_uptitle');
$home_heritage_title = nl2br(get_field('home_heritage_title', null, false));
$home_heritage_link_text = get_field('home_heritage_link_text');
$home_heritage_link = get_field('home_heritage_link');
$home_heritage_image_topleft = get_field('home_heritage_image_topleft');
$home_heritage_image_topleft_portrait = get_field('home_heritage_image_topleft_portrait');
$home_heritage_image_bottomleft = get_field('home_heritage_image_bottomleft');
$home_heritage_image_bottomleft_portrait = get_field('home_heritage_image_bottomleft_portrait');
$home_heritage_image_right = get_field('home_heritage_image_right');
$home_heritage_image_right_legend = get_field('home_heritage_image_right_legend');

//$home_quote_text = nl2br(get_field('home_quote_text', null, false));
//$home_quote_author = get_field('home_quote_author');
$home_notes_title = nl2br(get_field('home_notes_title', null, false));
$home_notes_link_text = get_field('home_notes_link_text');
$home_notes_link = get_field('home_notes_link');
$home_notes_items = get_field('home_notes_items');

$home_instagram_uptitle = get_field('home_instagram_uptitle');
$home_instagram_title = get_field('home_instagram_title');
$home_instagram_url = get_field('home_instagram_url');
$home_instagram_link_text = get_field('home_instagram_link_text');


$args = array(
    'posts_per_page'   => 4,
    'orderby'          => 'date',
    'order'            => 'DESC',
    'post_type'        => 'post',
    'post_status'      => 'publish',
    'suppress_filters' => false
);
wp_reset_postdata();
$posts = get_posts( $args );

get_header(); ?>

    <div id="page-homepage" class="homepage">
        <section class="homepage-header"> 

        <!-- <?php if(CUVEEROSE_IS_MOBILE) { ?> 
            <img class="palm-only" data-src="<?php echo getAcfImageUrl($home_cover_background_portrait); ?>" src="<?php echo getAcfImageUrl($home_cover_background_portrait, 'lazy'); ?>" alt="<?php echo getAcfImageAlt($home_cover_background_portrait); ?>" />
        <?php } else { ?>            
            <img class="portrait-only" data-src="<?php echo getAcfImageUrl($home_cover_background_portrait); ?>" src="<?php echo getAcfImageUrl($home_cover_background_portrait, 'lazy'); ?>" alt="<?php echo getAcfImageAlt($home_cover_background_portrait); ?>" />

            <div class="background landscape-only" style="background-image: url(<?php echo getAcfImageUrl($home_cover_background, 'lazy'); ?>)"></div>

        <?php } ?> -->

        <?php if(CUVEEROSE_IS_MOBILE) { ?> 
            <div id="parallax-mouse" data-relative-input="true" data-scalar-x="50" data-friction-x="0.2" >
                <div data-depth-x="0.8" data-depth-y="0.0" data-limit-x="1" class="parallax-bg"><img data-src="<?php echo getAcfImageUrl($home_cover_background_portrait); ?>" src="<?php echo getAcfImageUrl($home_cover_background_portrait); ?>" alt="<?php echo getAcfImageAlt($home_cover_background_portrait); ?>"></div>
                <!-- <div data-depth-x="0.4" data-depth-y="0.0" class="parallax-bottle"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/homepage-bottle.png" alt=""></div> -->
            
        <?php } else { ?> 
            <div id="parallax-mouse" data-relative-input="true">
                <div data-depth-x="-0.3" data-depth-y="0.2" data-limit-x="0" class="parallax-bg">
                    <img class="portrait-only" data-src="<?php echo getAcfImageUrl($home_cover_background_portrait); ?>" src="<?php echo getAcfImageUrl($home_cover_background_portrait); ?>" alt="<?php echo getAcfImageAlt($home_cover_background_portrait); ?>">
                    <img class="lazy landscape-only" data-src="<?php echo getAcfImageUrl($home_cover_background); ?>" src="<?php echo getAcfImageUrl($home_cover_background, 'lazy'); ?>" alt="<?php echo getAcfImageAlt($home_cover_background_portrait); ?>">
                </div>
                <!-- <div data-depth-x="-0.4" data-depth-y="0.3" class="parallax-bottle">
                    <img class="portrait-only"src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/homepage-bottle.png" alt="">
                    <img  class="lazy landscape-only" data-src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/homepage-bottle-desktop.png" src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/homepage-bottle-desktop-lazy.png" alt="">
                </div> -->
                    
        <?php } ?> 
            </div>

            <!-- <div class="parallax-text"> -->
                <div class="container">
                    <div class="parallax slide-in">
                        <h3 class="container-resume"><?php echo $home_cover_subtitle; ?></h3>
                        <h2 class="container-title"><?php echo $home_cover_title ?></h2> 
                        <div class="decal">
                            <h2 class="container-title line2"><?php echo $home_cover_title2 ?></h2>  
                            <h2 class="container-subtitle"><?php echo $home_cover_subtitle2 ?></h2> 
                        </div>  
                        <h3 class="container-resume container-resume-under"><?php echo $home_cover_subtitle; ?></h3>
                        <div class="container-discover"><a href="<?php echo $home_cover_link; ?>" class="btn btn-link2 discover-btn"><?php echo $home_cover_link_text; ?></a></div>
                    </div>  
                </div>
            <!-- </div> -->
            
            <?php if(!CUVEEROSE_IS_MOBILE) { ?>
            <div class="homepage-header-scroll desktop-only">
                <!-- <div class="center"> -->
                    <div class="txt"><?php echo $home_cover_scroll_text; ?></div>
                    <div class="separator"></div>
                <!-- </div> -->
            </div>   
            <?php } ?>

        </section>

        <section class="homepage-news clearfix">
            
            <?php if(is_array($posts)) : ?>
                
                <?php $count = 0; ?>
                <?php foreach($posts as $post) :
                    $post_id = $post->ID;
                    $categories = get_the_category($post_id);
                    $category_name = '';
                    if(isset($categories[0]->name)) $category_name = $categories[0]->name;

                    $image_listing = get_field('image_listing', $post_id);
                    $image_listing_portrait = get_field('image_listing_portrait', $post_id);
                    ?>

                    <div class="news parallax slide-in">
                        <h2 class="news-title"><?php echo $category_name; ?></h2>
                        <?php if($count == 3) { ?>                        
                            <!-- <div class="news-logo-50"><?php include(locate_template('views/partials/logo-50.php')) ?></div> -->
                        <?php } ?>
                        <a href="<?php echo get_permalink($post_id); ?>">
                            <?php if(CUVEEROSE_IS_MOBILE) { ?>
                                <img class="news-image lazy" data-src="<?php echo getAcfImageUrl($image_listing); ?>" src="<?php echo getAcfImageUrl($image_listing, 'lazy'); ?>" alt="<?php echo getAcfImageAlt($image_listing); ?>" />
                            <?php } else { ?>
                                <?php if($count == 0 || $count == 3) { ?>
                                    <img class="news-image portrait-only lazy" data-src="<?php echo getAcfImageUrl($image_listing); ?>" src="<?php echo getAcfImageUrl($image_listing, 'lazy'); ?>" alt="<?php echo getAcfImageAlt($image_listing); ?>" />
                                    <img class="news-image landscape-only lazy <?php echo $count; ?>" data-src="<?php echo getAcfImageUrl($image_listing); ?>" src="<?php echo getAcfImageUrl($image_listing, 'lazy'); ?>" alt="<?php echo getAcfImageAlt($image_listing); ?>" />
                                <?php } else { ?>
                                    <img class="news-image portrait-only lazy" data-src="<?php echo getAcfImageUrl($image_listing); ?>" src="<?php echo getAcfImageUrl($image_listing, 'lazy'); ?>" alt="<?php echo getAcfImageAlt($image_listing); ?>" />
                                    <img class="news-image landscape-only lazy <?php echo $count; ?>" data-src="<?php echo getAcfImageUrl($image_listing_portrait); ?>" src="<?php echo getAcfImageUrl($image_listing_portrait, 'lazy'); ?>" alt="<?php echo getAcfImageAlt($image_listing_portrait); ?>" />
                                <?php } ?>
                            <?php } ?>
                            <div class="news-layer">
                                <div class="news-resume"><?php echo get_the_title($post_id); ?></div>
                                <div class="news-discover"><a href="<?php echo get_permalink($post_id); ?>" class="btn btn-link2 grey-dark"><?php echo $home_news_txt_discover; ?></a></div>
                            </div>
                        </a>
                    </div>

                    <?php $count++; ?>
                <?php endforeach; ?>
            <?php endif; ?>

            <?php /*
            <!--
            <div class="news">
                <h2 class="news-title">News</h2>
                
                <?php if(CUVEEROSE_IS_MOBILE) { ?>
                    <img class="news-image" src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/homepage-news.jpg" alt="News" />
                <?php } else { ?>
                    <img class="news-image portrait-only" src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/homepage-news.jpg" alt="News" />
                    <img class="news-image landscape-only" src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/homepage-news-desktop.jpg" alt="News" />
                <?php } ?>

                <div class="news-layer">
                    <div class="news-resume">Lorem ipsum dolor sit amet, consectetur</div>  
                    <div class="news-discover"><a href="" class="btn btn-link2">Discover</a></div>        
                </div>    
            </div>

            <div class="news">
                <h2 class="news-title">Events</h2>
                <?php if(CUVEEROSE_IS_MOBILE) { ?>
                    <img class="news-image" src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/homepage-events.jpg" alt="News" />
                <?php } else { ?>
                    <img class="news-image portrait-only" src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/homepage-events.jpg" alt="News" />
                    <img class="news-image landscape-only" src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/homepage-news2-desktop.jpg" alt="News" />
                <?php } ?>
                <div class="news-layer">
                    <div class="news-resume">Lorem ipsum dolor sit amet, lorem ipsum dolor sit amet </div>  
                    <div class="news-discover"><a href="" class="btn btn-link2">Discover</a></div>      
                </div>
            </div>

            <div class="news">
                <h2 class="news-title">Culture</h2>

                <?php if(CUVEEROSE_IS_MOBILE) { ?>
                    <img class="news-image" src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/homepage-events.jpg" alt="News" />
                <?php } else { ?>
                    <img class="news-image portrait-only" src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/homepage-events.jpg" alt="News" />
                    <img class="news-image landscape-only" src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/homepage-events-desktop.jpg" alt="News" />
                <?php } ?>
                <div class="news-layer">
                    <div class="news-resume">Lorem ipsum dolor sit amet, lorem ipsum dolor sit amet </div>
                    <div class="news-discover"><a href="" class="btn btn-link2">Discover</a></div>
                </div>
            </div>

            <div class="news">
                <h2 class="news-title">News</h2>
                
                <div class="news-logo-50"><?php include(locate_template('views/partials/logo-50.php')) ?></div>
                
                <?php if(CUVEEROSE_IS_MOBILE) { ?>
                    <img class="news-image" src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/homepage-news.jpg" alt="News" />
                <?php } else { ?>
                    <img class="news-image portrait-only" src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/homepage-news.jpg" alt="News" />
                    <img class="news-image landscape-only" src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/homepage-news3-desktop.jpg" alt="News" />
                <?php } ?>

                <div class="news-layer">
                    <div class="news-resume">Lorem ipsum dolor sit amet, consectetur</div>  
                    <div class="news-discover"><a href="" class="btn btn-link2">Discover</a></div>        
                </div>     
            </div>
            --> */ ?>
        </section>
        
        <div class="homepage-link"><a href="<?php echo $home_news_link; ?>" class="btn-link2" data-content="<?php echo $home_news_link_text; ?>"><?php echo $home_news_link_text; ?></a></div>

        <section class="homepage-heritage">
        
            <?php if(CUVEEROSE_IS_MOBILE) { ?>
            <div class="heritage" style="background-image: url(<?php echo getAcfImageUrl($home_heritage_image_topleft_portrait); ?>)">
            <?php } else { ?>
            <div class="heritage" style="background-image: url(<?php echo getAcfImageUrl($home_heritage_image_topleft_portrait); ?>)">
                <div class="max-width">
                    <div class="heritage-photo-top landscape-only"><img class="lazy" data-src="<?php echo getAcfImageUrl($home_heritage_image_topleft); ?>" src="<?php echo getAcfImageUrl($home_heritage_image_topleft, 'lazy'); ?>" alt="<?php echo getAcfImageAlt($home_heritage_image_topleft); ?>" class="parallax slide-in"/></div>
            <?php } ?>
            
                    <div class="heritage-photo parallax slide-in">
                        <div class="heritage-photo-bg"></div>
                        <div class="heritage-photo-image">
                            <img class="lazy" data-src="<?php echo getAcfImageUrl($home_heritage_image_right); ?>" src="<?php echo getAcfImageUrl($home_heritage_image_right, 'lazy'); ?>" alt="<?php echo $home_heritage_image_right_legend; ?>" />
                            <div class="heritage-photo-image-legend"><?php echo $home_heritage_image_right_legend; ?></div>
                        </div>
                    </div>

                    <div class="heritage-resume">
                        <div class="parallax fade-in">
                            <h2 class="heritage-resume-title txt-title-small"><?php echo $home_heritage_uptitle; ?></h2>
                            <h3 class="heritage-resume-txt"><?php echo $home_heritage_title; ?></h3>
                            <div class="heritage-resume-link"><a href="<?php echo $home_heritage_link; ?>" class="btn btn-link2 salmon-dark"><?php echo $home_heritage_link_text; ?></a></div>
                        </div>
                    </div>

                <?php if(!CUVEEROSE_IS_MOBILE) { ?>
                </div>
                <?php } ?>

                <div class="heritage-resume2">     
                    <div class="max-width clearfix">

                    <?php if(CUVEEROSE_IS_MOBILE) { ?>
                        <img class="pict lazy" data-src="<?php echo getAcfImageUrl($home_heritage_image_bottomleft_portrait); ?>" src="<?php echo getAcfImageUrl($home_heritage_image_bottomleft_portrait, 'lazy'); ?>" alt="<?php echo getAcfImageAlt($home_heritage_image_bottomleft_portrait); ?>" />
                    <?php } else { ?>
                        <div class="clearfix">
                            <img class="pict parallax slide-in lazy" data-src="<?php echo getAcfImageUrl($home_heritage_image_bottomleft); ?>" src="<?php echo getAcfImageUrl($home_heritage_image_bottomleft, 'lazy'); ?>" alt="<?php echo getAcfImageAlt($home_heritage_image_bottomleft); ?>" />
                        </div>
                    <?php } ?>
                        
                        <div class="left parallax slide-in" data-parallax-delay="0.1">
                            <h2 class="title"><?php echo $home_notes_title; ?></h2>
                            <a class="btn btn-link2 white discover" href="<?php echo $home_notes_link; ?>"><?php echo $home_notes_link_text; ?></a>
                        </div>
                        <div class="heritage-resume2-notes parallax fade-in" data-parallax-delay="0.7">
                            <?php if(is_array($home_notes_items)) : foreach($home_notes_items as $home_notes_item) : ?>
                                <div class="note">
                                    <div class="title"><?php echo get_field('uptitle', $home_notes_item->ID); ?></div>
                                    <div class="logo">
                                        <img src="<?php echo getAcfImageUrl(get_field('image', $home_notes_item->ID)); ?>" alt="<?php echo getAcfImageAlt(get_field('image', $home_notes_item->ID)); ?>">
                                    </div>
                                    <div class="separator"></div>
                                    <div class="place"><?php echo get_field('title', $home_notes_item->ID); ?></div>
                                    <p class="resume"><?php echo nl2br(get_field('text', $home_notes_item->ID, false)); ?></p>
                                    <div class="line"></div>
                                </div>
                            <?php endforeach; endif; ?>
                        </div>
                    </div>  
                </div>  


                
                <div class="heritage-social">
                    <div class="max-width">
                        <div class="heritage-social-title"><?php echo $home_instagram_uptitle; ?></div>
                        <div class="heritage-social-name"><a href="<?php echo $home_instagram_url; ?>" target="_blank"><?php echo $home_instagram_title; ?></a></div>
                        
                        <div>
                        <?php include(locate_template('views/partials/instagram.php')); ?>
                        </div>

                        <!-- <div class="heritage-social-images">
                            <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/homepage-heritage-social1.jpg" alt="">
                            <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/homepage-heritage-social2.jpg" alt="">
                            <?php if(!CUVEEROSE_IS_MOBILE) { ?>
                                <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/homepage-heritage-social3.jpg" alt="">
                                <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/homepage-heritage-social4.jpg" alt="">
                            <?php } ?>
                        </div> -->
                        <a href="<?php echo $home_instagram_url; ?>" class="heritage-social-link btn btn-link2" target="_blank"><?php echo $home_instagram_link_text; ?></a>
                    </div>
                </div>

            </div>      
        </section>

    </div>

<?php get_footer(); ?>