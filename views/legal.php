<?php
/**
 * Template Name: Legal information
 */

get_header(); ?>

    <div class="legal-mention">

        <h1><?php echo get_the_title(); ?></h1>
        <div class="separator"></div>
        <div class="content">
            <?php the_content(); ?>
        </div>

    </div>

<?php get_footer(); ?>