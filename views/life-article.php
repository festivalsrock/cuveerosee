<?php
/**
* Page d'un article
*/

$uptitle = get_field('uptitle'); // Date
$title = get_field('title');
$cover_background = get_field('cover_background');
$cover_background_portrait = get_field('cover_background_portrait');

$actualite_txt_prev_article = get_field('actualite_txt_prev_article', CUVEEROSE_ACF_OPTIONS);
$actualite_txt_next_article = get_field('actualite_txt_next_article', CUVEEROSE_ACF_OPTIONS);

$gabarits = get_field('gabarits');
//die(var_dump($gabarits));

get_header(); ?>

<div class="life-article">
    
    <section class="life-article-header"> 

        <?php if(CUVEEROSE_IS_MOBILE) { ?>

        <div class="background lazy" data-background="<?php echo getAcfImageUrl($cover_background_portrait); ?>" style="background-image: url(<?php echo getAcfImageUrl($cover_background_portrait, 'lazy'); ?>"></div>

        <?php } else { ?>

        <div class="background portrait-only lazy" data-background="<?php echo getAcfImageUrl($cover_background_portrait); ?>" style="background-image: url(<?php echo getAcfImageUrl($cover_background_portrait, 'lazy'); ?>)"></div>

        <div class="background landscape-only lazy" data-background="<?php echo getAcfImageUrl($cover_background); ?>" style="background-image: url(<?php echo getAcfImageUrl($cover_background, 'lazy'); ?>"></div>

        <?php } ?>

        <div class="container">
            <div class="container-title title"><?php echo $uptitle ?></div>  
            <h2 class="container-resume"><?php echo $title ?></h2>
            <!-- <div class="container-discover"><a href="" class="btn btn-link2 discover-btn">Discover</a></div>    -->
            <div class="separator"></div>
            <div class="container-share">
                <span class="txt">share : </span>
                <a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo urlencode(get_the_permalink()); ?>" class="picto" target="_blank">
                    <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/bt-facebook.svg" alt="">
                </a>
                <a href="http://twitter.com/share?text=<?php echo urlencode(get_the_title()); ?>&url=<?php echo urlencode(get_the_permalink()); ?>" class="picto" target="_blank">
                    <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/bt-twitter.svg" alt="">
                </a>
            </div>
        </div>
    </section>

    <section class="life-article-content">
        <?php
        if(is_array($gabarits)) {
            foreach($gabarits as $gabarit) :
                if(!empty($gabarit['acf_fc_layout'])) include(locate_template('views/partials/article-gabarit-'.$gabarit['acf_fc_layout'].'.php'));
            endforeach;
        }
        ?>
    </section>


    <section class="life-article-nav clearfix">
        <?php
        $next_post = get_adjacent_post( false, '', false );
        $prev_post = get_adjacent_post( false, '', true );

        $current_post_id = get_the_id();

        // On prend un article aléatoire si aucun article suivant
        if ( !is_a( $next_post, 'WP_Post' ) ) {
            $args = array(
                'posts_per_page'   => 1,
                'orderby'          => 'rand',
                'post_type'        => 'post',
                'post_status'      => 'publish',
                'exclude'          => array($current_post_id),
                'suppress_filters' => false
            );
            if(isset($prev_post->ID) && $prev_post->ID > 0) $args['exclude'][] = $prev_post->ID;
            wp_reset_postdata();
            $posts = get_posts( $args );
            if ( isset($posts[0]) && is_a( $posts[0], 'WP_Post' ) ) $next_post = $posts[0];
        }
        // On prend un article aléatoire si aucun article précédent
        if ( !is_a( $prev_post, 'WP_Post' ) ) {
            $args = array(
                'posts_per_page'   => 1,
                'orderby'          => 'rand',
                'post_type'        => 'post',
                'post_status'      => 'publish',
                'exclude'          => array($current_post_id),
                'suppress_filters' => false
            );
            if(isset($next_post->ID) && $next_post->ID > 0) $args['exclude'][] = $next_post->ID;
            wp_reset_postdata();
            $posts = get_posts( $args );
            if ( isset($posts[0]) && is_a( $posts[0], 'WP_Post' ) ) $prev_post = $posts[0];
        }
        ?>
        <!-- NEXT -->
        <?php if ( is_a( $next_post, 'WP_Post' ) ) {
            $image_listing = get_field('image_listing', $next_post->ID);
            $image_listing_portrait = get_field('image_listing_portrait', $next_post->ID); ?>
            <a class="nav-item nav-item-next" href="<?php echo get_permalink( $next_post->ID ); ?>">
                <div class="container clearfix">
                    <div class="container-visual" style="background-image:url(<?php echo getAcfImageUrl($image_listing_portrait); ?>)">
                        <!-- <img src="<?php echo getAcfImageUrl($image_listing_portrait); ?>" alt="">                 -->
                    </div>
                    <div class="container-bloc">
                        <div class="center">
                            <div class="title"><?php echo $actualite_txt_next_article; ?></div>
                            <div class="real-title"><?php echo get_the_title( $next_post->ID ); ?></div>
                        </div>
                    </div>
                </div>
            </a>
        <?php } ?>

        <!-- PREV -->
        <?php if ( is_a( $prev_post, 'WP_Post' ) ) {
            $image_listing = get_field('image_listing', $prev_post->ID);
            $image_listing_portrait = get_field('image_listing_portrait', $prev_post->ID); ?>
            <a class="nav-item nav-item-prev" href="<?php echo get_permalink( $prev_post->ID ); ?>">
                <div class="container">
                    <div class="container-visual" style="background-image:url(<?php echo getAcfImageUrl($image_listing_portrait); ?>)">
                        <!-- <img src="<?php echo getAcfImageUrl($image_listing_portrait); ?>" alt="">                 -->
                    </div>
                    <div class="container-bloc">
                        <div class="center">
                            <div class="title"><?php echo $actualite_txt_prev_article; ?></div>
                            <div class="real-title"><?php echo get_the_title( $prev_post->ID ); ?></div>
                        </div>
                    </div>
                </div>
            </a>
        <?php } ?>
    </section>
</div>





<?php get_footer(); ?>