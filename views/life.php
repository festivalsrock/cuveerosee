<?php
/**
 * Template Name: Life
 */

$categories = get_categories( array(
    'orderby' => 'name',
    'order' => 'ASC',
    'hide_empty' => 1
) );

$cover_post_id = 0;
$article_cover = get_field('article_cover');
$txt_all = get_field('txt_all');
$txt_filters = get_field('txt_filters');
$txt_discover = get_field('txt_discover');

// Si aucun article défini alors on prend le plus récent
if(is_null($article_cover)) {
    wp_reset_postdata();
    $last_post = get_posts( ['posts_per_page' => 1, 'suppress_filters' => false]);
    if(isset($last_post[0]->ID)) {
        $cover_post_id = $last_post[0]->ID;
    }
}else {
    if(isset($article_cover->ID)) {
        $cover_post_id = $article_cover->ID;
    }
}

$args = array(
    'posts_per_page'   => 9999,
    'orderby'          => 'menu_order',
    'order'            => 'DESC',
//    'orderby'          => 'date',
//    'order'            => 'DESC',
    'post_type'        => 'post',
    'post_status'      => 'publish',
    'suppress_filters' => false
);
if($cover_post_id > 0) $args['exclude'] = array($cover_post_id);
wp_reset_postdata();
$posts = get_posts( $args );
//echo '<pre>';
//var_dump($cover_post_id, $article_cover, $args, $posts);
//echo '</pre>';
//exit;

get_header(); ?>

    <div id="page-life" class="life">

        <?php if($cover_post_id > 0) :
            $cover_background = get_field('cover_background', $cover_post_id);
            $cover_background_portrait = get_field('cover_background_portrait', $cover_post_id);
        ?>
        <section class="life-header"> 

            <?php if(CUVEEROSE_IS_MOBILE) { ?>

            <div class="background lazy" data-background="<?php echo getAcfImageUrl($cover_background_portrait); ?>" style="background-image: url(<?php echo getAcfImageUrl($cover_background_portrait, 'lazy'); ?>"></div>

            <?php } else { ?>

            <div class="background portrait-only lazy" data-background="<?php echo getAcfImageUrl($cover_background_portrait); ?>" style="background-image: url(<?php echo getAcfImageUrl($cover_background_portrait, 'lazy'); ?>)"></div>

            <div class="background landscape-only lazy" data-background="<?php echo getAcfImageUrl($cover_background); ?>" style="background-image: url(<?php echo getAcfImageUrl($cover_background, 'lazy'); ?>)"></div>

            <?php } ?>

            <div class="container">
                <div class="container-title title"><?php echo get_the_date(null, $cover_post_id); ?></div>
                <h2 class="container-resume"><?php echo get_the_title($cover_post_id); ?></h2>
                <div class="container-discover"><a href="<?php echo get_the_permalink($cover_post_id); ?>" class="btn btn-link2 discover-btn"><?php echo $txt_discover; ?></a></div>
            </div>

        </section>
        <?php endif; ?>


        <section class="life-articles max-width">

            <div class="life-articles-filters parallax fade-in">
                <span class="txt-filter"><?php echo $txt_filters; ?>: </span>
                <button type="button" class="link btn btn-link2 grey selected" data-filter=""><?php echo $txt_all; ?></button>
            <?php foreach($categories as $category) : ?>
                <button type="button" class="link btn btn-link2 grey" data-filter="filter-<?php echo $category->slug; ?>"><?php echo $category->name; ?></button>
            <?php endforeach ?>
            </div>

            <div class="life-articles-container">
            <?php
            // The Loop
            if (sizeof($posts) > 0) :
                foreach($posts as $post) :
                    $post_id = $post->ID;
                    $categories = get_the_category($post_id);
                    $classes = '';
                    foreach($categories as $category) {
                        $classes .= ' filter-'.$category->slug.' ';
                    }
                    $image_listing = get_field('image_listing', $post_id);
                    $image_listing_portrait = get_field('image_listing_portrait', $post_id);
                    ?>
                    
                    <div class="news <?php echo $classes; ?> parallax slide-in">

                        <a href="<?php echo get_permalink($post_id); ?>">
                        <?php if(CUVEEROSE_IS_MOBILE) { ?>
                            <img class="news-image lazy" data-src="<?php echo getAcfImageUrl($image_listing_portrait); ?>" src="<?php echo getAcfImageUrl($image_listing_portrait, 'lazy'); ?>" alt="<?php echo getAcfImageAlt($image_listing_portrait); ?>" />
                        <?php } else { ?>
                            <img class="news-image portrait-only lazy" data-src="<?php echo getAcfImageUrl($image_listing_portrait); ?>" src="<?php echo getAcfImageUrl($image_listing_portrait, 'lazy'); ?>" alt="<?php echo getAcfImageAlt($image_listing_portrait); ?>" />
                            <img class="news-image landscape-only lazy" data-src="<?php echo getAcfImageUrl($image_listing); ?>" src="<?php echo getAcfImageUrl($image_listing, 'lazy'); ?>" alt="<?php echo getAcfImageAlt($image_listing); ?>" />
                        <?php } ?>
                        </a>
                        <div class="news-layer">
                            <h3 class="news-title"><?php echo get_the_date(null, $post_id); ?></h3>
                            <h2 class="news-resume"><a href="<?php echo get_permalink($post_id); ?>"><?php echo get_the_title($post_id); ?></a></h2>
                            <div class="news-discover"><a href="<?php echo get_permalink($post_id); ?>" class="btn btn-link2"><?php echo $txt_discover; ?></a></div>
                        </div>    
                    </div>

                    <?php
               endforeach;
            else:
                // no posts found
            endif;
            ?>

            </div>

<?php /*
            <div class="life-articles-page-num">

            <?php
            $big = 999999999; // need an unlikely integer

            echo paginate_links( array(
                'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
                'format' => '?paged=%#%',
                'current' => max( 1, get_query_var('paged') ),
                'total' => $wp_query->max_num_pages,
                'prev_text' => '',
                'next_text' => ''
            ) );
            ?>
<!--            <a href="" class="prev page-numbers"></a>-->
<!--            <a href="" class='page-numbers'>1</a>-->
<!--            <a href="" class='page-numbers'>2</a>-->
<!--            <a href="" class='page-numbers'>3</a>-->
<!--            <span class="page-numbers dots">...</span>-->
<!--            <a href="" class="next page-numbers"></a>-->

            </div> */ ?>
        </section>
    </div>
<?php get_footer(); ?>