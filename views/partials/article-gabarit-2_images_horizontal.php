<!-- <h2>2 images horizontales</h2> -->
<div class="max-width">
    <div class="gabarit-images-horizontales parallax slide-in">
        <img class="lazy" data-src="<?php echo getAcfImageUrl($gabarit['image_left']); ?>" src="<?php echo getAcfImageUrl($gabarit['image_left'], 'lazy'); ?>" alt="<?php echo getAcfImageAlt($gabarit['image_left']); ?>">
        <img class="lazy" data-src="<?php echo getAcfImageUrl($gabarit['image_right']); ?>" src="<?php echo getAcfImageUrl($gabarit['image_right'], 'lazy'); ?>" alt="<?php echo getAcfImageAlt($gabarit['image_right']); ?>">
    </div>
</div>