<!-- <h2>2 images verticales</h2> -->
<div class="gabarit-images-verticales max-width parallax slide-in">
    <img class="lazy" data-src="<?php echo getAcfImageUrl($gabarit['image_left']); ?>" src="<?php echo getAcfImageUrl($gabarit['image_left'], 'lazy'); ?>" alt="<?php echo getAcfImageAlt($gabarit['image_left']); ?>">
    <img class="lazy" data-src="<?php echo getAcfImageUrl($gabarit['image_right']); ?>" src="<?php echo getAcfImageUrl($gabarit['image_right'], 'lazy'); ?>" alt="<?php echo getAcfImageAlt($gabarit['image_right']); ?>">
</div>