<?php if(isset($gabarit['text']) && isset($gabarit['url'])): ?>
    <div class="life-article-content-discover">
        <a href="<?php echo $gabarit['url']; ?>" class="btn btn-link2 discover-btn" <?php if(isset($gabarit['blank']) && $gabarit['blank'] === true) echo 'target="_blank"'; ?> >
            <?php echo $gabarit['text']; ?>
        </a>
    </div>
<?php endif; ?>
