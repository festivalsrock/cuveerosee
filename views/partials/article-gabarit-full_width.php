<!-- <h2>1 image pleine largeur</h2> -->
<div class="gabarit-image-fullwidth parallax slide-in">
    
    <?php if(CUVEEROSE_IS_MOBILE) { ?>

    <img class="lazy" data-src="<?php echo getAcfImageUrl($gabarit['image'], 'mobile'); ?>" src="<?php echo getAcfImageUrl($gabarit['image'], 'lazy'); ?>" alt="<?php echo getAcfImageAlt($gabarit['image']); ?>">

    <?php } else { ?>

    <img class="lazy" data-src="<?php echo getAcfImageUrl($gabarit['image']); ?>" src="<?php echo getAcfImageUrl($gabarit['image'], 'lazy'); ?>" alt="<?php echo getAcfImageAlt($gabarit['image']); ?>">

    <?php } ?>

    <?php if(!empty($gabarit['title']) || !empty($gabarit['subtitle'])): ?>
    <div class="gabarit-image-fullwidth-bloc">
        <h3 class="title"><?php echo nl2br(strip_tags_only($gabarit['title'], 'p')); ?></h3>
        <p class="resume"><?php echo nl2br(strip_tags_only($gabarit['subtitle'], 'p')); ?></p>
    </div>
    <?php endif; ?>
</div>