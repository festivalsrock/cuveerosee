<div class="gabarit-images-gallery max-width parallax fade-in">

    <div class="swiper-container js-slider-life-article">
        <div class="swiper-wrapper">
            <!-- Slides -->
            <?php if(CUVEEROSE_IS_MOBILE) { ?>
                <?php foreach($gabarit['images'] as $image) : ?>
                    <div class="swiper-slide"><img class="swiper-lazy" data-src="<?php echo getAcfImageUrl($image, 'mobile'); ?>" src="<?php echo getAcfImageUrl($image, 'lazy'); ?>" alt="<?php echo getAcfImageAlt($image); ?>"></div>
                <?php endforeach; ?>
            <?php } else { ?>
                <?php foreach($gabarit['images'] as $image) : ?>
                    <div class="swiper-slide"><img class="swiper-lazy" data-src="<?php echo getAcfImageUrl($image); ?>" src="<?php echo getAcfImageUrl($image, 'lazy'); ?>" alt="<?php echo getAcfImageAlt($image); ?>"></div>
                <?php endforeach; ?>
            <?php } ?>
        </div>
        <div class="swiper-button">
            <div class="swiper-button-prev">
                <svg id="btn-swiper-prev" x="0px" y="0px" width="100%" height="100%" viewBox="0 0 100 100" enable-background="new 0 0 100 100" xml:space="preserve">
                
                    <path id="btn-swiper-prev-circle" shape-rendering="geometricPrecision" fill="none" stroke="#FFFFFF" stroke-width="1" d="M50.008,1.652c26.699,0,48.343,21.649,48.343,48.354c0,26.699-21.644,48.342-48.343,48.342c-26.705,0-48.358-21.643-48.358-48.342 C1.649,23.301,23.303,1.652,50.008,1.652z"/>
    
                    <polygon class="head" fill="#FFFFFF" points="36.019,50 47.997,43.084 59.978,36.167 59.978,50 59.978,63.833 47.997,56.915 "/>
                    <rect class="tail" x="44.009" y="48.5" shape-rendering="geometricPrecision" fill="#FFFFFF" width="15.982" height="3"/>
                </svg>
            </div>
            <div class="swiper-button-next">
                <svg id="btn-swiper-next" x="0px" y="0px" width="100%" height="100%" viewBox="0 0 100 100" enable-background="new 0 0 100 100" xml:space="preserve">

                    <path shape-rendering="geometricPrecision" fill="none" stroke="#FFFFFF" stroke-width="1" d="M50.008,1.652c26.699,0,48.343,21.649,48.343,48.354c0,26.699-21.644,48.342-48.343,48.342c-26.705,0-48.358-21.643-48.358-48.342 C1.649,23.301,23.303,1.652,50.008,1.652z"/>
                    <path id="btn-swiper-next-circle" shape-rendering="geometricPrecision" fill="none" stroke="#FFFFFF" stroke-width="2" d="M50.008,1.652c26.699,0,48.343,21.649,48.343,48.354c0,26.699-21.644,48.342-48.343,48.342c-26.705,0-48.358-21.643-48.358-48.342 C1.649,23.301,23.303,1.652,50.008,1.652z"/>

                    <polygon class="head" fill="#FFFFFF" points="64.823,50 53.001,56.826 41.177,63.652 41.177,50 41.177,36.348 53.001,43.175 "/>
                    <rect class="tail" x="42.009" y="48.5" fill="#FFFFFF" width="15.982" shape-rendering="geometricPrecision" height="3"/>
                </svg>
            </div>
        </div>
    </div>

    <?php if(!empty($gabarit['title']) || !empty($gabarit['subtitle'])): ?>
        <div class="gabarit-images-gallery-bloc">
            <h3 class="title"><?php echo nl2br(strip_tags_only($gabarit['title'], 'p')); ?></h3>
            <p class="resume"><?php echo nl2br(strip_tags_only($gabarit['subtitle'], 'p')); ?></p>
        </div>
    <?php endif; ?>
</div>