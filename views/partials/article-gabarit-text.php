<?php if(isset($gabarit['text'])): ?>
<p class="resume max-width parallax slide-in"><?php echo nl2br(strip_tags_only($gabarit['text'], 'p')); ?></p>
<?php endif; ?>
