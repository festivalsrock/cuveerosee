<?php
/**
 * The template for displaying the footer
 *
 */

$copyright = get_field('footer_copyright', CUVEEROSE_ACF_OPTIONS);
$footer_logo = get_field('footer_logo', CUVEEROSE_ACF_OPTIONS);
$footer_subscribe_text = get_field('footer_subscribe_text', CUVEEROSE_ACF_OPTIONS);

$social_facebook = get_field('social_facebook', CUVEEROSE_ACF_OPTIONS);
$social_instagram = get_field('social_instagram', CUVEEROSE_ACF_OPTIONS);

$intro_cookies_text = get_field('intro_cookies_text', CUVEEROSE_ACF_OPTIONS);
$intro_cookies_link_text = get_field('intro_cookies_link_text', CUVEEROSE_ACF_OPTIONS);
$intro_cookies_btn_text = get_field('intro_cookies_btn_text', CUVEEROSE_ACF_OPTIONS);
$intro_cookies_btn_text_refuse = get_field('intro_cookies_btn_text_refuse', CUVEEROSE_ACF_OPTIONS);
$intro_cookies_link = get_field('intro_cookies_link', CUVEEROSE_ACF_OPTIONS);
?>

</div><!-- .site-content -->

<footer id="footer" class="site-footer" role="contentinfo">

  <div class="max-width">
    <div class="logo-lpcr">
        <a href="<?php echo get_home_url(); ?>">
            <img src="<?php echo getAcfImageUrl($footer_logo); ?>" alt="<?php echo getAcfImageAlt($footer_logo); ?>">
        </a>
    </div>

    <?php
    wp_nav_menu( array(
        'theme_location' => 'main-footer-'.apply_filters( 'wpml_current_language', NULL),
        'container_class' => 'menu-footer' ) );
    ?>
              
    <ul class="site-footer-social">
        <li><a href="<?php echo $social_facebook; ?>" target="_blank"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/bt-facebook-white.svg" alt="Facebook"></a></li>
        <li><a href="<?php echo $social_instagram; ?>" target="_blank"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/bt-instagram-white.svg" alt="Instagram"></a></li>
    </ul>
    
    <div class="site-footer-link">
      <a href="" id="show-modal-newsletter" class="btn btn-link2 white btn-modal"><?php echo $footer_subscribe_text; ?></a>
    </div>

    <div class="separator desktop-only"></div>
    
    <div class="site-footer-legal">
      <div class="brand"><span class="copyright">&copy;</span> <?php echo date('Y'); ?> <?php echo $copyright; ?><span class="space"></span></div>

      <?php
      echo preg_replace('/<a /', '<a class="btn-link2"', strip_tags(wp_nav_menu( array(
          'theme_location' => 'main-footer2-'.apply_filters( 'wpml_current_language', NULL),
          'echo' => false,
          'container_class' => '' ) ), '<a>'));
      ?>
    </div> 


    <ul class="site-footer-social desktop-only">
        <li><a href="<?php echo $social_facebook; ?>" target="_blank"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/bt-facebook-white.svg" alt="Facebook"></a></li>
        <li><a href="<?php echo $social_instagram; ?>" target="_blank"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/bt-instagram-white.svg" alt="Instagram"></a></li>
    </ul>  
  </div>
</footer><!-- .site-footer -->

</div><!-- .site -->

<?php wp_footer(); ?>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-125798903-1"></script>
<script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());
      gtag('config', 'UA-125798903-1');
</script>



<!-- COOKIE CONSENT -->
<script src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.js"></script>    
<script>
window.addEventListener('load', function(){
    
    // var isPortrait = window.innerWidth < window.innerHeight;    
    window.cookieconsent.initialise({
        "type":"opt-in",
        "palette": {
            "popup": {
                "background": "#ffffff",
                "text": "#000000"
            },
            "button": {
                "background": "transparent",
                "text": "#ffffff",
                "border": "#ffffff"
            }
        },     
        // "revokable": true,
        "position": "bottom",
        "content": {
            "dismiss": "<?php echo $intro_cookies_btn_text_refuse; ?>",
            "allow": "<?php echo $intro_cookies_btn_text; ?>",
            "message": "<?php echo $intro_cookies_text; ?>",
            "link": "<?php echo $intro_cookies_link_text; ?>",
            "href": "<?php echo $intro_cookies_link; ?>"
        },
        onInitialise: function (status) {
            var type = this.options.type;
            var didConsent = status == 'allow'; //this.hasConsented();
            if (type == 'opt-in' && didConsent) {
                // enable cookies
                console.log('init cookie : enable cookies');
            }
            if (type == 'opt-out' && !didConsent) {
                // disable cookies
                console.log('init cookie : disable cookies');
            }
        },            
        onStatusChange: function(status) {
            console.log('COOKIE STATUS', status);
            if(status == 'allow') {
                App.cookieAccepted();
            }
        },
    });
});
</script>
<?php // If cookie was already accepted
/* Tracking Activated by Cookie Consent acceptation 
All the content of tracking scripts is parsed by JS, and added to the DOM if cookie accepted */
if(isset($_COOKIE['cookieconsent_status']) && $_COOKIE['cookieconsent_status'] == 'allow') {
    // Cookie accepted, start tracking
    do_action('wp_ajax_cookie_consent');    
} 
?>
<!-- END - COOKIE CONSENT -->

</body>
</html>
