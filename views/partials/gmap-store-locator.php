<?php

$storeLocator = new StoreLocator();
$storeLocator->update_stores_us_ftp();

$stores_title = nl2br(get_field('stores_title',null, false));
$input_address_txt = get_field('input_address_txt');
$close_txt = get_field('close_txt');
$submit_txt = get_field('submit_txt');
$buy_online_title = get_field('buy_online_title');
$buy_online_btn = get_field('buy_online_btn');
$webstores_all = get_field('webstores_all');
$webstores_title = get_field('webstores_title');
$store_US_here_or_online_label = get_field('store_US_here_or_online_label');
$store_US_here_or_online_url = get_field('store_US_here_or_online_url');

//$webstores = Store::getWebstores();
//$mapstores = Store::getMapstores();
//$pageData = StoreLocator::getMetas();

$mapstores = array();
$webstores = array();
$webstores['countries'][] = 'FR';
$webstores['countries'][] = 'UK';
$webstores['countries'][] = 'DE';
//for($i = 1; $i < 10; $i++) {
//    $webstores['stores']['FR'][] = [
//        'name' => 'Nom du webstore',
//        'country' => 'France',
//        'website' => 'www.google.fr',
//    ];
//}

$current_lang = apply_filters( 'wpml_current_language', NULL );

global $sitepress;
$trid = $sitepress->get_element_trid(get_the_ID(), 'post_page');
$translations = $sitepress->get_element_translations($trid, 'page');
$mapstores['data'] = array();
foreach($translations as $translation) {
    if($translation->language_code == 'us') {
        $stores_us = $storeLocator->cuveerose_get_stores_us($translation->element_id);
        if(is_array($mapstores['data']) && is_array($stores_us)) {
            $mapstores['data'] = array_merge($mapstores['data'], $stores_us);
        }
    }else {
        $stores_no_us = $storeLocator->cuveerose_get_stores_csv($translation->element_id);
        if(is_array($mapstores['data']) && is_array($stores_no_us)) {
            $mapstores['data'] = array_merge($mapstores['data'], $stores_no_us);
        }
    }
}
// Dedoublon
$dedoublon = [];
foreach($mapstores['data'] as $k => $data) {

    $tmp = $data['name'].$data['lat'].$data['lng'];
    if(in_array($tmp, $dedoublon)) {
        unset($mapstores['data'][$k]);
    }else {
        $dedoublon[] = $tmp;
    }
}
$mapstores['data'] = array_merge($mapstores['data']);
//if(in_array(strtolower($current_lang), ['', 'fr', 'uk'])) {
//    $mapstores['data'] = cuveerose_get_stores_csv();
//}else {
//    $mapstores['data'] = cuveerose_get_stores_us();
//}
//for($i = 1; $i < 10; $i++) {
//    $mapstores['data'][] = [
//        'id' => 'store-' . $i,
//        'name' => 'Nom du store',
//        'country' => 'FR',
//        'country_name' => 'France',
//        'address' => 'lorem ipsum address',
//        'address2' => 'lorem ipsum address 2',
//        'zipcode' => '69004',
//        'city' => 'Lyon',
//        'phone' => '0123456789',
//        'phone_intl' => '0123456789',
//        'website' => 'www.google.fr',
//        'lat' => '45.'.rand(70, 80),
//        'lng' => '4.'.rand(80, 90),
//    ];
//}
$countries = $webstores['countries'];

$lang = 'FR';
$MAX_NB_WS = 5;
$webstoresExtract = [];

if (isset($webstores['stores'][$lang])) {
    $keys = array_rand($webstores['stores'][$lang], min($MAX_NB_WS, count($webstores['stores'][$lang])));
    if (is_int($keys)) {
        $keys = [$keys];
    }
    $webstoresExtract = array_filter($webstores['stores'][$lang], function($k) use ($keys){
        return in_array($k ,$keys);
    }, ARRAY_FILTER_USE_KEY);
} else if (isset($webstores['stores']['uk'])) {
    $keys = array_rand($webstores['stores']['uk'], min($MAX_NB_WS, count($webstores['stores']['uk'])));
    $webstoresExtract = array_filter($webstores['stores']['uk'], function($k) use ($keys){
        return in_array($k ,$keys);
    }, ARRAY_FILTER_USE_KEY);
} else {
    if(isset($webstores['stores']) && is_array($webstores['stores'])) {
        $arr = [];
        foreach ($webstores['stores'] as $key => $stores) {
            foreach ($stores as $key => $store) {
                $arr[] = $store;
            }
        }
        $keys = array_rand($arr, min($MAX_NB_WS, count($arr)));
        $webstoresExtract = array_filter($arr, function($k) use ($keys){
            return in_array($k ,$keys);
        }, ARRAY_FILTER_USE_KEY);
    }else {
        $webstores['stores'] = [];
    }
}

$hasMapstore = count($mapstores['data']) > 0;
$hasWebstore = count($webstores['stores']) > 0;

function domain($url)
{
    $pattern = '/(?<=http:\/\/|https:\/\/).*(?=\/|\0|\n)/U';
    if (preg_match($pattern, $url, $matches) === 1) {
        return $matches[0];
    }
    return $url;
}

// Check the country
$country = '';
if (strpos($_SERVER['REQUEST_URI'], '/us/') !== false) {
    $country = 'US';
}


//echo '<pre>';
//var_dump($mapstores);
//echo '</pre>';
//exit;
?>

<?php if ($hasMapstore || $hasWebstore) : ?>
    <section id="section_gmap-store-locator" class="slide slide_light section_anchor section_gmap-store-locator <?php echo $country == 'US' ? 'country-us' : ''; ?>" data-title="<?php echo $pageData['title'].' - '.get_bloginfo( 'name' ); ?>">
        <?php if ($hasMapstore) : ?>
            <script>
              window.laup_map_stores = <?php $prefix = '';
              echo '{"data":[';
              foreach($mapstores['data'] as $row) {
                  echo $prefix, json_encode($row);
                  $prefix = ',';
              }
              echo ']}'; //echo json_encode($mapstores); ?>;
              window.lang = '<?php echo $lang; ?>';
            </script>
            <div class="map-container">
                <div class="gmap-container" data-key="AIzaSyCVkAVEqmwrmsxhwU4yrkeMicwLYI_9iFs"></div>
                <div class="bloc dark-bloc result-search-bloc">
                    <header>
                        <div class="form-group">
                            <span class="icon-search"></span>
                            <input type="text" id="result-search-input" class="controls search-input" placeholder="<?php echo $input_address_txt; ?>">
                        </div>
                    </header>
                    <div class="content">
                        <ul></ul>
                    </div>
                </div>
                <button class="close-map-button" type="button">
                    <span class="icon-cross"></span>
                    <span class="visuallyhidden"><?php echo $close_txt; ?></span>
                </button>
            </div>
        <?php endif; ?>
        <div class="container">
            <div class="background-wrapper">
                <?php if(!CUVEEROSE_IS_MOBILE) { ?>
                <video loop muted playsinline autoplay preload="none" id="storeLocatorVideo">
                    <source src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/carte.mp4" type="video/mp4" media="(min-width: 769px)" />
                </video>
                <?php } ?>
                <!-- <picture>
                    <source srcset="<?php echo esc_url( get_template_directory_uri() ); ?>/images/store-map-desktop.jpg" media="(min-width: 769px)" />
                    <img class="background" srcset="<?php echo esc_url( get_template_directory_uri() ); ?>/images/store-map.jpg" />
                </picture> -->
            <?php if(CUVEEROSE_IS_MOBILE) { ?>
                <div class="background-map" style="background-image:url(<?php echo esc_url( get_template_directory_uri() ); ?>/images/store-map.jpg)"></div>
            <?php } else { ?>
                <div class="background-map" style="background-image:url(<?php echo esc_url( get_template_directory_uri() ); ?>/images/store-map-desktop.jpg)"></div>
            <?php } ?>
            </div>
            <?php if ($hasMapstore) : ?>
                <div class="search-box <?php echo $hasWebstore ? 'has-webstore-bloc' : '' ?>">
                    <!-- <div class="logo">
                        <span class="icon-monogramme"></span>
                    </div> -->
                    <h1 class="title_big"><?php echo $stores_title; ?></h1>
                    <div class="separator"></div>
                    <div class="form-group">
                        <span class="icon-search"></span>
                        <input type="text" id="main-search-input" class="controls search-input" placeholder="<?php echo $input_address_txt; ?>">
                        <div class="store-search-buttons-container">
                            <button type="button" id="submitSearchButton" class="btn btn-link2"><?php echo $submit_txt; ?></button>                            
                            <?php if ($country == 'US') { ?>
                            <a class="btn btn-link2 btn-store-external" href="<?php echo $store_US_here_or_online_url; ?>" target="_blank"><?php echo $store_US_here_or_online_label; ?></a>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
            <?php if ($hasWebstore) : ?>
                <div class="bloc dark-bloc webstore-bloc">
                    <header>
                        <h2><?php echo $buy_online_title; ?></h2>
                        <div class="separator"></div>
                    </header>
                    <div class="content">
                        <ul>
                            <?php foreach ($webstoresExtract as $key => $store) : ?>
                                <li>
                                    <h3><?php echo $store['name']; ?></h3>
                                    <div class="website"><?php echo domain($store['website']); ?></div>
                                    <div class="buy-link">
                                        <a href="<?php echo $store['website']; ?>" target="_blank">
                                            <div class="icon icon-arrow_right"></div>
                                            <span class='label'><?php echo $buy_online_btn; ?></span>
                                        </a>
                                    </div>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                    <footer>
                        <button id="see-all-webstore-button">
                            <a href="#" class="btn btn-link2 white"><?php echo $webstores_all; ?></a>
                        </button>
                    </footer>
                </div>
                <div class="modal-lpgs webstores-modal slide_dark">
                    <header>
                        <h1 class="title_big"><?php echo $webstores_title; ?></h1>
                        <button class="close-modal-button" type="button">
                            <span class="icon-cross"></span>
                            <span class="visuallyhidden"><?php echo $close_txt; ?></span>
                        </button>
                        <select name="country" id="country-select-ws">
                            <?php foreach ($countries as $key => $country) : ?>
                                <option value="<?php echo $country; ?>" <?php //echo ($key == 0 ? 'selected' : '') ?>><?php echo $country; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </header>
                    <div class="content">
                        <?php foreach ($webstores['stores'] as $key => $stores) : ?>
                            <ul class="stores-list stores-list-<?php echo $key; ?>">
                                <?php foreach ($stores as $k => $store) : ?>
                                    <li>
                                        <h3><?php echo $store['name']; ?></h3>
                                        <div class="website"><div class="icon icon-arrow_right"></div><?php echo domain($store['website']); ?></div>
                                        <div class="buy-link">
                                            <a href="<?php echo $store['website']; ?>" target="_blank">
                                                <div class="icon icon-arrow_right"></div>
                                                <span class='label'><?php echo $buy_online_btn; ?></span>
                                            </a>
                                        </div>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                        <?php endforeach; ?>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </section>
<?php endif; ?>