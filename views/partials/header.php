<?php
/**
 * The template for displaying the header
 */
session_start();


$social_facebook = get_field('social_facebook', CUVEEROSE_ACF_OPTIONS);
$social_instagram = get_field('social_instagram', CUVEEROSE_ACF_OPTIONS);

$intro_picto = get_field('intro_picto', CUVEEROSE_ACF_OPTIONS);
$intro_logo = get_field('intro_logo', CUVEEROSE_ACF_OPTIONS);
$intro_title = get_field('intro_title', CUVEEROSE_ACF_OPTIONS);
$intro_text = nl2br(get_field('intro_text', CUVEEROSE_ACF_OPTIONS, false));
$intro_text_no = nl2br(get_field('intro_text_no', CUVEEROSE_ACF_OPTIONS, false));
$intro_btn_yes = get_field('intro_btn_yes', CUVEEROSE_ACF_OPTIONS);
$intro_btn_no = get_field('intro_btn_no', CUVEEROSE_ACF_OPTIONS);
$intro_legend = get_field('intro_legend', CUVEEROSE_ACF_OPTIONS);

$intronotes_uptitle = nl2br(get_field('intronotes_uptitle', CUVEEROSE_ACF_OPTIONS, false));
$intronotes_title = nl2br(get_field('intronotes_title', CUVEEROSE_ACF_OPTIONS, false));
$intronotes_items = get_field('intronotes_items', CUVEEROSE_ACF_OPTIONS);

$nav_picto = get_field('nav_picto', CUVEEROSE_ACF_OPTIONS);
$nav_logo = get_field('nav_logo', CUVEEROSE_ACF_OPTIONS);
$nav_logo_fixed = get_field('nav_logo_fixed', CUVEEROSE_ACF_OPTIONS);
$nav_image = get_field('nav_image', CUVEEROSE_ACF_OPTIONS);

$nav_langs = icl_get_languages('skip_missing=N&orderby=KEY&order=DIR&link_empty_to=str');
// echo '<pre>';
// var_dump($_COOKIE);
// echo '</pre>';

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js <?php echo join( ' ', cuveerose_get_support_classes() ) ?>">
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
    <!--[if lt IE 9]>
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/html5.js"></script>
    <![endif]-->
    <?php
    if(isset($_REQUEST['gid']) && intval($_REQUEST['gid']) > 0) {
        remove_action( 'wp_head', '_wp_render_title_tag', 1 );
        echo onirim_curl_get_data(get_permalink( intval($_REQUEST['gid']) ).'?seo');
    }
    wp_head(); ?>

    <link rel="shortcut icon" type="image/png" href="<?php echo esc_url( get_template_directory_uri() ); ?>/images/favicon.png" />

    <!-- Flatpickr -->
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
    
</head>

<body <?php body_class(); ?>>

<?php
$current_lang = apply_filters( 'wpml_current_language', NULL );
if($current_lang == 'uk' && is_front_page()) {
    if($_GET['utm_source'] == 'elle' && $_GET['utm_medium'] == 'display' && $_GET['utm_campaign'] == 'ope-lp-uk-oct19') {
        ?>
        <!--
Start of Floodlight Tag: Please do not remove
Activity name of this tag: Cuvée Rosé_Visite Landing page (déclenchement dès chargement de la page)
URL of the webpage where the tag is expected to be placed: https://cuveerose.com/uk/?utm_source=elle&utm_medium=display&utm_campaign=ope-lp-uk-oct19
This tag must be placed between the <body> and </body> tags, as close as possible to the opening tag.
Creation Date: 10/14/2019
-->
        <script type="text/javascript">
          var axel = Math.random() + "";
          var a = axel * 10000000000000;
          document.write('<iframe src="https://8233454.fls.doubleclick.net/activityi;src=8233454;type=cuvee0;cat=lpcr-19;dc_lat=;dc_rdid=;tag_for_child_directed_treatment=;tfua=;npa=;ord=' + a + '?" width="1" height="1" frameborder="0" style="display:none"></iframe>');
        </script>
        <noscript>
            <iframe src="https://8233454.fls.doubleclick.net/activityi;src=8233454;type=cuvee0;cat=lpcr-19;dc_lat=;dc_rdid=;tag_for_child_directed_treatment=;tfua=;npa=;ord=1?" width="1" height="1" frameborder="0" style="display:none"></iframe>
        </noscript>
        <!-- End of Floodlight Tag: Please do not remove -->

        <!--
        Start of Floodlight Tag: Please do not remove
        Activity name of this tag: Cuvée Rosé_Visite Landing page_qualifié (déclenchement à la 4eme sec de chargement de la page)
        URL of the webpage where the tag is expected to be placed: https://cuveerose.com/uk/?utm_source=elle&utm_medium=display&utm_campaign=ope-lp-uk-oct19
        This tag must be placed between the <body> and </body> tags, as close as possible to the opening tag.
        Creation Date: 10/14/2019
        -->
        <script type="text/javascript">
          var axel = Math.random() + "";
          var a = axel * 10000000000000;
          document.write('<iframe src="https://8233454.fls.doubleclick.net/activityi;src=8233454;type=cuvee0;cat=lp4s-19;dc_lat=;dc_rdid=;tag_for_child_directed_treatment=;tfua=;npa=;ord=' + a + '?" width="1" height="1" frameborder="0" style="display:none"></iframe>');
        </script>
        <noscript>
            <iframe src="https://8233454.fls.doubleclick.net/activityi;src=8233454;type=cuvee0;cat=lp4s-19;dc_lat=;dc_rdid=;tag_for_child_directed_treatment=;tfua=;npa=;ord=1?" width="1" height="1" frameborder="0" style="display:none"></iframe>
        </noscript>
        <!-- End of Floodlight Tag: Please do not remove -->

        <?php
    }
}
?>
<?php include(locate_template('views/partials/modal-newsletter.php')); ?>

<div class="header" id="header">
    <?php
    /*wp_nav_menu( array(
        'theme_location' => 'main-menu-'.apply_filters( 'wpml_current_language', NULL),
        'container_class' => 'menu-desktop' ) );
        */
    ?>

    <svg style="display:none" xmlns="http://www.w3.org/2000/svg" version="1.1">
        <defs>
            <filter id="svg-blur">
                <feGaussianBlur stdDeviation="5" />
            </filter>
            <filter id="svg-blur-out">
                <feGaussianBlur stdDeviation="0" />
            </filter>
        </defs>
    </svg>

    <div id="menu-mobile" class="menu-mobile">
        <div class="menu-mobile-burger js-menu-burger">
         <svg version="1.1" id="burger-svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
        width="30px" height="30px" viewBox="0 0 30 30" enable-background="new 0 0 30 30" xml:space="preserve">
            <rect fill="none" width="25" height="25"/>
            <rect class="burger-line" x="5" y="6" width="25" height="1" fill="#ffffff"/>
            <rect class="burger-line" x="5" y="12" width="25" height="1" fill="#ffffff"/>
            <rect class="burger-line" x="5" y="18" width="25" height="1" fill="#ffffff"/>
        </svg>
            <!-- <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="15px" height="8px" viewBox="0 0 15 8" enable-background="new 0 0 15 8" xml:space="preserve">
            <polygon id="Fill-1" points="0,0.533 15,0.533 15,0 0,0 "/>
            <polygon id="Fill-2" points="0,4.111 15,4.111 15,3.578 0,3.578 "/>
            <polygon id="Fill-3" points="0,7.689 15,7.689 15,7.156 0,7.156 "/>
            </svg> -->
        </div>
        
        
        <?php #if(!CUVEEROSE_IS_MOBILE) { ?>
            <div class="logo">
                <a href="<?php echo get_home_url(); ?>">
                    <img class="logo-bw" src="<?php echo getAcfImageUrl($nav_logo_fixed); ?>" alt="<?php echo getAcfImageAlt($nav_logo_fixed); ?>"><img class="logo-pink" src="<?php echo getAcfImageUrl($intro_logo); ?>" alt="<?php echo getAcfImageAlt($intro_logo); ?>">
                </a></div>
        <?php #} ?>
    </div>

    <nav class="menu-mobile-content js-menu-content">
        <div class="left">
            <div class="menu-mobile-content-centered">
                <!-- <div class="logo-50"><img src="<?php echo getAcfImageUrl($nav_picto); ?>" alt="<?php echo getAcfImageAlt($nav_picto); ?>"></div> -->
                <!-- <div class="logo-50 show-me"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/logo-50-transparent.svg" alt="<?php echo getAcfImageAlt($nav_picto); ?>"></div> -->
                <div class="logo-lpcr show-me">
                    <a href="<?php echo get_home_url(); ?>">
                        <img src="<?php echo getAcfImageUrl($nav_logo); ?>" alt="<?php echo getAcfImageAlt($nav_logo); ?>">
                    </a>
                </div>

                <div class="separator show-me"></div>
                
                <?php
                    wp_nav_menu( array(
                    'theme_location' => 'main-menu-'.apply_filters( 'wpml_current_language', NULL),
                    'container_class' => 'menu-mobile-ul' ) );
                ?>

                <div class="separator palm-only"></div>

                <?php
                    wp_nav_menu( array(
                    'theme_location' => 'main-menu2-'.apply_filters( 'wpml_current_language', NULL),
                    'container_class' => 'menu-mobile-ul-2' ) );
                ?>
            </div>            

            <?php
                wp_nav_menu( array(
                'theme_location' => 'main-menu3-'.apply_filters( 'wpml_current_language', NULL),
                'container_class' => 'menu-mobile-ul-3' ) );
            ?>
        
            <ul class="social">
                <li><a href="<?php echo $social_facebook; ?>" target="_blank"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/bt-facebook.svg" alt="Facebook"></a></li>
                <li><a href="<?php echo $social_instagram; ?>" target="_blank"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/bt-instagram.svg" alt="Instagram"></a></li>
            </ul>

            <div class="bt-close js-menu-close"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                    width="30px" height="30px" viewBox="0 0 25 25" enable-background="new 0 0 25 25" xml:space="preserve">
            <rect fill="none" width="25" height="25"/>
                    <rect x="5" y="12" transform="matrix(0.7071 -0.7071 0.7071 0.7071 -5.1777 12.5)" fill="#000000" width="15" height="1"/>
                    <rect x="5" y="12" transform="matrix(0.7071 0.7071 -0.7071 0.7071 12.5 -5.1777)" fill="#000000" width="15" height="1"/>
            </svg></div>
        </div>
        
        <?php if(!CUVEEROSE_IS_MOBILE) { ?>
        <div class="right">
            <div class="visual" style="background-image: url(<?php echo getAcfImageUrl($nav_image); ?>)">
                <!-- <img src="<?php echo getAcfImageUrl($nav_image); ?>" alt="<?php echo getAcfImageAlt($nav_image); ?>"> -->
            </div>
        </div>
        <?php } ?>

    </nav>

    <div id="lang-selector">        
        <ul>
        <?php foreach($nav_langs as $nav_lang) : ?>
            <?php if($nav_lang['active'] == '1') { ?>
                <li class="selected" ><?php echo $nav_lang['code']; ?></li> 
            <?php } ?>
        <?php endforeach; ?>

        <?php foreach($nav_langs as $nav_lang) : ?>        
            <?php if($nav_lang['active'] !== '1') { ?>
                <li>
                    <a href="<?php echo $nav_lang['url']; ?>"><?php echo $nav_lang['code']; ?></a>
                </li>
            <?php } ?>
        <?php endforeach; ?>
        </ul>
    </div>

    <?php if(!isset($_COOKIE['legal_cookie']) || $_COOKIE['legal_cookie'] != 1) { ?>
    <div id="intro">
        <div class="container max-width">
            <!-- <img class="logo1" src="<?php echo getAcfImageUrl($intro_logo1); ?>" alt="<?php echo getAcfImageAlt($intro_logo1); ?>">
            <img class="logo2" src="<?php echo getAcfImageUrl($intro_logo2); ?>" alt="<?php echo getAcfImageAlt($intro_logo2); ?>"> -->
            <!-- <img class="logo1" src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/logo-50.svg" alt="Logo Cuvée Rosé 50th"> -->
            
            <!-- <?php include(locate_template('views/partials/logo-50.php')) ?> -->
            <!-- <img class="logo1" src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/logo-anim.gif" alt="Logo Cuvée Rosé 50th"> -->
            <img class="logo2 hide" src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/logo-lpcr.svg" alt="Laurent-Perrier, Cuvée Rosé">
            <h2 class="title hide"><?php echo $intro_title; ?></h2>
            <div class="separator hide"></div>
            
            <div class="question-container hide">
                <p id="question" class="question"><?php echo $intro_text; ?></p>
                <p id="answer-no" class="answer-no"><?php echo $intro_text_no; ?></p>
            </div>
            <div id="answer" class="answer hide">
                <input id="bt-answer-yes" type="button" align="center" value="<?php echo $intro_btn_yes; ?>" class="yes" accesskey="y"/>
                <input id="bt-answer-no" type="button" align="center" value="<?php echo $intro_btn_no; ?>" class="no" accesskey="n"/>
            </div>
        </div>
        <div class="warning"><?php echo $intro_legend; ?></div>
    </div>
    <?php } ?>

    <?php if(!isset($_SESSION['intro-notes']) || $_SESSION['intro-notes'] !== '1') {?>
    <div id="intro-notes" class="intro-notes">
        <div class="container max-width">
            <div class="intro-notes-subtitle hide"><?php echo $intronotes_uptitle; ?></div>
            <div class="intro-notes-title hide"><?php echo $intronotes_title; ?></div>
            <div class="notes-container">

                <?php if(is_array($intronotes_items)) : foreach($intronotes_items as $item) : ?>
                    <div class="notes-container-item hide">
                        <div class="title"><?php echo get_field('uptitle', $item->ID); ?></div>
                        <div class="logo">
                            <div class="center">
                                <img data-object-fit="contain" data-object-position="50% 50%" src="<?php echo getAcfImageUrl(get_field('image_intro', $item->ID)); ?>" alt="<?php echo getAcfImageAlt(get_field('image_intro', $item->ID)); ?>">
                            </div>
                        </div>
                        <div class="place"><?php echo get_field('title', $item->ID); ?></div>
                    </div>
                <?php endforeach; endif; ?>
                
                <!-- <div class="notes-container-item hide">
                    <div class="title">france</div>
                    <div class="logo">
                        <div class="center">
                            <img data-object-fit="contain" data-object-position="top left" src="//local.cuveerosee.fr/data/files/2018/07/la-revenue-du-vin-de-france-2.png" alt="">
                        </div>
                    </div>
                    <div class="place">14,5/20</div>
                </div>
                <div class="notes-container-item hide">
                    <div class="title">usa</div>
                    <div class="logo">
                        <div class="center">
                            <img data-object-fit="contain" data-object-position="top left" src="//local.cuveerosee.fr/data/files/2018/07/wine-spectator-1.png" alt="">
                        </div>
                    </div>
                    <div class="place">92/100</div>
                </div>
                <div class="notes-container-item hide">
                    <div class="title">france</div>
                    <div class="logo">
                        <div class="center">
                            <img data-object-fit="contain" data-object-position="top left" src="//local.cuveerosee.fr/data/files/2018/07/bettane-et-desseauve.png" alt="">
                        </div>
                    </div>
                    <div class="place">16,5/20</div>
                </div> -->
            </div>
        </div>
    </div>
    <?php  } 
    $_SESSION['intro-notes'] = '1'; ?>

    <div id="transition" class="enter"></div>

    <!--<div id="logo" class="">
        <a href="/"><img class="black" src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/onirim.svg" alt="Logo Onirim"><img class="white" src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/onirim-white.svg" alt="Logo Onirim"></a>
    </div>-->
</div>


<div id="page" class="hfeed site">
    <div id="content" class="site-content">

