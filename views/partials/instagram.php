<?php
// Flux Instagram Onirim
//$user_id = "1346067497";
//$access_token = "4739200299.f8e4fe2.6f6ef59d03bc47e0a5f57d070782389f";

/*
 * Trouver un nouveau jeton > appeler URL : https://www.instagram.com/oauth/authorize/?client_id=f8e4fe242d844883838e9b4d676d4fd8&redirect_uri=https://cuveerose.com&response_type=token
 * puis récup jeton dans url redirection
 */

// On récupère les variable depuis la page d'options ACF, id et token issus du plugin Instagram Feed
$user_id = get_field('social_instagram_user_id', CUVEEROSE_ACF_OPTIONS);
$access_token = get_field('social_instagram_access_token', CUVEEROSE_ACF_OPTIONS);
$instagram_url = get_field('social_instagram', CUVEEROSE_ACF_OPTIONS);

// On récupère les 6 dernières images publiées sur le compte Instagram
$endpoint  = "https://api.instagram.com/v1/users/".$user_id."/media/recent/?count=4&access_token=".$access_token;
//echo $endpoint;
$curl = curl_init($endpoint);
curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 3);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

$data = curl_exec($curl);

// On converit en pbjet le retour JSON d'Instagram
$publications = json_decode($data);
//echo '<pre>';
//var_dump($endpoint, $publications, $user_id, $access_token);
//echo '</pre>';
//exit;


/* à mettre sur homeage.php :
                        <?php include(locate_template('views/partials/instagram.php')); ?> */
?>

<div class="heritage-social-images">
    <?php if(isset($publications->data) && is_array($publications->data)) { ?>
        <?php $count = 0; ?>
        <?php foreach($publications->data as $k=>$publication) {  ?>
            <?php if($k < 2 || ($k > 1 && !CUVEEROSE_IS_MOBILE)) { // on affiche que 2 images sur mobile ?>
                <div class="link">
                    <a href="<?php echo $publication->link; ?>" target="_blank">
                        <img src="<?php echo $publication->images->standard_resolution->url; ?>" alt="<?php echo $publication->caption->text; ?>"  class="parallax fade-in" data-parallax-delay="<?php echo $count; ?>" >
                    </a>
                </div>
            <?php } ?>
            <?php $count += 0.1; ?>
        <?php } ?>
    <?php } ?>
</div>
