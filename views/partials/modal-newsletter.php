<?php

$newsletter_uptitle = get_field('newsletter_uptitle', CUVEEROSE_ACF_OPTIONS);
$newsletter_title = get_field('newsletter_title', CUVEEROSE_ACF_OPTIONS);
$newsletter_label_name = get_field('newsletter_label_name', CUVEEROSE_ACF_OPTIONS);
$newsletter_label_firstname = get_field('newsletter_label_firstname', CUVEEROSE_ACF_OPTIONS);
$newsletter_label_birthday = get_field('newsletter_label_birthday', CUVEEROSE_ACF_OPTIONS);
$newsletter_label_birthday_jj = get_field('newsletter_label_birthday_jj', CUVEEROSE_ACF_OPTIONS);
$newsletter_label_birthday_mm = get_field('newsletter_label_birthday_mm', CUVEEROSE_ACF_OPTIONS);
$newsletter_label_birthday_yyyy = get_field('newsletter_label_birthday_yyyy', CUVEEROSE_ACF_OPTIONS);
$newsletter_label_zipcode = get_field('newsletter_label_zipcode', CUVEEROSE_ACF_OPTIONS);
$newsletter_label_zone = get_field('newsletter_label_zone', CUVEEROSE_ACF_OPTIONS);
$newsletter_label_email = get_field('newsletter_label_email', CUVEEROSE_ACF_OPTIONS);

$newsletter_error_age_legal = get_field('newsletter_error_age_legal', CUVEEROSE_ACF_OPTIONS);
$newsletter_error_already_subscribed = get_field('newsletter_error_already_subscribed', CUVEEROSE_ACF_OPTIONS);
$newsletter_error_invalid_email = get_field('newsletter_error_invalid_email', CUVEEROSE_ACF_OPTIONS);
$newsletter_error_global = get_field('newsletter_error_global', CUVEEROSE_ACF_OPTIONS);

$newsletter_must_filled = get_field('newsletter_must_filled', CUVEEROSE_ACF_OPTIONS);
$newsletter_legend = nl2br(get_field('newsletter_legend', CUVEEROSE_ACF_OPTIONS, false));
$newsletter_checkbox_text = nl2br(get_field('newsletter_checkbox_text', CUVEEROSE_ACF_OPTIONS, false));
$newsletter_submit_text = get_field('newsletter_submit_text', CUVEEROSE_ACF_OPTIONS);
$newsletter_footer = nl2br(get_field('newsletter_footer', CUVEEROSE_ACF_OPTIONS, false));
$newsletter_success_uptitle = get_field('newsletter_success_uptitle', CUVEEROSE_ACF_OPTIONS);
$newsletter_success_title = get_field('newsletter_success_title', CUVEEROSE_ACF_OPTIONS);

$current_lang = apply_filters( 'wpml_current_language', NULL );
$countries = COUNTRIES_EN;
if(defined('COUNTRIES_'.strtoupper($current_lang))) {
    $countries = constant('COUNTRIES_'.strtoupper($current_lang));
}
?>

<div id="modal-newsletter" class="modal modal-newsletter">
    <!-- <div class="modal-zone"></div> -->
    <div class="modal-dialog modal-newsletter-dialog animated">
        
        <div class="bt-close" id="bt-close-newsletter"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                        width="25px" height="25px" viewBox="0 0 25 25" enable-background="new 0 0 25 25" xml:space="preserve">
                <rect fill="none" width="25" height="25"/>
                        <rect x="5" y="12" transform="matrix(0.7071 -0.7071 0.7071 0.7071 -5.1777 12.5)" fill="#000000" width="15" height="1"/>
                        <rect x="5" y="12" transform="matrix(0.7071 0.7071 -0.7071 0.7071 12.5 -5.1777)" fill="#000000" width="15" height="1"/>
                </svg></div>

        
        <div id="newsletter-message-success" class="modal-newsletter-message-success">
            <h2 class="title"><?php echo $newsletter_success_uptitle; ?></h2>
            <h3 class="subtitle"><?php echo $newsletter_success_title; ?></h3>
        </div>

        <div id="newsletter-content" class="modal-newsletter-content">
            <form id="newsletter-form" class="form-horizontal" method="POST" novalidate="">
                <div class="modal-newsletter-header">
                    <h2 class="title"><?php echo $newsletter_uptitle; ?></h2>
                    <h3 class="subtitle"><?php echo $newsletter_title; ?></h3>
                </div>

                <div class="modal-newsletter-body">

                    <div class="form-group">
                        <label for="lastname"><?php echo $newsletter_label_name; ?></label>
                        <input type="text" name="lastname" id="lastname" class="form-control" placeholder="<?php echo $newsletter_label_name; ?>" required="required" minlength="2" maxlength="50">
                    </div>

                    <div class="form-group">
                        <label for="firstname"><?php echo $newsletter_label_firstname; ?></label>
                        <input type="text" name="firstname" id="firstname" class="form-control" placeholder="<?php echo $newsletter_label_firstname; ?>" required="required" minlength="2" maxlength="30">
                    </div>
                    
                    <!-- <div class="form-group">
                        <label for="birthday">Birthday</label>
                        <input type="date" id="birthday" name="birthday"  min="1900-01-01" max="2001-01-01" class="form-control" required="required" title="Birthday" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}"/>
                    </div> -->
                    
                    <div class="form-group">
                        <label for="birthday"><?php echo $newsletter_label_birthday; ?></label>
                        <input type="text" id="birthday" name="birthday" class="form-control form-control-date" placeHolder="<?php echo $newsletter_label_birthday; ?>" required="required" title="Birthday"/>
                    </div>
                    
                    <!-- <div class="form-group">
                        <label for="birthday-text">Birthday</label>
                        <input type="text" id="birthday-text" name="birthday-text"  class="form-control" required="required" placeHolder="yyyy-mm-dd" maxlength="10" minlength="10" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" title="Birthday"/>
                    </div> -->
                                        
                    <div class="form-group">
                        <label for="zipcode"><?php echo $newsletter_label_zipcode; ?></label>
                        <input type="text" name="zipcode" id="zipcode" class="form-control" placeholder="<?php echo $newsletter_label_zipcode; ?>" required="required" minlength="2" maxlength="6" pattern="[0-9]{5}[A-Za-z]?">
                    </div>

                    <div class="form-group select">
                        <label for="country"><?php echo $newsletter_label_zone; ?></label>
                        <select name="country" id="country" class="c-select form-control" required="required">
                            <option value="" selected=""><?php echo $newsletter_label_zone; ?></option>
                            <?php foreach($countries as $iso=>$country) : ?>
                                <option value="<?php echo $iso; ?>"><?php echo $country; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="email"><?php echo $newsletter_label_email; ?></label>
                        <input type="email" name="email" id="email" class="form-control" placeholder="<?php echo $newsletter_label_email; ?>" required="required" maxlength="50">
                        <!-- <small>Try <i>test@test.com</i> for ajax validation</small> -->
                    </div>
                    
                    <div class="mendatory"><?php echo $newsletter_must_filled; ?></div>
                    
                    <div class="input-error">
                        <div class="error error_invalid_email"><?php echo $newsletter_error_invalid_email; ?></div>
                    </div>
                    
                    <p class="info"><?php echo $newsletter_legend; ?></p>

                    <div class="form-group form-group-optin">
                        <div class="wrapper_input ">
                            <input id="custom_newsletter" name="custom_newsletter" type="checkbox" value="custom_newsletter">
                            <label for="custom_newsletter" class="c-input c-radio c-radio-yes">
                            <span class="c-indicator"></span><div class="agree-txt"><?php echo $newsletter_checkbox_text; ?></div>
                            </label>
                        </div>
                    </div>

                    <div class="response">
                        <div class="error error_global"><?php echo $newsletter_error_global; ?></div>
                        <div class="error error_age_legal"><?php echo $newsletter_error_age_legal; ?></div>
                        <div class="error error_already_subscribed"><?php echo $newsletter_error_already_subscribed; ?></div>
                    </div>

                    <input type="hidden" id="lang" name="lang" value="<?php echo $current_lang; ?>">
                    <input type="submit" class="btn btn-link2" value="<?php echo $newsletter_submit_text; ?>">
                    

                </div>
            </form>
        </div>
        <div class="legal"><?php echo $newsletter_footer; ?></div>
    </div>
</div>
