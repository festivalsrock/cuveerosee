<?php
/**
 * Template Name: Wine
 */

$wine_cover_image = get_field('wine_cover_image');
$wine_cover_image_portrait = get_field('wine_cover_image_portrait');
$cover_scroll_text = get_field('cover_scroll_text');

$wine_details1_uptitle = get_field('wine_details1_uptitle');
$wine_details1_title = nl2br(get_field('wine_details1_title',null, false));
$wine_details1_intro = nl2br(get_field('wine_details1_intro',null, false));
$wine_details1_text = nl2br(get_field('wine_details1_text',null, false));

$wine_details2_title = nl2br(get_field('wine_details2_title',null, false));
$wine_details2_text = nl2br(get_field('wine_details2_text',null, false));
$wine_details2_image_topleft = get_field('wine_details2_image_topleft');
$wine_details2_image_bottomleft = get_field('wine_details2_image_bottomleft');
$wine_details2_image_right = get_field('wine_details2_image_right');
$wine_details2_pdf = get_field('wine_details2_pdf');
$wine_details2_pdf_text = get_field('wine_details2_pdf_text');

$wine_accords_title = nl2br(get_field('wine_accords_title',null, false));
$wine_accords_items = get_field('wine_accords_items');

$wine_timeline_title = nl2br(get_field('wine_timeline_title',null, false));
$wine_timeline_image = get_field('wine_timeline_image');

$args = array(
    'posts_per_page'   => 9999,
    'orderby'          => 'date',
    'order'            => 'DESC',
    'post_type'        => 'cuveerose_timeline',
    'post_status'      => 'publish'
);
$timeline_posts = get_posts( $args );

$timeline = array();
foreach($timeline_posts as $timeline_post) {
    $year = get_field('year', $timeline_post->ID);
    if(intval($year) > 1900) {
        $timeline[intval($year)][] = [
            'uptitle' => get_field('uptitle', $timeline_post->ID), //$timeline_post->post_title,
            'title' => get_field('title', $timeline_post->ID),
            'text' => nl2br(get_field('text', $timeline_post->ID, false)),
            'image' => get_field('image', $timeline_post->ID),
        ];
    }
}
krsort($timeline);
$timeline_years = array();
foreach($timeline as $year => $e) {
    $timeline_years[] = $year;
}


// Timeline
$wine_timeline_data = array();
$args = array(
    'posts_per_page'   => 8,
    'offset'           => 0,
    'category'         => '',
    'category_name'    => '',
    'orderby'          => 'menu_order',
    'order'            => 'DESC',
    'include'          => '',
    'exclude'          => '',
    'meta_key'         => '',
    'meta_value'       => '',
    'post_type'        => 'cuveerose_timeline',
    'post_mime_type'   => '',
    'post_parent'      => '',
    'author'	   => '',
    'author_name'	   => '',
    'post_status'      => 'publish',
    'suppress_filters' => true,
    'fields'           => '',
);
$timeline_items = get_posts( $args );

foreach($timeline_items as $timeline_item) {
    $uptitle = get_field('uptitle', $timeline_item->ID);
    $title = get_field('title', $timeline_item->ID);
    $text = get_field('text', $timeline_item->ID);
    $image = get_field('image', $timeline_item->ID);
    $year = get_field('year', $timeline_item->ID);
    if($year instanceof WP_Term) {
        $year->image = get_field('image', $year);
        $wine_timeline_data[$year->name]['year'] = $year;
        $wine_timeline_data[$year->name]['items'][] = $timeline_item;
    }
}

get_header(); ?>

    <div class="wine">

        <section class="wine-header"> 
        <?php if(CUVEEROSE_IS_MOBILE) { ?> 

            <img class="palm-only lazy" data-src="<?php echo getAcfImageUrl($wine_cover_image_portrait); ?>" src="<?php echo getAcfImageUrl($wine_cover_image_portrait, 'lazy'); ?>" alt="<?php echo getAcfImageAlt($wine_cover_image_portrait); ?>" />

        <?php } else { ?>
            
            <img class="portrait-only lazy" data-src="<?php echo getAcfImageUrl($wine_cover_image_portrait); ?>" src="<?php echo getAcfImageUrl($wine_cover_image_portrait, 'lazy'); ?>" alt="<?php echo getAcfImageAlt($wine_cover_image_portrait); ?>" />

            <div class="background landscape-only lazy" data-background="<?php echo getAcfImageUrl($wine_cover_image); ?>" style="background-image: url(<?php echo getAcfImageUrl($wine_cover_image, 'lazy'); ?>)"></div>

        <?php } ?> 

            <?php if(!CUVEEROSE_IS_MOBILE) { ?>
            <div class="homepage-header-scroll white desktop-only">
                <!-- <div class="center"> -->
                    <div class="txt"><?php echo $cover_scroll_text; ?></div>
                    <div class="separator"></div>
                <!-- </div> -->
            </div>   
            <?php } ?>

        </section>

        <section class="wine-crafted">
            <div class="max-width">
                <h3 class="name txt-title-small parallax slide-in"><?php echo $wine_details1_uptitle; ?></h3>
                <h2 class="title parallax slide-in"><?php echo $wine_details1_title; ?></h2>
                <div class="separator parallax slide-in"></div>
                <h4 class="subtitle parallax slide-in"><?php echo $wine_details1_intro; ?></h4>
                <p class="resume parallax slide-in"><?php echo $wine_details1_text; ?></p>
            </div>
        </section>

        <section class="wine-infos clearfix">
            <div class="max-width">
                <?php if(!CUVEEROSE_IS_MOBILE) { ?>
                    <div class="photo2-container">
                        <img class="photo2 parallax slide-in lazy" data-src="<?php echo getAcfImageUrl($wine_details2_image_topleft); ?>" src="<?php echo getAcfImageUrl($wine_details2_image_topleft, 'lazy'); ?>" alt="<?php echo getAcfImageAlt($wine_details2_image_topleft); ?>">
                    </div>
                <?php } ?>
                <div class="photo1-container parallax slide-in">
                    <img class="photo1 lazy" data-src="<?php echo getAcfImageUrl($wine_details2_image_right); ?>" src="<?php echo getAcfImageUrl($wine_details2_image_right, 'lazy'); ?>" alt="<?php echo getAcfImageAlt($wine_details2_image_right); ?>">                  
                    <!-- <div class="logo-50"><?php include(locate_template('views/partials/logo-50.php')) ?></div> -->
                </div>
                <div class="txt-container clearfix parallax slide-in">
                    <h2 class="title"><?php echo $wine_details2_title; ?></h2>
                    <p class="resume"><?php echo $wine_details2_text; ?></p>
                    <?php if(isset($wine_details2_pdf['url'])) : ?>
                    <div>
                        <span class="btn-download"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/bt-download.svg" alt=""></span>
                        <a href="<?php echo $wine_details2_pdf['url']; ?>" class="wine-infos-link btn btn-link2" target="_blank"><?php echo $wine_details2_pdf_text; ?></a><span class="file-type">(<?php echo strtoupper($wine_details2_pdf['subtype']); ?> / <?php echo getFileSizeName($wine_details2_pdf['filesize']); ?>)</span>
                    </div>
                    <?php endif; ?>
                </div>
            </div>
        </section>

        

        <section class="wine-portraits">
            <div class="max-width clearfix">
                <img class="photo1 parallax slide-in lazy" data-src="<?php echo getAcfImageUrl($wine_details2_image_bottomleft); ?>" src="<?php echo getAcfImageUrl($wine_details2_image_bottomleft, 'lazy'); ?>" alt="<?php echo getAcfImageAlt($wine_details2_image_bottomleft); ?>">
                <h2 class="title portrait-only"><?php echo $wine_accords_title; ?></h2>
                <div class="wine-portraits-slider parallax slide-in">
                    <h2 class="title landscape-only"><?php echo $wine_accords_title; ?></h2>
                    <div class="wine-portraits-slider-bg"></div>
                    <?php if(is_array($wine_accords_items) && sizeof($wine_accords_items) > 0) : ?>
                    <div class="wine-portraits-slider-pages swiper-pagination"><span id="page-num" class="page-num">1</span><div class="line"></div><span id="page-total" class="page-total"><?php echo sizeof($wine_accords_items); ?></span></div>
                    <div class="wine-portraits-slider-container swiper-container js-slider-portraits">
                        <div class="swiper-wrapper">

                            <?php foreach($wine_accords_items as $k => $portrait) :  ?>

                            <!-- SLIDE <?php echo ($k + 1); ?> -->
                            <div class="swiper-slide">
                                <img class="photo swiper-lazy" data-src="<?php echo getAcfImageUrl($portrait['image_big']); ?>" src="<?php echo getAcfImageUrl($portrait['image_big'], 'lazy'); ?>" alt="<?php echo getAcfImageAlt($portrait['image_big']); ?>">
                                <div class="swiper-slide-block">
                                    <div class="swiper-slide-block-txt">
                                        <img class="logo" src="<?php echo getAcfImageUrl($portrait['logo']); ?>" alt="<?php echo getAcfImageAlt($portrait['logo']); ?>">
                                        <div class="title"><?php echo $portrait['title']; ?></div>
                                        <div class="place"><?php echo $portrait['subtitle']; ?></div>
                                        <div class="separator"></div>
                                        <p class="resume"><?php echo nl2br(strip_tags_only($portrait['text'], 'p')); ?></p>
                                    </div>
                                    <img class="photo2 swiper-lazy" data-src="<?php echo getAcfImageUrl($portrait['image_small']); ?>" src="<?php echo getAcfImageUrl($portrait['image_small'], 'lazy'); ?>" alt="<?php echo getAcfImageAlt($portrait['image_small']); ?>">
                                </div>
                            </div>

                            <?php endforeach; ?>
                        </div>
                        <div class="swiper-button">
                            <div class="swiper-button-prev">
                                <svg version="1.1" id="btn-swiper-prev" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="80px" height="80px" viewBox="0 0 80 80" enable-background="new 0 0 80 80" xml:space="preserve">
                                    <rect fill="#FFFFFF" width="80" height="80"/>
                                    <polygon class="head" points="35.556,38.889 44.444,33.333 44.444,44.444 "/>
                                    <rect class="tail" fill="#000000" x="44" y="37" width="1em" height="4"/>
                                </svg>
                            </div>
                            <div class="swiper-button-next">
                                <svg version="1.1" id="btn-swiper-next" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="80px" height="80px" viewBox="0 0 80 80" enable-background="new 0 0 80 80" xml:space="preserve">
                                    <rect fill="#FFFFFF" width="80" height="80"/>
                                    <polygon class="head" points="44.444,38.889 35.556,44.444 35.556,33.333 "/>
                                    <rect class="tail" fill="#000000" x="28" y="37" width="1em" height="4"/>
                                </svg>
                            </div>
                        </div>
                    </div>
                    <?php endif; ?>
                </div>
            </div>
        </section>

        <?php if(sizeof($timeline_years) > 0) : ?>
        <section class="wine-notes js-wine-notes" id="wine-notes">
            <div class="max-width">
                <div class="left">
                    <div class="title"><?php echo $wine_timeline_title; ?></div>
                    <img class="photo lazy" data-src="<?php echo getAcfImageUrl($wine_timeline_image); ?>" src="<?php echo getAcfImageUrl($wine_timeline_image, 'lazy'); ?>" alt="<?php echo getAcfImageAlt($wine_timeline_image); ?>">
                    <!-- https://github.com/nolimits4web/Swiper/blob/master/demos/300-thumbs-gallery.html -->
                    <div class="wine-notes-slider-years js-slider-years">
                        <ul>
                            <?php $i = 0; foreach($timeline_years as $year) : ?>
                            <li class="<?php echo ($i == 0) ? 'active' : ''; ?>"><?php echo $year; ?></li>
                            <?php $i++; endforeach; ?>
                        </ul>
                    </div>
                </div>
                <div class="right">
                    <div class="wine-notes-slider-notes js-slider-notes">
                        <div class="swiper-wrapper">

                            <?php foreach($timeline_years as $year) : ?>
                            <!-- <?php echo $year; ?> -->
                            <div class="swiper-slide">
                                <!-- MULTIPLE NOTES IN ONE YEAR -->

                                <?php foreach($timeline[$year] as $note) : ?>
                                <div class="swiper-slide-note">
                                    <div class="title"><?php echo $note['uptitle']; ?></div>
                                    <div class="logo">
                                        <img src="<?php echo getAcfImageUrl($note['image']); ?>" alt="<?php echo getAcfImageAlt($note['image']); ?>">
                                    </div>
                                    <div class="separator"></div>
                                    <div class="place"><?php echo $note['title']; ?></div>
                                    <p class="resume"><?php echo $note['text']; ?></p>
                                </div>
                                <?php endforeach; ?>
                            </div>
                            <?php endforeach; ?>

                        </div>
                    </div>
                </div>
            </div>
        </section>
        <?php endif; ?>

    </div>

<!-- <pre>
<?php var_dump($wine_timeline_data); ?>
</pre> -->

<?php get_footer(); ?>